<?php 
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
 
namespace App\Controller;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\Network\Email\Email;
use Cake\Datasource\ConnectionManager; // For Custom query
use Cake\Core\Configure;
use Cake\Utility\Security;


require_once(ROOT .  DS .'vendor' . DS . 'hybridauth' . DS . 'Hybrid' . DS . 'Auth.php');
use Hybrid;

use Cake\Event\Event;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */

class CommingusersController extends AppController
{
    public $helpers = ['Form'];
    private $provider = "facebook";


    /**
    * Function which is call at very first when this controller load
    */
     
    public function initialize()
    {
        parent::initialize();
        set_time_limit(300);
        // Loaded EmailTemplate Model
        
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);    $this->Auth->allow(array('fbindex', 'fblogin', 'glogin','fblogout', 'viewForm', 'addWowzaFbTarget', 'deleteStream', 'editStream','commonLogOut','editCommon','multilikeViewForm','multilikeFormRegistration','multilikeFormLogin','multilikeViewSocialnetwork','multilikeThankyou','searchSates','searchCities','saveFormSession', 'amigosDePetropolisWhatsapp', 'amigosDePetropolisVoice'));

    }  

	public function amigosDePetropolisWhatsapp(){
		$this->viewBuilder()->layout('embed');

		$countries = $this->getCountryCodesWithDialCode();

		$countriesListing = TableRegistry::get("countries");
        $countriesListingData = $countriesListing->find('all')->toArray();
        $this->set('countries', $countriesListingData);
		
		if($this->request->is('post')){
			$t = TableRegistry::get("multi");
            
            $mult_data = $t->find('all')->where(['phone'=> $this->request->data['phone'],'reg_type = ' => 1])->first();

            if(count($mult_data) == 1){
             
                return $this->redirect('https://multiplierapp.com.br/app/webroot/duplicated.html');
            }else{
			
    			$base = $t->newEntity();

    			$base->name = $this->request->data['name'];
    			$base->pais = $this->request->data['pais'];
    			$base->phone = $this->request->data['phone'];
				$base->reg_type = 1;
    			$base->form = "amigos-de-petropolis-whatsapp";
				
    			if($t->save($base)){
    				header('Location: https://api.whatsapp.com/send?phone=5524988034853'); die();
    			}else{
                    header('Location: https://multiplierapp.com.br/app/duplicated.html');
                }
            }
		}	

        $this->set('page', 'amigos_de_petropolis_whatsapp');	
	}

	public function amigosDePetropolisVoice(){
        $this->set('title', 'Canal de Voz Amigos de Petrópolis');
		$this->viewBuilder()->layout('embed');

		$countries = $this->getCountryCodesWithDialCode();

		$countriesListing = TableRegistry::get("countries");
        $countriesListingData = $countriesListing->find('all')->toArray();
        $this->set('countries', $countriesListingData);
		
		if($this->request->is('post')){

			$t = TableRegistry::get("multi");

			$mult_data = $t->find("all")->where(['phone' =>$this->request->data['phone'],'reg_type' => 0])->first();

		//echo count($mult_data); die;

            if(count($mult_data) == 1){
                return $this->redirect('https://multiplierapp.com.br/app/webroot/duplicated.html');
            }else{

            $base = $t->newEntity();
            $phonevalidator = $t->find('all', [
                'conditions' => ['multi.phone' => $this->request->data['phone'],'reg_type' => 0]
            ]);
            $number = $phonevalidator->count();
			//echo $number;die;
            
			
			if($number > 0){
                return $this->redirect('https://multiplierapp.com.br/app/webroot/duplicated.html');
            }
			$base->name = $this->request->data['name'];
			$base->pais = $this->request->data['pais'];
			$base->phone = $this->request->data['phone'];
			$base->reg_type = 0;
			$base->form = "amigos-de-petropolis-voice";
			
			if($t->save($base)){
				die();
			}
        }

		}

         $this->set('page', 'amigos_de_petropolis_voice');
		
	}

    /**
    * This function is for get config variable
    *
    */ 
    private function getConfig(){
        return array(
            "base_url" => HTTP_ROOT . "commingusers/fbindex",
            "providers" => array(
                "Google" => array(
                    "enabled" => true,
                    "keys" => array("id" => GMAIL_APP_ID, "secret" => GMAIL_APP_SECRET),
                    "approval_prompt" =>'force',
                    'access_type' =>"offline",
                    "scope" => " https://www.googleapis.com/auth/youtube.readonly https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtube.force-ssl https://www.googleapis.com/auth/plus.login ", // optional
                ),
                "Facebook" => array ( 
                    "enabled" => true,
                    "keys"    => array ( "id" => FB_APP_ID, "secret" => FB_APP_SECRET, "trustForwarded" => true, "allowSignedRequest" => false, ), 
                ),
            ),

            // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
            "debug_mode" => true,
            "debug_file" => "../webroot/log.text",
        );
    } 
    
    function fbindex($formId='')
    {
        if ($_GET['error'] =='access_denied')  // if user cancelled facebook login
        {
            $this->Flash->error(__($_GET['error_description']));
            return $this->redirect(array("controller" => "commingusers", "action" => "viewForm?id=" . $formId));
        }
        require_once(ROOT .  DS .'vendor' . DS . 'hybridauth' . DS . 'Hybrid' . DS . 'Auth.php');
        require_once(ROOT .  DS .'vendor' . DS . 'hybridauth' . DS . 'Hybrid' . DS . 'Endpoint.php');
        \Hybrid_Endpoint::process();
    }

    public function getFBData($id=''){
        try{            
        $config =array(
            //"base_url" => HTTP_ROOT."commingusers/fbindex/".$id, 
            "base_url" => HTTP_ROOT."commingusers/fbindex/", 
            "providers" => array ( 
                "Facebook" => array ( 
                    "enabled" => true,
                    "keys"    => array ( "id" => FB_APP_ID, "secret" => FB_APP_SECRET, "trustForwarded" => true, "allowSignedRequest" => false, ), 
                        'scope'   => 'email, public_profile, user_events, business_management, manage_pages, publish_video, publish_to_groups, publish_pages,  read_insights, user_friends' 
                ),
            ),
            // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
            "debug_mode" => false,
            "debug_file" => "../webroot/log.text",
        );
        $hybridauth = new \Hybrid_Auth( $config );
        $authProvider = $hybridauth->authenticate($provider = 'facebook');
        $user_profile = $authProvider->getUserProfile();
        if($user_profile && isset($user_profile->identifier)){  
            $session = $this->request->session();
            $session->write('fb.is_login', 'facebook'); 
            $session->write('fb.name', $user_profile->displayName); 
            $session->write('fb.identifier', $user_profile->identifier); 
            $session->write('fb.profile_url', $user_profile->profileURL); 
            $session->write('fb.image', $user_profile->photoURL); 
            $session->write('fb.email', $user_profile->email); 
            $session->write('fb.accesstoken', $user_profile->accessToken); 
            $session->write('fb.posts', $user_profile->posts); 
            $session->write('fb.groups', $user_profile->groups); 
            $session->write('fb.accounts', $user_profile->accounts); 
            $session->write('fb.events', $user_profile->events);                
        }
        }catch (\Exception $e) {

            switch ($e->getCode()) {
                case 0 : echo "Unspecified error.";
                    break;
                case 1 : echo "Hybridauth configuration error.";
                    break;
                case 2 : echo "Provider not properly configured.";
                    break;
                case 3 : echo "Unknown or disabled provider.";
                    break;
                case 4 : echo "Missing provider application credentials.";
                    break;
                case 5 : echo "Authentication failed. "
                    . "The user has canceled the authentication or the provider refused the connection.";
                    break;
                case 6 : echo "User profile request failed. Most likely the user is not connected "
                    . "to the provider and he should to authenticate again.";
                    $this->fblogout();
                    break;
                case 7 : echo "User not connected to the provider.";
                    $this->fblogout();
                    break;
                case 8 : echo "Provider does not support this feature.";
                    break;
            }

            // well, basically your should not display this to the end user, just give him a hint and move on..
            echo "<br /><br /><b>Original error message:</b> " . $e->getMessage();

            echo "<hr /><h3>Trace</h3> <pre>" . $e->getTraceAsString() . "</pre>";
        }
    }


    public function glogin() {
        $formId = $_GET['id'];
        $this->provider = "google";
        if($this->commonLogin("google")){
            $this->redirect(array("controller" => "commingusers",
                    "action" => "viewForm?id=" . $formId));
        }
        
    }

    private function commonLogin(){
        try {
            $hybridauth = new \Hybrid_Auth($this->getConfig());
            $authProvider = $hybridauth->authenticate($provider = $this->provider);
            $user_profile = $authProvider->getUserProfile();
            if ($user_profile && isset($user_profile->identifier)) {
                $session = $this->request->session();
                $session->write('social.is_login',$this->provider);
                $session->write('social.name', $user_profile->displayName);
                $session->write('social.identifier', $user_profile->identifier);
                $session->write('social.profile_url', $user_profile->profileURL);
                $session->write('social.image', $user_profile->photoURL);
                $session->write('social.email', $user_profile->email);
                $session->write('social.theme', "google");
                if($provider == "google"){
                     $accessTokn = $authProvider->getAccessToken();
                    $session->write('social.accesstoken', $accessTokn['access_token']);
                    $session->write('social.reftoken', $accessTokn['refresh_token']);
                }
                return true;
            }
        } catch (\Exception $e) {

            switch ($e->getCode()) {
                case 0 : echo "Unspecified error.";
                    break;
                case 1 : echo "Hybridauth configuration error.";
                    break;
                case 2 : echo "Provider not properly configured.";
                    break;
                case 3 : echo "Unknown or disabled provider.";
                    break;
                case 4 : echo "Missing provider application credentials.";
                    break;
                case 5 : echo "Authentication failed. "
                    . "The user has canceled the authentication or the provider refused the connection.";
                    break;
                case 6 : echo "User profile request failed. Most likely the user is not connected "
                    . "to the provider and he should to authenticate again.";
                    $this->commonLogOut();
                    break;
                case 7 : echo "User not connected to the provider.";
                    $this->commonLogOut();
                    break;
                case 8 : echo "Provider does not support this feature.";
                    break;
            }

            // well, basically your should not display this to the end user, just give him a hint and move on..
            echo "<br /><br /><b>Original error message:</b> " . $e->getMessage();

            echo "<hr /><h3>Trace</h3> <pre>" . $e->getTraceAsString() . "</pre>";
        }
    }

    public function commonLogOut(){  
        $formId = $_GET['id'];
		$session = $this->request->session();
		$session->write('newNetwork', false);
        $session->delete('social'); 
        $session->delete('fb'); 
        $hybridauth = new \Hybrid_Auth( $this->getConfig());
        //$authProvider = $hybridauth->authenticate($provider = $this->provider);
        $authProvider = $hybridauth->authenticate($provider = 'google');
        $user_profile = $authProvider->logout();  
        $hybridauth->logoutAllProviders();      
        $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
   
    }

    public function fblogin()
    {   

		/**
		 * 0 = Profile
		 * 1 = Groups
		 * 2 = Pages
		 */
		$permission = $_GET['permission'];

		$scope = "email, public_profile, user_events, business_management, read_insights, user_friends, ";

		switch($permission){
			case 0:
			case "0":
				$scope .= "publish_video";
				$permission = "timeline";
				break;
			case 1:
			case "1":
				$scope .= "publish_to_groups";
				$permission = "group";
				break;
			case 2:
			case "2":
				$scope .= "manage_pages, publish_pages";
				$permission = "page";
				break;
			default:
				$scope .= "publish_video, publish_to_groups, manage_pages, publish_pages";
				$permission = "timeline";
		}

        $formId = $_GET['id'];
        $config =array(
			//"base_url" => HTTP_ROOT."commingusers/fbindex/".$formId, 
			"base_url" => HTTP_ROOT."commingusers/fbindex/", 
			"providers" => array ( 
				"Facebook" => array ( 
					"enabled" => true,
					"keys"    => array ( "id" => FB_APP_ID, "secret" => FB_APP_SECRET, "trustForwarded" => true, "allowSignedRequest" => false, ), 
					// 'scope'   => 'email, public_profile, user_events, business_management, manage_pages, publish_pages, publish_video, publish_to_groups, read_insights, user_friends', 
					'scope' => $scope,
					'trustForwarded' => false
				),
			),
			// if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
			"debug_mode" => false,
			"debug_file" => "../webroot/log.text",
    	);

        try{
            $hybridauth = new \Hybrid_Auth( $config );
        

            $authProvider = $hybridauth->authenticate($provider = 'facebook');

            $user_profile = $authProvider->getUserProfile();

            if($user_profile && isset($user_profile->identifier))
            {  

                //echo '<pre>'; print_r($user_profile); die; 
                $session = $this->request->session();
                $session->write('fb.is_login', 'facebook'); 
                $session->write('fb.name', $user_profile->displayName); 
                $session->write('fb.identifier', $user_profile->identifier); 
                $session->write('fb.profile_url', $user_profile->profileURL); 
                $session->write('fb.image', $user_profile->photoURL); 
                $session->write('fb.email', $user_profile->email); 
                $session->write('fb.accesstoken', $user_profile->accessToken); 
                $session->write('fb.posts', $user_profile->posts); 
                $session->write('fb.groups', $user_profile->groups); 
                $session->write('fb.accounts', $user_profile->accounts); 
				$session->write('fb.events', $user_profile->events); 
				$session->write('fb.permissions', $permission);
                $this->redirect(array("controller" => "commingusers", 
                      "action" => "viewForm?id=" . $formId));
            }           

            }
            catch( \Exception $e )
            { 
            
                 switch( $e->getCode() )
                 {
                        case 0 : echo "Unspecified error."; break;
                        case 1 : echo "Hybridauth configuration error."; break;
                        case 2 : echo "Provider not properly configured."; break;
                        case 3 : echo "Unknown or disabled provider."; break;
                        case 4 : echo "Missing provider application credentials."; break;
                        case 5 : echo "Authentication failed. "
                                         . "The user has canceled the authentication or the provider refused the connection.";
                                 break;
                        case 6 : echo "User profile request failed. Most likely the user is not connected "
                                         . "to the provider and he should to authenticate again.";
                           $this->fblogout();
                                 break;
                        case 7 : echo "User not connected to the provider.";
                            $this->fblogout();
                                 break;
                        case 8 : echo "Provider does not support this feature."; break;
                }

                // well, basically your should not display this to the end user, just give him a hint and move on..
                echo "<br /><br /><b>Original error message:</b> " . $e->getMessage();

                echo "<hr /><h3>Trace</h3> <pre>" . $e->getTraceAsString() . "</pre>";

            }
    }


    public function fblogout($value='')
    {   
        $formId=$_GET['id'];
		$session = $this->request->session();
		$session->write('newNetwork', false);
        $session->delete('fb'); 
        $config =array(
            "base_url" => HTTP_ROOT."commingusers/fbindex", 
            "providers" => array ( 
                "Facebook" => array ( 
                    "enabled" => true,
                    "keys"    => array ( "id" => FB_APP_ID, "secret" => FB_APP_SECRET ),
                ),
            ),
            // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
            "debug_mode" => false,
            "debug_file" => "",
        );
        $hybridauth = new \Hybrid_Auth( $config );
        $authProvider = $hybridauth->authenticate($provider = 'facebook');
        $user_profile = $authProvider->logout();    
        $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
    }


    function checkTargetExists($formUsersData,$mainfbFlag,$mainflagOption,$identifier='')
    {

        foreach($formUsersData as $fuKey=>$fuValue)
        {
            $socialAccountAllready =  "";

            if($mainfbFlag == "timeline"){
                if($fuValue->flag == $mainfbFlag){
                   $socialAccountAllready = "1";
                   break;
                }
            }else if($mainfbFlag == "page"){
                $mainflagOption = $this->request->data['pages'];
                  if(($fuValue->flag == $mainfbFlag) && ($fuValue->flag_option == $mainflagOption)){
                   $socialAccountAllready = "1";
                   break;
                }
            }else if($mainfbFlag == "event"){
                $mainflagOption = $this->request->data['events'];
                  if(($fuValue->flag == $mainfbFlag) && ($fuValue->flag_option == $mainflagOption)){
                   $socialAccountAllready = "1";
                   break;
                }
            }else if($mainfbFlag == "group"){
                $mainflagOption = $this->request->data['groups'];
                if(($fuValue->flag == $mainfbFlag) && ($fuValue->flag_option == $mainflagOption)){
                   $socialAccountAllready = "1";
                   break;
                }
            }
        }
        if(!empty($socialAccountAllready)){
            return true;
        }
        return false;
    }

    function _checkTargetExists($formUsersData,$identifier='')
    {
        $userExists = searchSubArray(clean($formUsersData,true),'user_identifier_id', $identifier);
        if (!empty($userExists)) {
            return true;
        }
        return false;
    }

    function videoType($url) {
        if (strpos($url, 'youtube') > 0) {
            return 'youtube';
        } elseif (strpos($url, 'vimeo') > 0) {
            return 'vimeo';
        } else {
            return 'unknown';
        }
    }

    /*
    * Save Comming user data
    * @CreatedBy gourav soni
    * @param Array
    */
    public function saveCommingUserData($CommingUserListingModel,$arr){
        $CommingUserData                        = $CommingUserListingModel->newEntity();
        $CommingUserData->form_id               = $arr['form_id'];
        $CommingUserData->target_name           = $arr['target_name'];
        $CommingUserData->flag                  = $arr['flag']; 
        $CommingUserData->flag_option           = $arr['flag_option'];
        $CommingUserData->app_id                = $arr['app_id'];
        $CommingUserData->group_id              = ($arr['group_id']) ? $arr['group_id'] : 0;
        $CommingUserData->str_src_name          = $arr['str_src_name']; 
        $CommingUserData->stream_title          =$arr['stream_title'];
        $CommingUserData->stream_description    = $arr['stream_description']; 
        $CommingUserData->access_token          = $arr['access_token'];
        $CommingUserData->user_identifier_id    = $arr['user_identifier_id'];
        $CommingUserData->new_stream_url        = $arr['new_stream_url'];
        $CommingUserData->stream_key            = $arr['stream_key'];
        $CommingUserData->rmtp_stream_id        = $arr['rmtp_stream_id'];
        $CommingUserData->new_stream_url_secure = $arr['new_stream_url_secure']; 
        $CommingUserData->rtmp_url              =  $arr['rtmp_url']; 
        $CommingUserData->app_name              = $arr['app_name'];
        $CommingUserData->fb_email              = $arr['fb_email']; 
        $CommingUserData->fb_name               =$arr['fb_name'];
        $CommingUserData->image_path            = $arr['image_path'];
        $CommingUserData->wowza_status_message  = $arr['wowza_status_message'];
        $CommingUserData->followers_updated_on  =  $arr['followers_updated_on'];
        $CommingUserData->live_status           = $arr['live_status'];
        $CommingUserData->form_data =$arr['form_data'];
        $CommingUserData->followers             = $arr['followers'];
        $CommingUserData->broadcast_id          = $arr['broadcast_id'];
        $CommingUserData->channelId          = $arr['channelId'];
        $CommingUserData->reftoken          = $arr['reftoken'];
        /*new field addition*/
        $CommingUserData->registerat          = getUtcTime();


        if($arr['user_name'] && $arr['user_email'] && $arr['user_country'] && $arr['user_phone']){
                $CommingUserData->user_name          = $arr['user_name'];
                $CommingUserData->user_email          = $arr['user_email'];
                $CommingUserData->user_country          = $arr['user_country'];
                $CommingUserData->user_phone          = $arr['user_phone'];
            }
            
        if ( $addedTarget = $CommingUserListingModel->save( $CommingUserData ) ) {
            $this->changeAutoLiveStatus($CommingUserData->id,1,true);
            $configData=Configure::read('sonoconfig.mautic');
            $this->updateMauticData($configData['registration_form_segment_id']);
            //return true;
			return $addedTarget;
        }else{
            return false;
        }

    }


    /*
    * This function is for handle all post request from google stream registration
    * @CreatedBy gourav soni
    * @Param Arr $formData
    */
    public function broadcastsGoogleFormSubmit($formData,$CommingUserListingModel,$formId,$formUsersData, $user_name="", $user_email="", $user_country="", $user_phone=""){
        $session = $this->request->session();
        $followerCount      = 0;
        $flag               = ($this->request->data('flag')!="") ? $this->request->data('flag') : "youtube";
        $streamTitle        = ($this->request->data('stream_title')!="") ? $this->request->data('stream_title') : $formData->facebook_title;
        $streamDescription  = ($this->request->data('stream_description')!="") ? $this->request->data('stream_description') : $formData->facebook_description;
        $targetName         = $this->request->data('target_name');
        $socialEmail        = $session->read( 'social.email' );
        $streamDescriptionUpdated = $streamDescription;
        /** If trial sentence not hide then get sentence **/
        $trial_sentence = $this->getTrialSentence($formData->owner_id);
        if($trial_sentence){
            $streamDescriptionUpdated .= " ".$trial_sentence;
        }

        /*if($formData->hide_trial_sentence != 1){
            $SentencesModel = TableRegistry::get("sentences");
            $SentenceData = $SentencesModel->find('all')->where(['title'=>'trial_sentence'])->first();
            $this->set('SentenceData',$SentenceData);
            $streamDescriptionUpdated .= " ".$SentenceData->content;
        }*/
        $streamTitle = $this->cleanString($streamTitle);
        $streamDescriptionUpdated = $this->cleanString($streamDescriptionUpdated);
        $identifier = $session->read( 'social.identifier' );
        $target_name = $this->googleSteamRegister($streamTitle,$streamDescription,$this->request->data('privacy'),$formId,$session->read( 'social.accesstoken'),false,true);

        if(!$target_name){
            if($session->read('social_selected_error') == 'required_t_n_d'){
                /*echo '<ul class="list-group">
                      <li style="margin-bottom:15px;" class="list-group-item">Olá, não é possível cadastrar esse canal YouTube pois ou o tìtulo ou a descrição do post estão em branco em <a target="_blank" href="https://www.youtube.com/live_dashboard">www.youtube.com/live_dashboard</a> por favor acesse como admin do canal e preencha com qualquer texto ou palavra. Verifique também por favor se seu canal está ativado para transmissões ao vivo acessando <a target="_blank" href="https://www.youtube.com/features">www.youtube.com/features</a> e após isso, tente novamente seu cadastro no Multiplierapp. obrigado.</li>
                      <li style="margin-bottom:15px;" class="list-group-item">Hi, it is not possible to register this YouTube channel because either the title or description of the post is blank at <a target="_blank" href="https://www.youtube.com/live_dashboard">www.youtube.com/live_dashboard</a> please access as channel admin and fill in with any text or word. Please also check that your channel is enabled for live streams by going to <a target="_blank" href="https://www.youtube.com/features">www.youtube.com/features</a> and after that, try your Multiplierapp registration again. thank you.</li>
                      <li style="margin-bottom:15px;" class="list-group-item">Hola, no es posible registrar este canal de YouTube porque el título o la descripción de la publicación están en blanco en <a target="_blank" href="https://www.youtube.com/live_dashboard">www.youtube.com/live_dashboard</a>, vaya como administrador del canal y complete cualquier texto o palabra. También verifique que su canal esté habilitado para transmisiones en vivo visitando <a target="_blank" href="https://www.youtube.com/features">www.youtube.com/features</a> y luego intente registrarse nuevamente en Multiplierapp. gracias.</li>
                    </ul>';
                
                echo "<br/>";
                echo "<img height='800px' src='".HTTP_ROOT.'img/uploads/YT_t_n_d.jpg'."' />";
                echo "<br/>";*/

				$session->write('youtubeError', true);
				return $this->redirect($this->referer() );

                // $selectedLanguage=$this->siteLanguage();   
                // if($selectedLanguage=='English'){
                //     $this->ytTargetRegistrationErrorMessageEng();
                // }else if($selectedLanguage=='Spanish'){
                //     $this->ytTargetRegistrationErrorMessageSpan();
                // }else{
                //     $this->ytTargetRegistrationErrorMessagePort();
                // }

                // echo "<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";
                // exit;
            }else{
				$session->write('youtubeError', true);
				return $this->redirect($this->referer() );
                // $selectedLanguage=$this->siteLanguage();   
                // if($selectedLanguage=='English'){
                //     $this->ytTargetRegistrationErrorMessageEng();
                // }else if($selectedLanguage=='Spanish'){
                //     $this->ytTargetRegistrationErrorMessageSpan();
                // }else{
                //     $this->ytTargetRegistrationErrorMessagePort();
                // }

                // echo $session->read('social_selected_error')."<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";
                // exit;
            }
            
        }
        
        $target_name = str_replace(" ", "", $target_name);
        $target_name = $target_name ."_". time();// make url unique
        //$target_name = "YOUTUBE_Channel_". time();
        $appData =  $this->getApplicationForUser($formData->group->id, $target_name, $formData['wowza_source_name']);


        /**** Check plans and set wowza status ****/

        $enabled = "false";
        $live_status = $this->checkFormPlans($formData->owner_id, $formData->form_id, 'youtube');
        if($live_status == 1){
            $enabled = "true";
        }

        if(!empty( $this->_checkTargetExists($formUsersData,$identifier) )){
            $CmModel = TableRegistry::get("comming_users");
            $commingData = $CmModel->find("all")->where(['form_id' => $formData->form_id,'flag = ' => 'youtube', 'wowza_stream_status' => 'Error', 'user_identifier_id' => $identifier, 'live_status' => 0])->first();

                $errorTargetId = 0;

                if(!empty($commingData)){

                    $errorTargetId = $commingData->id;

                    $response = $this->googleSteamRegister($streamTitle,$streamDescription,$this->request->data('privacy'),$formId,$session->read( 'social.accesstoken'));  
                     if($response){

                       $added = $this->addWowzaYoutubeStream($appData->app_name, $target_name, $formData['wowza_source_name'], $response['streamName'], $enabled);
                        if(!$added){
                            echo  __(no_rtmp)."<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";exit;
                        }else{
                           

                            $commingData->target_name       = $target_name;
                            $commingData->access_token = $session->read( 'social.accesstoken');
                            $commingData->new_stream_url        = $response['rtmpUrl']; 
                            $commingData->stream_key            = $response['streamName']; 
                            $commingData->rmtp_stream_id        = $response['streamId'];
                            $commingData->new_stream_url_secure = "new_stream_url_secure"; 
                            $commingData->rtmp_url              = $response['rtmpUrl']; 
                            $commingData->wowza_status_message  = 'Success';
                            $commingData->wowza_stream_status   = 'Waiting...';
                            $commingData->live_status           = $live_status;

                            if($CmModel->save($commingData)){
                                $session->write('successfull', __(activesaved));
                                return $this->redirect( $FormSingleData->success_url );
                            }

                        }
                     }else{
						$session->write('youtubeError', true);
						return $this->redirect($this->referer() );
                        // $selectedLanguage=$this->siteLanguage();   
                        // if($selectedLanguage=='English'){
                        //     $this->ytTargetRegistrationErrorMessageEng();
                        // }else if($selectedLanguage=='Spanish'){
                        //     $this->ytTargetRegistrationErrorMessageSpan();
                        // }else{
                        //     $this->ytTargetRegistrationErrorMessagePort();
                        // }

                        // echo $session->read('social_selected_error')."<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";

                        // exit;

                     }
                

                }else{
            $session->write('social_selected_error', __(alreadyconnected));
            return $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
                }
        }else{
			
             $response = $this->googleSteamRegister($streamTitle,$streamDescription,$this->request->data('privacy'),$formId,$session->read( 'social.accesstoken'));  
             if($response){

               $added = $this->addWowzaYoutubeStream($appData->app_name, $target_name, $formData['wowza_source_name'], $response['streamName'], $enabled);
                if(!$added){
                    echo  __(no_rtmp)."<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";exit;
                }
             }else{
				$session->write('youtubeError', true);
				return $this->redirect($this->referer() );

                // $selectedLanguage=$this->siteLanguage();   
                // if($selectedLanguage=='English'){
                //     $this->ytTargetRegistrationErrorMessageEng();
                // }else if($selectedLanguage=='Spanish'){
                //     $this->ytTargetRegistrationErrorMessageSpan();
                // }else{
                //     $this->ytTargetRegistrationErrorMessagePort();
                // }
                
                // echo $session->read('social_selected_error')."<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";
                
                // exit;

             }
        }
        $image_path_stored = HTTP_ROOT.'img/uploads/stream_images/'.$identifier.'.jpg';
        $image_path = "";
        if(copy($response['imageUrl'], './img/uploads/stream_images/'.$identifier.'.jpg')){
             $image_path = $image_path_stored;
        }
      
        $FormListing = $this->request->data;
        if (empty($followerCount)) {
            $followerCount = 0;
        }

        if (!empty($response['channelSub'])) {
            $followerCount = $response['channelSub'];
        }
        $reftoken = '';
        if($session->read( 'social.reftoken') != ""){
            $reftoken = $session->read( 'social.reftoken');
        }
        $requestData = array('form_id' =>$formId,'target_name'=>$target_name,'flag'=>"youtube",'flag_option'=>$this->request->data('privacy'),'app_id'=>$appData->app_id,'group_id'=> 
        $formData->group->id,'str_src_name'=>$formData->wowza_source_name,'stream_title'=>$streamTitle,'stream_description'=>$streamDescription,'access_token'=>$session->read( 'social.accesstoken'),'user_identifier_id'=>$identifier,'new_stream_url'=>$response['rtmpUrl'],'stream_key'=>$response['streamName'],'rmtp_stream_id'=>$response['streamId'],"new_stream_url_secure"=>"new_stream_url_secure",'rtmp_url'=>$response['rtmpUrl'],'app_name'=>$appData->app_name,'fb_name'=>$session->read( 'social.name' ),'fb_email'=>$session->read( 'social.email' ),'image_path'=>$image_path,"wowza_status_message"=>"Success",'followers_updated_on'=>getUtcTime(),"live_status"=>$live_status,'form_data'=> '','followers'=>$followerCount, 'broadcast_id'=>$response['broadCastId'],'reftoken' => $reftoken,'channelId' =>$response['channelId'] , 'user_name' => $user_name, 'user_email' => $user_email, 'user_country' => $user_country, 'user_phone' => $user_phone );

        if($addedTarget = $this->saveCommingUserData($CommingUserListingModel,$requestData)){   
            if (!empty($requestData)){
                // Send Email notification on target registration
                $this->form_data_mail($requestData,$FormListing,$formData);
                // Send Push notification on target registration
                $this->send_push_notification($formData, "Youtube", $session->read( 'social.name' ), "user", $addedTarget);
            }
            if ($FormSingleData->linked_status == '1') {
                $dataRespo = explode('?id=', $FormSingleData->linked_to);
                $parentFormID = $dataRespo[1];
                $FormListingModel = TableRegistry::get("form_listings");
                $parentFormSingleData = $FormListingModel->find('all')->contain(['Groups'])->where(['form_id'=>$parentFormID])->first();
                    
                if (!empty($parentFormSingleData)) {
                    
                    $formUsersData = $CommingUserListingModel->find('all')->where(['form_id' => $parentFormID,'fb_email' => $session->read('fb.email')])->toArray();
                    if (empty( $this->_checkTargetExists($formUsersData,$identifier) ))  // target on parent form already not registerd
                    {   
                        //$parentAppData =  $this->getApplicationForUser($parentFormSingleData->group->id);
                        $parentAppData =  $this->getApplicationForUser($parentFormSingleData->group->id, $target_name, $parentFormSingleData['wowza_source_name']);
                        $added = $this->addWowzaStream($parentAppData->app_name, $target_name, $parentFormSingleData['wowza_source_name'], $new_stream_url, $enabled);
                       // $added = $this->addWowzaYoutubeStream($parentAppData->app_name, $target_name, $parentFormSingleData['wowza_source_name'], $response['streamName'], $enabled);
                        
                        $parentCommingUserListingModel = TableRegistry::get("comming_users");
                        
                        $parentCommingUserData = $parentCommingUserListingModel->newEntity();

                        $parentCommingUserData->form_id                 = $parentFormID;
                        $parentCommingUserData->target_name             = $target_name;
                        $parentCommingUserData->flag                    = "youtube"; 
                        $parentCommingUserData->flag_option             = $this->request->data('privacy');
                        $parentCommingUserData->app_id                  = $parentAppData->app_id;
                        $parentCommingUserData->group_id                = $parentFormSingleData->group->id;
                        $parentCommingUserData->str_src_name            = $parentFormSingleData->wowza_source_name; 
                        $parentCommingUserData->stream_title            = $streamTitle; 
                        $parentCommingUserData->stream_description      = $streamDescription; 
                        $parentCommingUserData->access_token            = $session->read( 'social.accesstoken'); 
                        $parentCommingUserData->user_identifier_id      = $identifier; 
                        $parentCommingUserData->new_stream_url          = $parentnew_stream_url; 
                        $parentCommingUserData->stream_key              = $response['rtmpUrl']; 
                        $parentCommingUserData->rmtp_stream_id          = $response['streamId'];
                        $parentCommingUserData->new_stream_url_secure   = "new_stream_url_secure"; 
                        $parentCommingUserData->rtmp_url                = $response['rtmpUrl']; 
                        $parentCommingUserData->app_name                = $parentAppData->app_name; 
                        $parentCommingUserData->fb_email                = $session->read( 'social.email' ); 
                        $parentCommingUserData->fb_name                 = $session->read( 'social.name');
                        $parentCommingUserData->image_path              = $image_path;                                  
                        $parentCommingUserData->followers               = $followerCount; 
                        $parentCommingUserData->followers_updated_on    = getUtcTime();
                        $parentCommingUserData->wowza_status_message    = 'Success';
                        $parentCommingUserData->live_status             = $live_status; 

                        $jsonCommingData=json_encode($FormListing);
                        $parentCommingUserData->form_data = $jsonCommingData;

                        if ( $parentCommingUserListingModel->save( $parentCommingUserData ) ) {
                            $this->changeAutoLiveStatus($parentCommingUserData->id,1,true);
                            $session->write('successfull', __(activesaved));
                            return $this->redirect( $FormSingleData->success_url );
                        }
                    }
                }
            }
           $session->write('successfull', __(activesaved));
           return true;
        }else{
            $this->Flash->error(__(savenot));
            echo __(not_able_create_wowza)." <a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(Try_again)."</a>";exit;
        }
    }

    /*public function checkTargetPermission($identifier, $type, $access_token, $fb_id){
        $status = false;
        if($type == 'group'){
            $url = "https://graph.facebook.com/v2.8/".$identifier."/admins?access_token=".$access_token;
            $result = json_decode($this->getCurl($url));
            foreach ($result->data as $value) {
                if($value->id == $fb_id){
                    $status = true;
                }
            }
        }else if($type == 'event'){
            $url = "https://graph.facebook.com/v2.8/".$identifier."/?fields=is_viewer_admin&access_token=".$access_token;
            $result = json_decode($this->getCurl($url));
            //echo '<pre>'; print_r($result);die;
            if($result->is_viewer_admin){
                $status = true;
            }
        }else if($type == 'page'){
            $url = "https://graph.facebook.com/v2.8/".$identifier."/?fields=can_post&access_token=".$access_token;
            //echo $url;
            $result = json_decode($this->getCurl($url));
            //echo '<pre>'; print_r($result); die;
            if($result->can_post){
                $status = true;
            }
        }
        return $status;
    }*/

    /*public function checkFutureEvent($identifier, $type, $access_token, $fb_id){

        $url = "https://graph.facebook.com/v2.8/".$identifier."/?access_token=".$access_token;
        $result = json_decode($this->getCurl($url));
        if(strtotime($result->start_time) > strtotime(date('Y-m-d H:i:s'))){
            return true;
        }else{
           return false;
        }
    }*/

    public function syncMessengerData($data){//$fb_id
        //echo 'Atul';
        //echo $fb_id;
        $syncMessengerTbl = TableRegistry::get("tbl_messenger_sync_data");
        //$syncData=$syncMessengerTbl->find('all')->where(['asid' => $fb_id])->toArray();
        //print_r($syncData);
        //if(empty($syncData)){
            //print_r($syncData);
            $syncNewEntry = $syncMessengerTbl->newEntity();
            $syncNewEntry->psid=$data['psid'];
            $syncNewEntry->asid=$data['asid'];
            $syncNewEntry->user_id=$data['user_id'];
            $syncNewEntry->language=$data['language'];
            $syncNewEntry->form_id=$data['form_id'];
            $syncNewEntry->unique_id=$data['unique_id'];
            $syncNewEntry->title=$data['title'];
            $syncNewEntry->description=$data['description'];
            $syncMessengerTbl->save($syncNewEntry);
            return  $syncNewEntry->id;
        //}else{
            //print_r($syncData);
            //print_r($data);
            // $syncData[0]->psid=$data['psid'];
            // $syncData[0]->asid=$data['asid'];
            // $syncData[0]->user_id=$data['user_id'];
            // $syncData[0]->language=$data['language'];
            // $syncData[0]->form_id=$data['form_id'];
            // $syncData[0]->title=$data['title'];
            // $syncData[0]->description=$data['description'];
            //echo 'I am here';
            //print_r(['psid'=>$data['psid'],'asid'=>$data['asid'],'user_id'=>$data['user_id'],'language'=>$data['language'],'form_id'=>$data['form_id'],'title'=>$data['title'],'description'=>$data['description']]);
            //$query = $syncMessengerTbl->query();
            //$result = $query->update()->set(['psid'=>$data['psid'],'asid'=>$data['asid'],'user_id'=>$data['user_id'],'language'=>$data['language'],'form_id'=>$data['form_id'],'title'=>$data['title'],'description'=>$data['description']])->where(['asid' => $fb_id])->execute();
            //$query->updateAll(['asid' => $fb_id],['psid'=>$data['psid'],'asid'=>$data['asid'],'user_id'=>$data['user_id'],'language'=>$data['language'],'form_id'=>$data['form_id'],'title'=>$data['title'],'description'=>$data['description']]);
        //}
    }

    public function viewForm($id=null,$logout = null){ 
        $session = $this->request->session();
        $countries=$this->getCountryCodesWithDialCode();
        //$this->set('countries', $countries);

        $countriesListing = TableRegistry::get("countries");
        $countriesListingData = $countriesListing->find('all')->toArray();
        $this->set('countries', $countriesListingData);


        $salt =  Security::salt();
        $formId     =   $_GET['id'];
        if(isset($_GET['logout']) && $_GET['logout'] == 1){
            session_destroy();
            unset($_POST);
            $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
        }
        $CommingUserListingModel = TableRegistry::get("comming_users");
        $fb_id='';
        $sender_id='';
        if($session->read( 'social.is_login' ) == "google")
        {
            $this->provider = $session->read( 'social.is_login' );
            
            $googleData = $CommingUserListingModel->find('all')->where(['form_id' => $formId,'user_identifier_id' => $session->read('social.identifier')])->toArray();
        
            $this->set('fbUserSubData',$googleData);
        }else if( $identifier = $session->read('fb.identifier')){

            $fb_id = $session->read('fb.identifier');
            $senderRelationData=json_decode(file_get_contents('https://graph.facebook.com/v4.0/'.$fb_id.'?fields=ids_for_pages&access_token=181949048879001|hrwuj4M9BQA-leLSgS1jajgxGgg'));
            $sender_id=isset($senderRelationData->ids_for_pages)?$senderRelationData->ids_for_pages->data[0]->id:'';
            $facebookData = $CommingUserListingModel->find('all')->where(['form_id' => $formId,'fb_email' => $session->read('fb.email')])->toArray();
            $this->set('fbUserSubData',$facebookData);
        }
       
        //$this->set('sss',"testing..");
        $friendsApiUrl = FB_GRAPH_API_HOST .$session->read('fb.identifier').'/?fields=id,name,friends{picture,name},picture&access_token='.$session->read( 'fb.accesstoken' );
        $friendsData = json_decode( $this->getCurl($friendsApiUrl));  
        $this->set('friendsData',$friendsData);
       
       
        if ( ( !empty( $session->read( 'fb.is_login' ) ) ) || ( $session->read( 'fb.is_login' ) != '' ) ){
               $this->getFBData($_GET['id']);
        }
        $FormListingModel = TableRegistry::get("form_listings");    
        $FormSingleData = $FormListingModel->find('all')->contain(['Groups'])->where(['form_id'=>$formId])->first();
    
        $this->viewBuilder()->layout('embed');
        $CommingUsersCount = $CommingUserListingModel->find('all')->where(['form_id' => $formId])->count();
        $this->set('CommingUsersCount',$CommingUsersCount);
    

        // getting youtube video
        $pages = TableRegistry::get('pages');
        $embed = $pages->find('all')->where(['id' => 2])->first();

        $type = $this->videoType($embed['title']);
        if($type == 'youtube'){
            $xplode = explode('watch?v=', $embed['title']);
            $this->set('videoId',$xplode[1]);           
        }else{
            if(preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $embed['title'], $output_array)) {
                if(!empty($output_array[5])){
                    $this->set('videoId', $output_array[5]);
                }
            }           
        }
		
		$selectedLanguage=$this->siteLanguage();
		$form_title_lang=isset($FormSingleData->form_title)?$FormSingleData->form_title:'';
		$form_description_lang=isset($FormSingleData->form_description)?$FormSingleData->form_description:'';
		if($selectedLanguage=='Portuguese'){
			$form_title_lang=isset($FormSingleData->form_title_pt)?$FormSingleData->form_title_pt:'';
			$form_description_lang=isset($FormSingleData->form_description_pt)?$FormSingleData->form_description_pt:'';
		}else if($selectedLanguage=='Spanish'){
			$form_title_lang=isset($FormSingleData->form_title_sp)?$FormSingleData->form_title_sp:'';
			$form_description_lang=isset($FormSingleData->form_description_sp)?$FormSingleData->form_description_sp:'';
		}
        $unique_id='';
        if($fb_id){// && $sender_id!=''
          $unique_id=mt_rand(1111,9999);
          $this->syncMessengerData(['psid'=>$sender_id,'unique_id'=>$unique_id,'asid'=>$fb_id,'user_id'=>'','language'=>$this->siteLanguage(),'form_id'=>$formId,'description'=>$form_description_lang,'title'=>$form_title_lang]);
          //echo $unique_id = str_pad($unique_id, 4, '0', STR_PAD_LEFT);
        }

        /// Send registered page and group info///
        $pageData = $CommingUserListingModel->find('all')->where(['form_id' => $formId,'flag' => 'page'])->toArray();
        $pagesArray = array();
        if(count($pageData) > 0){
            foreach( $pageData as $pg ){
                array_push($pagesArray, $pg->user_identifier_id);
            }
        }
        $this->set('pageData',$pagesArray);
        $groupsData = $CommingUserListingModel->find('all')->where(['form_id' => $formId,'flag' => 'group'])->toArray();

        $groupArray = array();
        if(count($groupsData) > 0){
            foreach( $groupsData as $gp ){
                array_push($groupArray, $gp->user_identifier_id);
            }
        }
        $this->set('groupsData',$groupArray);
        /// Send registered page and group info///

        $this->set('fb_id',$fb_id);
        $this->set('sender_id',$sender_id);
        $this->set('page_id',$FormSingleData->page_id);
		$this->set('form_title_lang',$form_title_lang);
        $this->set('form_id',$formId);
		$this->set('form_description_lang',$form_description_lang);
        $this->set('videoType', $type);
        $this->set('unique_id', $unique_id);
        $this->set('FormListingData',$FormSingleData);

        $this->set('uname',$session->read('CommingUser.name'));
        $this->set('uemail',$session->read('CommingUser.email'));
        $this->set('ucountry',$session->read('CommingUser.country'));
        $this->set('uphone',$session->read('CommingUser.phone'));

        $successUrl = $FormSingleData['success_url'];
        $toEmail = $FormSingleData['to_email'];
        if( empty($toEmail) || empty( $successUrl ) )
        {
            echo __(form_not); exit;
        }
        if(isset($this->request->data)&& !empty($this->request->data)){

            //if target is google          
           $formUsersData = $CommingUserListingModel->find('all')->where(['form_id' => $formId,'fb_email' => $session->read('fb.email')])->toArray();
            if($this->provider == "google"){
                 $formUsersData = $CommingUserListingModel->find('all')->where(['form_id' => $formId,'fb_email' => $session->read('social.email')])->toArray();

                $user_name             = $session->read('CommingUser.name');
                $user_email            = $session->read('CommingUser.email');
                $user_country          = $session->read('CommingUser.country');
                $user_phone            = $session->read('CommingUser.phone');

				
                if($this->broadcastsGoogleFormSubmit($FormSingleData,$CommingUserListingModel,$formId,$formUsersData, $user_name, $user_email, $user_country, $user_phone)){
                    return $this->redirect($this->referer() );
                }
                return $this->redirect($this->referer() );
               
            } 
            if(!empty($formUsersData)){                
                $mainfbFlag = $this->request->data['flag'];
                $mainflagOption = $this->request->data['share'];
            }
       
            if( ( $this->request->data('flag') == '' ) && ( ( $this->request->data('pages') == '' ) && ( $this->request->data('share') == '' ) ) ){
                echo "VOCÃŠ PRECISA SE LOGAR NO FACEBOOK. <a href='" . $_SERVER["HTTP_REFERER"] . "'>Voltar</a>";exit;
            }
         
            $followerCount      = 0;
            $stream_title       = $this->request->data('stream_title');
            $flag               = $this->request->data('flag');
            $stream_description = $this->request->data('stream_description');
            $target_name        = $this->request->data('target_name');
            $fb_email           = $session->read( 'fb.email' );
         

            $stream_description_updated = $stream_description;
            /** If trial sentence not hide then get sentence **/

            $trial_sentence = $this->getTrialSentence($FormSingleData['owner_id']);
            if($trial_sentence){
                $stream_description_updated .= " ".$trial_sentence;
            }

            /*if($FormSingleData->hide_trial_sentence != 1){
                $SentencesModel = TableRegistry::get("sentences");
                $SentenceData = $SentencesModel->find('all')->where(['title'=>'trial_sentence'])->first();
                $this->set('SentenceData',$SentenceData);
                $stream_description_updated .= " ".$SentenceData->content;
            }*/
            $stream_title = $this->cleanString($stream_title);
            $stream_description_updated = $this->cleanString($stream_description_updated);
            
                if( $flag == 'timeline' ) {
                    $option         =   $this->request->data('share');
                    $fb_name        =   $session->read( 'fb.name' );
                    $identifier     =  str_replace( '?fields=cover', '', $session->read( 'fb.identifier' ) );
                    

                    $access_token = $session->read( 'fb.accesstoken' );
                    $result = shell_exec("curl ". FB_GRAPH_API_HOST . $identifier."'/live_videos?access_token=".$access_token."' -H 'Host: graph.facebook.com' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5'  -H 'Content-Type: application/x-www-form-urlencoded' -H 'Referer: https://developers.facebook.com' -H 'origin: https://developers.facebook.com' --data 'debug=all&format=json&method=post&pretty=0&title=".$stream_title."&description=".$stream_description_updated."&privacy=%7B%22value%22%3A%22".$option."%22%7D&suppress_http_code=1'");
                    $data = json_decode($result,true);
                    if( @$data['stream_url'] == '' || @$data['message']){
                        die('I am stop');
                        if (!empty($data['error']['message'])) // facebook meesage passed to user
                        {
                            echo "<h4>".__(facebook_respone)."</h4>";
                            echo $data['error']['message'];
                            echo "</br>";
                        }
                        echo __(no_rtmp)."<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";exit;
                    }
                    
                    $new_stream_url = $data['stream_url'];
                    /* Get Stream Key */
                    $explode_stream = explode('/rtmp/',$new_stream_url);
                    $stream_key = $explode_stream[1];
                    /* ************ */
                    $new_stream_url_secure = $data['secure_stream_url'];
                    $rmtp_stream_id = $data['id'];

                    $image_path = $session->read( 'fb.image');

                    //$target_name = "FB_Timeline_". ucfirst(str_replace(" ","",$fb_name))."_".$identifier;
                    $target_name = "FB_Timeline_". ucfirst(str_replace(" ","",$fb_name));
                    $pattern = '/[^Ø£-ÙŠA-Za-z0-9_ ]/ui';
                    $target_name = preg_replace($pattern, "", $target_name);
                    $target_name = $target_name ."_". $identifier;

                    $followerCount =  $this->get_follower($identifier,$access_token,$flag);


                    if ($FormSingleData->linked_status == '1') {
                        $parentResult = shell_exec("curl ". FB_GRAPH_API_HOST . $identifier."'/live_videos?access_token=".$access_token."' -H 'Host: graph.facebook.com' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5'  -H 'Content-Type: application/x-www-form-urlencoded' -H 'Referer: https://developers.facebook.com' -H 'origin: https://developers.facebook.com' --data 'debug=all&format=json&method=post&pretty=0&title=".$stream_title."&description=".$stream_description_updated."&privacy=%7B%22value%22%3A%22".$option."%22%7D&suppress_http_code=1'");
                        $parentData = json_decode($parentResult,true);
                        $parentnew_stream_url = $parentData['stream_url'];
                        $parent_explode_stream = explode('/rtmp/',$parentnew_stream_url);
                        $parent_stream_key = $parent_explode_stream[1];
                        /* ************ */
                        $parent_new_stream_url_secure = $parentData['secure_stream_url'];
                        $parent_rmtp_stream_id = $parentData['id'];
                    }
                } else if($flag == 'page' || $flag == 'group' || $flag == 'event'){
                    if($flag == 'page'){
                        $option = $this->request->data('pages');

                        $all_pages = $session->read( 'fb.accounts' );
                        
						//Start restrict
						$commingUserSingleDataList =$CommingUserListingModel->find('all')->where(['user_identifier_id'=> $option , 'form_id' =>$formId])->toArray();
						$count_target = count($commingUserSingleDataList);
						
						if($count_target >= 1){
							$session->write('social_selected_error', __(alreadythere));
							$session->write('newNetwork', true);
							return $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
						}
						//End
                        
                        foreach( $all_pages['data'] as $pages ){
                            if( $pages['id'] == $option ){   
                                $fb_name        = $pages['name'];
                                $identifier     = $pages['id'];
                                $access_token   = $pages['access_token'];
                                $page_name      = $pages['name'];
                                break;
                            }
                        }

                        /*$permission = $this->checkTargetPermission($identifier, 'page', $session->read( 'fb.accesstoken' ), $fb_id);
                        if($permission == false){
                            $session->write('social_selected_error', __(no_permission));
                            return $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
                        }*/

                        $result_picture = FB_GRAPH_API_HOST . $identifier.'/?fields=picture&access_token='.$access_token;
                        $imagData = json_decode($this->getCurl($result_picture));

                        $image_path = $imagData->picture->data->url;
                        //$target_name = "FB_Page_". ucfirst(str_replace(" ","", $pages['name'] ))."_".$pages['id'];

                        $target_name = "FB_Page_". ucfirst(str_replace(" ","", $pages['name'] ));
                        $pattern = '/[^Ø£-ÙŠA-Za-z0-9_ ]/ui';
                        $target_name = preg_replace($pattern, "", $target_name);
                        $target_name = $target_name ."_". $pages['id'];

                        $followerCount =  $this->get_follower($identifier,$access_token,$flag);


                    }elseif( $flag == 'group' ) {
                        $option = $this->request->data('groups');
                        $all_groups = $session->read( 'fb.groups' );

						//Start restrict
						$commingUserSingleDataList =$CommingUserListingModel->find('all')->where(['user_identifier_id'=> $option , 'form_id' =>$formId])->toArray();
						$count_target = count($commingUserSingleDataList);
						
						if($count_target >= 1){
							$session->write('social_selected_error', __(alreadythere));
							$session->write('newNetwork', true);
							return $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
						}
						//End

                        foreach($all_groups['data'] as $groups){
                            if($groups['id'] == $option){

                                $group_id = $groups['id'];
                                $identifier = $groups['id'];

                                $access_token = $session->read( 'fb.accesstoken' );
                                $group_name = $groups['name'];
                                $fb_name = $groups['name'];
                            }
                        }

                        // Check target permission
                        /*$permission = $this->checkTargetPermission($identifier, 'group', $access_token, $fb_id);
                        if($permission == false){
                            $session->write('social_selected_error', __(no_permission));
                            return $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
                        }*/


                        $result_picture = FB_GRAPH_API_HOST . $identifier.'/?fields=picture&access_token='.$access_token;
                        $imagData = json_decode($this->getCurl($result_picture));
                        $image_path = $imagData->picture->data->url;
                        //$target_name = "FB_Group_". ucfirst(str_replace(" ","", $group_name ))."_".$group_id;

                        $target_name = "FB_Group_". ucfirst(str_replace(" ","", $group_name ));
                        $pattern = '/[^Ø£-ÙŠA-Za-z0-9_ ]/ui';
                        $target_name = preg_replace($pattern, "", $target_name);
                        $target_name = $target_name ."_". $group_id;

                        $followerCount =  $this->get_follower($identifier,$access_token,$flag);
                        
                    }else{
                        $option = $this->request->data('events');
                        $all_events = $session->read( 'fb.events' );                                    
                        //Start restrict
						$commingUserSingleDataList =$CommingUserListingModel->find('all')->where(['user_identifier_id'=> $option , 'form_id' =>$formId])->toArray();
						$count_target = count($commingUserSingleDataList);
						
						if($count_target >= 1){
							$session->write('social_selected_error', __(alreadythere));
							return $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
						}
						//End						
                        foreach($all_events['data'] as $events){
                            if( $events['id'] == $option )
                            {   
                                $fb_name        = $events['name'];
                                $identifier     = $events['id'];
                                break;
                            }
                        }

                        $access_token = $session->read( 'fb.accesstoken' );

                        // Check target permission
                        /*$permission = $this->checkTargetPermission($identifier, 'event', $access_token, $fb_id);
                        if($permission == false){
                            $session->write('social_selected_error', __(no_permission));
                            return $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
                        }

                        // Check event future
                        $futureEvent = $this->checkFutureEvent($identifier, 'event', $access_token, $fb_id);
                        if($futureEvent){
                            $session->write('social_selected_error', __(future_event_err));
                            return $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
                        }*/


                        $result_picture = FB_GRAPH_API_HOST . $identifier.'/?fields=picture&access_token='.$access_token;
                        $imagData = json_decode($this->getCurl($result_picture));
                        $image_path = $imagData->picture->data->url;
                        //$target_name = "FB_Event_". ucfirst(str_replace(" ","", $events['name'] ))."_".$events['id'];

                        $target_name = "FB_Event_". ucfirst(str_replace(" ","", $events['name'] ));
                        $pattern = '/[^Ø£-ÙŠA-Za-z0-9_ ]/ui';
                        $target_name = preg_replace($pattern, "", $target_name);
                        $target_name = $target_name ."_". $events['id'];

                        $followerCount =  $this->get_follower($identifier,$access_token,$flag);
                    }
                    
                    $result = shell_exec("curl ". FB_GRAPH_API_HOST . $identifier."'/live_videos?access_token=".$access_token."' -H 'Host: graph.facebook.com' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5'  -H 'Content-Type: application/x-www-form-urlencoded' -H 'Referer: https://developers.facebook.com' -H 'origin: https://developers.facebook.com' --data 'debug=all&format=json&method=post&pretty=0&title=".$stream_title."&description=".$stream_description_updated."&suppress_http_code=1'");
    
                    $data = json_decode($result,true);

                    if( @$data['stream_url'] == '' || @$data['message']){

                        if (!empty($data['error']['message'])) // facebook meesage passed to user
                        {
                            if($data['error']['type'] == 'OAuthException'){
                                if($data['error']['code'] == 200){
                                    $session->write('social_selected_error', __(canadmin));
                                }else{
                                    $session->write('social_selected_error', $data['error']['message']);
								}
								$session->write('newNetwork', true);
                                return $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
                            }
                            echo "<h4>".__(facebook_respone)."</h4>";
                            echo $data['error']['message'];
                            echo "</br>";
                        }
                        echo "FB Error...".__(no_rtmp)."<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";exit;
                    }

                    $new_stream_url = $data['stream_url'];
                    /* Get Stream Key */
                    $explode_stream = explode('/rtmp/',$new_stream_url);
                    $stream_key = $explode_stream[1];
                    /* ************ */
                    $new_stream_url_secure = $data['secure_stream_url'];
                    $rmtp_stream_id = $data['id']; 


                    if ($FormSingleData->linked_status == '1') 
                    {
                        $parentResult = shell_exec("curl ". FB_GRAPH_API_HOST . $identifier."'/live_videos?access_token=".$access_token."' -H 'Host: graph.facebook.com' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5'  -H 'Content-Type: application/x-www-form-urlencoded' -H 'Referer: https://developers.facebook.com' -H 'origin: https://developers.facebook.com' --data 'debug=all&format=json&method=post&pretty=0&title=".$stream_title."&description=".$stream_description_updated."&suppress_http_code=1'");
                        $parentData = json_decode($parentResult,true);
                        $parentnew_stream_url = $parentData['stream_url'];
                        $parent_explode_stream = explode('/rtmp/',$parentnew_stream_url);
                        $parent_stream_key = $parent_explode_stream[1];
                        /* ************ */
                        $parent_new_stream_url_secure = $parentData['secure_stream_url'];
                        $parent_rmtp_stream_id = $parentData['id'];
                    }


                    
                }    
                else 
                {

                    $identifier     =  str_replace('?fields=cover','', $session->read( 'fb.identifier' ) );

                    $access_token = $session->read( 'fb.accesstoken' );

                    $result = shell_exec('curl -k -X POST '. FB_GRAPH_API_HOST .$identifier.'/live_videos \
                    -F "access_token='.$access_token.'" \
                    -F "published=true"');
                    $data = json_decode($result,true);
                    
                    if( @$data['stream_url'] == '' || @$data['message']){

                        if (!empty($data['error']['message'])) // facebook meesage passed to user
                        {
                            echo "<h4>".__(facebook_respone)."</h4>";
                            echo $data['error']['message'];
                            echo "</br>";
                        }
                        echo __(no_rtmp)."<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";exit;
                    }

                    $new_stream_url = $data['stream_url'];
                    /* Get Stream Key */
                    $explode_stream = explode('/rtmp/',$new_stream_url);
                    $stream_key = $explode_stream[1];
                    /* ************ */
                    $new_stream_url_secure = $data['secure_stream_url'];
                    $rmtp_stream_id = $data['id'];  

                    if ($FormSingleData->linked_status == '1') 
                    {
                            $parentResult = shell_exec('curl -k -X POST '. FB_GRAPH_API_HOST .$identifier.'/live_videos \
                            -F "access_token='.$access_token.'" \
                            -F "published=true"');

                        $parentData = json_decode($parentResult,true);
                        $parentnew_stream_url = $parentData['stream_url'];
                        $parent_explode_stream = explode('/rtmp/',$parentnew_stream_url);
                        $parent_stream_key = $parent_explode_stream[1];
                        /* ************ */
                        $parent_new_stream_url_secure = $parentData['secure_stream_url'];
                        $parent_rmtp_stream_id = $parentData['id'];
                    }
                }
                if( !empty( $new_stream_url  ) ){                   
                    $target_name = $target_name ."_". time();// make url unique
                    $appData =  $this->getApplicationForUser($FormSingleData->group->id, $target_name, $FormSingleData['wowza_source_name']);            
                    //print_r($appData);        

                    /**** Check plans and set wowza status ****/
                    $enabled = "false";
                    $live_status = $this->checkFormPlans($FormSingleData['owner_id'], $FormSingleData['form_id'], $flag);

                    if($live_status == 1){
                        $enabled = "true";
                    }
                    
                    // check if user is already registered
                    if(!empty( $this->_checkTargetExists($formUsersData,$identifier) )){

                      $commingData = $CommingUserListingModel->find("all")->where(['form_id' => $FormSingleData['form_id'],'flag != ' => 'youtube', 'wowza_stream_status' => 'Error', 'user_identifier_id' => $identifier, 'live_status' => 0])->first();

                    $errorTargetId = 0;

                    if(!empty($commingData)){

                        $errorTargetId = $commingData->id;

                        $addednew = $this->addWowzaStream($appData->app_name, $target_name, $FormSingleData['wowza_source_name'], $new_stream_url_secure, $enabled);

                        if(!$addednew){

                        echo  "WOWZA Error...".__(no_rtmp)."<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";exit;
                        }else{

                        $long_access_token_new  = $this->extendFbToken($access_token ,$type ,$identifier);
                        if ($long_access_token_new) {
                            $access_token_new = $long_access_token_new;
                        }

                        $commingData->target_name       = $target_name;
                        $commingData->access_token = $access_token_new;
                        $commingData->new_stream_url        = $new_stream_url; 
                        $commingData->stream_key            = $stream_key; 
                        $commingData->rmtp_stream_id        = $rmtp_stream_id;
                        $commingData->new_stream_url_secure = $new_stream_url_secure; 
                        $commingData->rtmp_url              = $new_stream_url; 
                        $commingData->wowza_status_message  = 'Success';
                        $commingData->wowza_stream_status   = 'Waiting...';
                        $commingData->live_status           = $live_status;

                        if($CommingUserListingModel->save($commingData)){
							$session->write('successfull',__(activesaved));
							$session->write('newNetwork', true);
                            return $this->redirect( $FormSingleData->success_url );
                        }

                    }

                    }else{
					   $session->write('social_selected_error', __(alreadyconnected));
					   $session->write('newNetwork', true);
                       return $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
                    }

                    }else{
                        $added = $this->addWowzaStream($appData->app_name, $target_name, $FormSingleData['wowza_source_name'], $new_stream_url_secure, $enabled);
                    }
                    if(!$added){

                        echo  "WOWZA Error...".__(no_rtmp)."<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";exit;
                    }else{

                        // check if user is already registered
                        if(!empty( $this->_checkTargetExists($formUsersData,$identifier) )){
                            $commingData = $CommingUserListingModel->find("all")->where(['form_id' => $FormSingleData['form_id'],'flag != ' => 'youtube', 'wowza_stream_status' => 'Error', 'user_identifier_id' => $identifier, 'live_status' => 0])->first();
                    
                            if(empty($commingData)){
							$session->write('social_selected_error', __(alreadyconnected));
							$session->write('newNetwork', true);
                            return $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);

                            }
                        }
                        
                        $FormListing                            = $this->request->data;
                        $CommingUserData                        = $CommingUserListingModel->newEntity();
                        $CommingUserData->form_id               = $formId;

                        // copying image to server from facebook url
                        $image_path_stored = HTTP_ROOT.'img/uploads/stream_images/'.$identifier.'.jpg';
                        copy($image_path, './img/uploads/stream_images/'.$identifier.'.jpg');
                        $image_path = $image_path_stored;

                        //$long_access_token  = $this->extendFbToken($access_token);
						$long_access_token  = $this->extendFbToken($access_token ,$type ,$identifier);
                        if ($long_access_token) {
                            $access_token = $long_access_token;
                        }

                        $CommingUserData->target_name           = $target_name;
                        $CommingUserData->flag                  = $flag; 
                        $CommingUserData->flag_option           = $option;
                        $CommingUserData->app_id                = $appData->app_id;
                        $CommingUserData->group_id              = $FormSingleData->group->id;
                        $CommingUserData->str_src_name          = $FormSingleData->wowza_source_name; 
                        $CommingUserData->stream_title          = $stream_title; 
                        $CommingUserData->stream_description    = $stream_description; 
                        $CommingUserData->access_token          = $access_token; 
                        $CommingUserData->user_identifier_id    = $identifier; 
                        $CommingUserData->new_stream_url        = $new_stream_url; 
                        $CommingUserData->stream_key            = $stream_key; 
                        $CommingUserData->rmtp_stream_id        = $rmtp_stream_id;
                        $CommingUserData->new_stream_url_secure = $new_stream_url_secure; 
                        $CommingUserData->rtmp_url              = $new_stream_url; 
                        $CommingUserData->app_name              = $appData->app_name; 
                        $CommingUserData->fb_email              = $fb_email; 
                        $CommingUserData->fb_name               = $fb_name;
                        $CommingUserData->image_path            = $image_path;
                        $CommingUserData->wowza_status_message  = 'Success';
                        $CommingUserData->followers_updated_on  = getUtcTime();
                        $CommingUserData->live_status           = $live_status;
                        
                        /*new field addition*/
                        $CommingUserData->registerat          = getUtcTime();
                        
                    //#116 - DEV-116 - Get mandatory aditional data at registration forms
                    $CommingUserData->user_name             = $session->read('CommingUser.name');
                    $CommingUserData->user_email            = $session->read('CommingUser.email');
                    $CommingUserData->user_country          = $session->read('CommingUser.country');
                    $CommingUserData->user_phone            = $session->read('CommingUser.phone');
                    

                        if (empty($followerCount)) {
                            $followerCount = 0;
                        }
                                    
                        $CommingUserData->followers             = $followerCount; 

                        unset($FormListing['fb_email']); 
                        unset($FormListing['stream_title']); 
                        unset($FormListing['stream_description']); 
                        unset($FormListing['json_data']); 
                        unset($FormListing['flag']); 
                        unset($FormListing['share']);
                        unset($FormListing['pages']);
                        unset($FormListing['new_stream_url_secure']);
                        unset($FormListing['target_name']);
                        unset($FormListing['events']);
                        unset($FormListing['group_id']);
                        unset($FormListing['groups']);
                        unset($FormListing['agree']);

                        $FormListing = $this->cleanFormData($FormListing);

                        $jsonCommingData=json_encode($FormListing);
                        $CommingUserData->form_data = $jsonCommingData; 
                        if ($CommingUserListingModel->save($CommingUserData)){   
                            if (!empty($CommingUserData)){
                                 $this->changeAutoLiveStatus($CommingUserData->id,1,true);
                                // Send Email notification on target registration
                                $this->form_data_mail($CommingUserData,$FormListing,$FormSingleData);

                                // Send Push notification on target registration
                                $this->send_push_notification($FormSingleData, "Facebook", $fb_name, $flag, $CommingUserData);
                            }

                            /********** if form has a parent form **************/
                            if ($FormSingleData->linked_status == '1') {
                                $dataRespo = explode('?id=', $FormSingleData->linked_to);
                                $parentFormID = $dataRespo[1];
                                
                                $parentFormSingleData = $FormListingModel->find('all')->contain(['Groups'])->where(['form_id'=>$parentFormID])->first();
                                    
                                if (!empty($parentFormSingleData)) {
                                    
                                    $formUsersData = $CommingUserListingModel->find('all')->where(['form_id' => $parentFormID,'fb_email' => $session->read('fb.email')])->toArray();
                                    if (empty( $this->_checkTargetExists($formUsersData,$identifier) ))  // target on parent form already not registerd
                                    {   
                                        //$parentAppData =  $this->getApplicationForUser($parentFormSingleData->group->id);
                                        $parentAppData =  $this->getApplicationForUser($parentFormSingleData->group->id, $target_name, $parentFormSingleData['wowza_source_name']);
                                        $added = $this->addWowzaStream($parentAppData->app_name, $target_name, $parentFormSingleData['wowza_source_name'], $new_stream_url_secure, $enabled);
                                        
                                        $parentCommingUserListingModel = TableRegistry::get("comming_users");
                                        
                                        $parentCommingUserData = $parentCommingUserListingModel->newEntity();

                                        $parentCommingUserData->form_id                 = $parentFormID;
                                        $parentCommingUserData->target_name             = $target_name;
                                        $parentCommingUserData->flag                    = $flag; 
                                        $parentCommingUserData->flag_option             = $option;
                                        $parentCommingUserData->app_id                  = $parentAppData->app_id;
                                        $parentCommingUserData->group_id                = $parentFormSingleData->group->id;
                                        $parentCommingUserData->str_src_name            = $parentFormSingleData->wowza_source_name; 
                                        $parentCommingUserData->stream_title            = $stream_title; 
                                        $parentCommingUserData->stream_description      = $stream_description; 
                                        $parentCommingUserData->access_token            = $access_token; 
                                        $parentCommingUserData->user_identifier_id      = $identifier; 
                                        $parentCommingUserData->new_stream_url          = $parentnew_stream_url; 
                                        $parentCommingUserData->stream_key              = $parent_stream_key; 
                                        $parentCommingUserData->rmtp_stream_id          = $parent_rmtp_stream_id;
                                        $parentCommingUserData->new_stream_url_secure   = $parentnew_stream_url; 
                                        $parentCommingUserData->rtmp_url                = $parentnew_stream_url; 
                                        $parentCommingUserData->app_name                = $parentAppData->app_name; 
                                        $parentCommingUserData->fb_email                = $fb_email; 
                                        $parentCommingUserData->fb_name                 = $fb_name;
                                        $parentCommingUserData->image_path              = $image_path;                                  
                                        $parentCommingUserData->followers               = $followerCount; 
                                        $parentCommingUserData->followers_updated_on    = getUtcTime();
                                        $parentCommingUserData->wowza_status_message    = 'Success';
                                        $parentCommingUserData->live_status             = $live_status;
                                        /*new field addition*/
                                        $parentCommingUserData->registerat             = getUtcTime();


                                        $jsonCommingData=json_encode($FormListing);
                                        $parentCommingUserData->form_data = $jsonCommingData;

                                        if ( $parentCommingUserListingModel->save( $parentCommingUserData ) ) {
                                             $this->changeAutoLiveStatus($parentCommingUserData->id,1,true);
											$session->write('successfull', __(activesaved));
											$session->write('newNetwork', true);
                                            return $this->redirect( $FormSingleData->success_url );
                                        }
                                    }
                                }
                            }
                            /****    if form has a parent form *********************/


							$session->write('successfull',__(activesaved));
							$session->write('newNetwork', true);
                            return $this->redirect( $FormSingleData->success_url );
                        }else{
                            $this->Flash->error(__(savenot));
                            echo __(not_able_create_wowza)." <a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(Try_again)."</a>";exit;
                        }
                    }
                }
        }
    }


    //DEV-116 - Get mandatory aditional data at registration forms
    public function searchSates(){

        $id_country = $this->request->data('id_contry');

        //Get states by country
        $statesListing = TableRegistry::get('states');
        $statesListingData = $statesListing->find('all')
            ->where(['states.country_id =' => $id_country])->order(['states.name' => 'ASC'])->toArray();

        $states = json_encode($statesListingData, JSON_UNESCAPED_UNICODE);

        $this->response->type('json');
        $this->response->body($states);
        return $this->response;

    }

    //DEV-116 - Get mandatory aditional data at registration forms
    public function searchCities(){

        $id_state = $this->request->data('id_state');

        //Get states by country
        $citiesListing = TableRegistry::get('cities');
        $citiesListingData = $citiesListing->find('all')
            ->where(['cities.state_id =' => $id_state])->order(['cities.name' => 'ASC'])->toArray();

        $cities = json_encode($citiesListingData, JSON_UNESCAPED_UNICODE);

        $this->response->type('json');
        $this->response->body($cities);
        return $this->response;

    }
    
    
    //DEV-116 - Get mandatory aditional data at registration forms
    public function saveFormSession(){
        
        $name = $this->request->data('name');
        $email = $this->request->data('email');
        $country = $this->request->data('country');
        $phone = $this->request->data('phone');
       
        $session = $this->request->session();
        
        $session->write('CommingUser.name', $name);
        $session->write('CommingUser.email', $email);
        $session->write('CommingUser.country', $country);
        $session->write('CommingUser.phone', $phone);
        
        
    }

    //function to show success form to users
    function multilikeUserViewSocialnetwork(){
        if(!$this->Auth->user('id')){
          return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
        $session = $this->request->session();
        $userId  =  $this->Auth->user('id');
        $this->viewBuilder()->layout('admin_login');
        $multilikeSocialnetworkModel = TableRegistry::get("tbl_mst_social_network");
        $multilikeSocialnetworkData=$multilikeSocialnetworkModel->find('all')->where(['is_active'=>true])->order(['order_by' => 'ASC'])->toArray();
        $multilikeUserNetworkModel = TableRegistry::get("tbl_users_has_social_networks");
        $multilikeUserNetworkData =$multilikeUserNetworkModel->find('all')->where(['tbl_multilike_user_id'=>$userId])->toArray();
        $existingSocialNetworks=array();
        if(!empty($multilikeUserNetworkData)){
            foreach($multilikeUserNetworkData as $multilikeUserNetwork){
                $existingSocialNetworks[]=$multilikeUserNetwork->tbl_social_network_id;
            }
        }
        $this->set('existingSocialNetworks',$existingSocialNetworks);

        $this->set('social_networks',$multilikeSocialnetworkData);
        $this->set('formId',$userId);
        if(isset($this->request->data)&& !empty($this->request->data)){
          $social_networks=$this->request->data['social_networks'];
          $multilikeUserSocialNetworkModel=TableRegistry::get("tbl_users_has_social_networks");
          $multilikeUserSocialNetworkDeleteData =$multilikeUserSocialNetworkModel->find('all')->where(['tbl_multilike_user_id' => $userId])->toArray();
          if(!empty($multilikeUserSocialNetworkDeleteData)){
              foreach($multilikeUserSocialNetworkDeleteData as $multilikeUserSocialNetworkDelete){
                $multilikeUserSocialNetworkModel->delete($multilikeUserSocialNetworkDelete);
              }
          }
          if(!empty($social_networks)){
            foreach($social_networks as $social_network){
              $multilikeUserSocialNetworkData = $multilikeUserSocialNetworkModel->newEntity();
              $multilikeUserSocialNetworkData->tbl_multilike_user_id=$userId;
              $multilikeUserSocialNetworkData->tbl_social_network_id=$social_network;
              $multilikeUserSocialNetworkModel->save($multilikeUserSocialNetworkData);
            }
          }
          $session->write('successfull',__(update_succ));
          return $this->redirect('/commingusers/multilike-user-view-socialnetwork?id='.$userId);
        }
    }


    //function to return user to thankyou pages
    function multilikeThankyou(){
      $this->viewBuilder()->layout('admin_login');
    }
    //function to show success form to users
    function multilikeViewSocialnetwork(){
        $userId     =   $_GET['id'];
        if($this->Auth->user('id')){
          return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-user-view-socialnetwork?id='.$userId]);
        }
        $session = $this->request->session();
        $this->viewBuilder()->layout('admin_login');
        $multilikeSocialnetworkModel = TableRegistry::get("tbl_mst_social_network");
        $multilikeSocialnetworkData=$multilikeSocialnetworkModel->find('all')->where(['is_active'=>true])->order(['order_by' => 'ASC'])->toArray();

        $multilikeUserNetworkModel = TableRegistry::get("tbl_users_has_social_networks");
        $multilikeUserNetworkData =$multilikeUserNetworkModel->find('all')->where(['tbl_multilike_user_id'=>$userId])->toArray();
        $existingSocialNetworks=array();
        if(!empty($multilikeUserNetworkData)){
            foreach($multilikeUserNetworkData as $multilikeUserNetwork){
                $existingSocialNetworks[]=$multilikeUserNetwork->tbl_social_network_id;
            }
        }
        $this->set('existingSocialNetworks',$existingSocialNetworks);

        $this->set('social_networks',$multilikeSocialnetworkData);
        $this->set('formId',$userId);
        if(isset($this->request->data)&& !empty($this->request->data)){
          $social_networks=$this->request->data['social_networks'];
          $multilikeUserSocialNetworkModel=TableRegistry::get("tbl_users_has_social_networks");
          $multilikeUserSocialNetworkDeleteData =$multilikeUserSocialNetworkModel->find('all')->where(['tbl_multilike_user_id' => $userId])->toArray();
          if(!empty($multilikeUserSocialNetworkDeleteData)){
              foreach($multilikeUserSocialNetworkDeleteData as $multilikeUserSocialNetworkDelete){
                $multilikeUserSocialNetworkModel->delete($multilikeUserSocialNetworkDelete);
              }
          }
          if(!empty($social_networks)){
            foreach($social_networks as $social_network){
              $multilikeUserSocialNetworkData = $multilikeUserSocialNetworkModel->newEntity();
              $multilikeUserSocialNetworkData->tbl_multilike_user_id=$userId;
              $multilikeUserSocialNetworkData->tbl_social_network_id=$social_network;
              $multilikeUserSocialNetworkModel->save($multilikeUserSocialNetworkData);
            }
          }
          $session->write('successfull',__(update_succ));
          return $this->redirect('/commingusers/multilike-thankyou?id='.$userId);
        }
    }

    function multilikeFormLogin(){
      $formId     =   $_GET['id'];
      if($this->Auth->user('id')){
        return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-user-view-socialnetwork?id='.$formId]);
      }
      $this->viewBuilder()->layout('admin_login');
      $this->set('formId',$_GET['id']);
    }
    public function multilikeFormRegistration($id=null,$logout = null){
      $formId     =   $_GET['id'];
      if($this->Auth->user('id')){
        return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-user-view-socialnetwork?id='.$formId]);
      }
      $session = $this->request->session();
      $this->viewBuilder()->layout('admin_login');
      $multilikeFormListingModel = TableRegistry::get("multilike_form_listings");
      $multilikeFormSingleData =$multilikeFormListingModel->find('all')->where(['id'=>$formId])->first();

      $multilikeSocialnetworkModel = TableRegistry::get("tbl_mst_social_network");
      $multilikeSocialnetworkData=$multilikeSocialnetworkModel->find('all')->where(['is_active'=>true])->order(['order_by' => 'ASC'])->toArray();
      //print_r($multilikeFormSingleData);
      $this->set('multilikeFormSingleData',$multilikeFormSingleData);
      $selectedLanguage=$this->siteLanguage();
  		$form_title_lang=isset($multilikeFormSingleData->form_title_en)?$multilikeFormSingleData->form_title_en:'';
  		$form_description_lang=isset($multilikeFormSingleData->form_description_en)?$multilikeFormSingleData->form_description_en:'';
  		if($selectedLanguage=='Portuguese'){
  			$form_title_lang=isset($multilikeFormSingleData->form_title_pt)?$multilikeFormSingleData->form_title_pt:'';
  			$form_description_lang=isset($multilikeFormSingleData->form_description_pt)?$multilikeFormSingleData->form_description_pt:'';
  		}else if($selectedLanguage=='Spanish'){
  			$form_title_lang=isset($multilikeFormSingleData->form_title_sp)?$multilikeFormSingleData->form_title_sp:'';
  			$form_description_lang=isset($multilikeFormSingleData->form_description_sp)?$multilikeFormSingleData->form_description_sp:'';
  		}
  		$this->set('form_title_lang',$form_title_lang);
      $this->set('social_networks',$multilikeSocialnetworkData);
      $this->set('formId',$_GET['id']);
  		$this->set('form_description_lang',$form_description_lang);
      if( empty($toEmail) || empty( $successUrl ) )
      {
          //echo __(form_not); exit;
      }
      if(isset($this->request->data)&& !empty($this->request->data)){
          //print_r($this->request->data);die;
          $postdata=$this->request->data;
          $UsersModel = TableRegistry::get('Users');
          $userEmailExists = $UsersModel->find('all')->where(['email' => $this->request->data['email']])->first();
          if(!empty($userEmailExists)){
            $session->write('social_selected_error', __(verifyEmailErr));
            return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-form-registration?id='.$_GET['id']]);
          }
          /*$userPhoneExists = $UsersModel->find('all')->where(['phone' => $this->request->data['phone']])->first();
          if(!empty($userPhoneExists)){
            $session->write('social_selected_error', __(verifyPhoneErr));
            return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-form-registration?id='.$_GET['id']]);
          }*/
          $multilikeSocialnetworkUserModel = TableRegistry::get("tbl_social_network_user");
          $multilikeSocialnetworkUserEmailExist=$multilikeSocialnetworkUserModel->find('all')->where(['email'=>$postdata['email']])->first();
          if(!empty($multilikeSocialnetworkUserEmailExist)){
            $session->write('social_selected_error', __(verifyEmailErr));
            return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-form-registration?id='.$_GET['id']]);
          }
          /*$multilikeSocialnetworkUserPhoneExist=$multilikeSocialnetworkUserModel->find('all')->where(['phone'=>$postdata['phone']])->first();
          if(!empty($multilikeSocialnetworkUserPhoneExist)){
            $session->write('social_selected_error', __(verifyPhoneErr));
            return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-form-registration?id='.$_GET['id']]);
          }*/

          $register_url = HTTP_ROOT. 'api/registerv2';
          $data = array(
              'username' => $this->request->data['username'],
              'email'=> $this->request->data['email'],
              'password'=> $this->request->data['password'],
              'first_name'=> $this->request->data['first_name'],
              'last_name'=> $this->request->data['last_name'],
              'phone'=> $this->request->data['phone'],
              'language'=> 'Portuguese',
              'is_multilike_user'=>true,
              'multilike_parent_id'=>$multilikeFormSingleData->owner_id,
              'role'=> '3',
              'webservice'=> '1',
              'register_from'=> 'WebApp'
          );
          $headers = array(
              'Content-Type:application/json'
          );
          $result = $this->postCurl($register_url, json_encode($data), $headers);
          $result = json_decode( $result );
          if($result->status == 1) {
            $multilikehaschildmodel=TableRegistry::get("tbl_multilike_user_has_child");
            $multilikehaschildata = $multilikehaschildmodel->newEntity();
            $multilikehaschildata->user_id=$result->data->user_id;
            $multilikehaschildata->form_id=$_GET['id'];
            $multilikehaschildmodel->save($multilikehaschildata);
            $session->write('successfull',__(updaload_success));
            return $this->redirect('/commingusers/multilike-view-socialnetwork?id='.$result->data->user_id);
          }else{
              echo __(Try_again);exit;
          }
      }

    }

    //function to load view for multilike
    public function multilikeViewForm($id=null,$logout = null){
      $formId     =   $_GET['id'];
      if($this->Auth->user('id')){
            // Social logout
            $session = $this->request->session();
            if(!empty($session->read('loginType'))){
            $hybridauth = new \Hybrid_Auth( $this->getConfig());
              $authProvider = $hybridauth->authenticate($provider = $session->read('loginType'));
            $user_profile = $authProvider->logout();
              if($session->read('loginType') == "google"){
            $hybridauth->logoutAllProviders();
              }
            }

        // Webapp logout
          $this->request->session()->delete('AdminUsers');
          $this->request->session()->delete('segmentAdminUsers');
          $this->Auth->logout();
          $session->delete('loginType');
          $session->delete('access_token');
        //return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-user-view-socialnetwork?id='.$formId]);
      }
      $session = $this->request->session();
      $this->viewBuilder()->layout('admin_login');
      $multilikeFormListingModel = TableRegistry::get("multilike_form_listings");
      $multilikeFormSingleData =$multilikeFormListingModel->find('all')->where(['id'=>$formId])->first();

      $multilikeSocialnetworkModel = TableRegistry::get("tbl_mst_social_network");
      $multilikeSocialnetworkData=$multilikeSocialnetworkModel->find('all')->where(['is_active'=>true])->order(['order_by' => 'ASC'])->toArray();
      //print_r($multilikeFormSingleData);
      $this->set('multilikeFormSingleData',$multilikeFormSingleData);
      $successUrl = $multilikeFormSingleData['success_url'];
      $toEmail = $multilikeFormSingleData['to_email'];
      $selectedLanguage=$this->siteLanguage();
  		$form_title_lang=isset($multilikeFormSingleData->form_title_en)?$multilikeFormSingleData->form_title_en:'';
  		$form_description_lang=isset($multilikeFormSingleData->form_description_en)?$multilikeFormSingleData->form_description_en:'';
  		if($selectedLanguage=='Portuguese'){
  			$form_title_lang=isset($multilikeFormSingleData->form_title_pt)?$multilikeFormSingleData->form_title_pt:'';
  			$form_description_lang=isset($multilikeFormSingleData->form_description_pt)?$multilikeFormSingleData->form_description_pt:'';
  		}else if($selectedLanguage=='Spanish'){
  			$form_title_lang=isset($multilikeFormSingleData->form_title_sp)?$multilikeFormSingleData->form_title_sp:'';
  			$form_description_lang=isset($multilikeFormSingleData->form_description_sp)?$multilikeFormSingleData->form_description_sp:'';
  		}
  		$this->set('form_title_lang',$form_title_lang);
      $this->set('social_networks',$multilikeSocialnetworkData);
      $this->set('formId',$formId);
  		$this->set('form_description_lang',$form_description_lang);
      if( empty($toEmail) || empty( $successUrl ) )
      {
          //echo __(form_not); exit;
      }
      if(isset($this->request->data)&& !empty($this->request->data)){
          //print_r($this->request->data);die;
          $postdata=$this->request->data;
          $UsersModel = TableRegistry::get('Users');
          $userEmailExists = $UsersModel->find('all')->where(['email' => $this->request->data['email']])->first();
          if(!empty($userEmailExists)){
            $session->write('social_selected_error', __(verifyEmailErr));
            return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-view-form','id'=>$formId]);
          }
          $userPhoneExists = $UsersModel->find('all')->where(['phone' => $this->request->data['phone']])->first();
          if(!empty($userPhoneExists)){
            $session->write('social_selected_error', __(verifyPhoneErr));
            return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-view-form','id'=>$formId]);
          }
          $multilikeSocialnetworkUserModel = TableRegistry::get("tbl_social_network_user");
          $multilikeSocialnetworkUserEmailExist=$multilikeSocialnetworkUserModel->find('all')->where(['email'=>$postdata['email']])->first();
          if(!empty($multilikeSocialnetworkUserEmailExist)){
            $session->write('social_selected_error', __(verifyEmailErr));
            return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-view-form','id'=>$formId]);
          }
          $multilikeSocialnetworkUserPhoneExist=$multilikeSocialnetworkUserModel->find('all')->where(['phone'=>$postdata['phone']])->first();
          if(!empty($multilikeSocialnetworkUserPhoneExist)){
            $session->write('social_selected_error', __(verifyPhoneErr));
            return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-view-form','id'=>$formId]);
          }

          $user = $multilikeSocialnetworkUserModel->newEntity();
          $user->email=$postdata['email'];
          $appArr = explode('@',$postdata['email']);
          $appName = clean($appArr[0]);
          $user->name=$appName;
          $user->phone=$postdata['phone'];
          $user->country_code=$postdata['country'];
          $user->tbl_multilike_form_id=$formId;
          //$user->password=base64_encode($postdata['password']);
          if($multilikeSocialnetworkUserModel->save($user)) {
            $social_networks=$postdata['social_networks'];
            if(!empty($social_networks)){
              $multilikeUserSocialNetworkModel=TableRegistry::get("tbl_multilike_users_has_social_networks");
              foreach($social_networks as $social_network){
                $multilikeUserSocialNetworkData = $multilikeSocialnetworkUserModel->newEntity();
                $multilikeUserSocialNetworkData->tbl_multilike_user_id=$user->id;
                $multilikeUserSocialNetworkData->tbl_social_network_id=$social_network;
                $multilikeUserSocialNetworkModel->save($multilikeUserSocialNetworkData);
              }
            }
            $userEncodedId = base64_encode($user->id);
            $replace = array('{user}','{link}');
            $link = HTTP_ROOT.'users/multilike-verify-email/'.$userEncodedId;
            $linkOnMail = '<a href="'.$link.'" target="_blank">'.__(click_here_to_verify_email).' </a>';
            $data = array( 'email' => $user->email, 'subject' => __(VERIFY_EMAIL_SUBJECT), 'name' => $user->name, 'email_heading' => __(VERIFY_EMAIL_SUBJECT), 'message' => $linkOnMail );
            $this->send_email_html($data,'verify_email');
            $session->write('successfull',__(updaload_success));
            return $this->redirect('/commingusers/multilike-view-socialnetwork?id='.$user->id);
          }else{
              echo __(Try_again);exit;
          }
      }

    }

    function checkFormPlans($owner_id, $formId, $flag){
        /*
        * Get User plan details
        * Get already exists flag (timeline, page, group, event) count
        * Get total active target count
        * Check count is greater or less from user plan details
        */
        $live_status = 0;
        $CommingUserListingModel = TableRegistry::get("comming_users");

        $date = date("Y-m-d");
        $UserPlansModel = TableRegistry::get("user_plans");
        $totalTargetLimit = 0;
        $peg_limit = 0;
        $PlanData = $UserPlansModel->find('all')->where(['user_id' => $owner_id, 'DATE(start_date) <=' => $date, 'DATE(expiry_date) >=' => $date, 'is_expired'=> 0])->toArray();
        if(count($PlanData) > 0){
            foreach($PlanData as $plan){
                $totalTargetLimit += $plan['target_limit'];
                //$advancedPlanNameArr = array('Trial', 'Unlimited', 'Multi 50', 'Multi PLUS 10', 'Multi PLUS 25', 'Multi PLUS 50', 'Multi PLUS 1 Extra'); 
                $advancedPlanNameArr = array('Trial', 'Unlimited', 'Customize', 'Multi 10', 'Multi 20', 'Multi 30'); 
                if(in_array($plan['product_name'], $advancedPlanNameArr)){
                    if($plan['product_name'] == 'Multi 50'){
                        $peg_limit += 1;
                    }else{
                        $peg_limit += $plan['target_limit'];
                    }
                }
            }           
        }

        $totalActiveCount = $CommingUserListingModel->find('all')->where(['form_id' => $formId, 'live_status'=> 1])->count();

        if($totalActiveCount >= $totalTargetLimit || $totalTargetLimit == 0){
            $live_status = 0;
            //$this->limitexceeds($formId);
        }else{
            if($flag == 'timeline'){
                $live_status = 1; 
            }else if($flag == 'page' || $flag == 'event' || $flag == 'group' || $flag == 'youtube'){
                $allFlagCount = $CommingUserListingModel->find('all')->where(['form_id' => $formId, 'flag IN'=> ['page', 'event', 'group', 'youtube'], 'live_status'=> 1])->count();
                if($allFlagCount >= $peg_limit || $peg_limit == 0){
                    $live_status = 0;
                    // $this->limitexceeds($formId);
                }else{
                    $live_status = 1;
                }
            }
        }

        return $live_status;
    }

    function cleanFormData($formData='')
    {
        if (!empty($formData)) {    
        foreach ($formData as $key => $value) {
            if (is_array($value)) {
                $returnArray[$key] = implode(', ', $value);
            }else{
                $returnArray[$key] = $value;
            }
        }}
        return $returnArray;
    }

    function limitexceeds($formId){
        //$formId = $_GET['id'];

        $FormListingModel = TableRegistry::get("form_listings");    
        $FormSingleData = $FormListingModel->find('all')->contain(['Groups'])->where(['form_id'=>$formId])->first();
        $oldLang = $this->changeLanguageForEmail($FormSingleData->owner_id);
        $message = __(Missed_registration).':<br />
        '.__(Account).': <a href="https://multiplierapp.com.br/app/formbuilder/view-user?id='.$formId.'">'.$FormSingleData->group['group_name'].'</a><br/>'.__(bodymsglimit).':<br/><br/>
        <a href="https://multiplierapp.com.br/app/formbuilder/view-user?id='.$formId.'" style="padding: 6px; background: #ff6600; color: #FFF; text-decoration: none;">'.__(Delete_some_registered).'</a>
        <a href="https://multiplierapp.com.br/loja/" style="padding: 6px; background: #4CAF50; color: #FFF; text-decoration: none;">'.__(Upgrade_you_plan).'</a>';

        $arr = explode("@" , $FormSingleData->to_email);
        //$name = ucwords(str_replace("_", " ", $arr[0]));
        $name = $this->getUsername($FormSingleData->owner_id);

        $data = array( 'email' => $FormSingleData->to_email, 'subject' => __(limit_subject), 'email_heading' => __(limit_subject),'name' => $name, 'message' => $message );
        $response = $this->send_email_html($data,'limit_reached');
        $this->changeLanguageAfterEmail($oldLang);

        return $response;       
    }

    function send_push_notification($FormListingData, $sourceType, $targetName, $targetType, $CommingUserData = ''){
        $oldLang = $this->changeLanguageForEmail($FormListingData['owner_id']);
        if($sourceType == "Facebook"){
            $message = __(new_facebook_register_notification, [$targetName, $targetType]);

            $messages = $this->getAllTypeMessages(new_facebook_register_notification, $targetName, $targetType);

        }else if($sourceType == "Youtube"){
            $message = __(new_youtube_register_notification, [$targetName]);
            $messages = $this->getAllTypeMessages(new_youtube_register_notification, $targetName);
		}
        $image = "";
        if($CommingUserData->id){
            
            $CommingUsers = TableRegistry::get("comming_users");
            $comingUserData = $CommingUsers->find('all')->where( [ 'id' => $CommingUserData->id])->first();
            if($comingUserData->image_path){
                $image = $comingUserData->image_path;
            }
        }
        
        //$this->sendPush($FormListingData['owner_id'], 'New Registration', $message, $messages, TARGET_REGISTER_TYPE, $CommingUserData->id);
		$this->sendPush($FormListingData['owner_id'], __(newregistration), $message, $messages, TARGET_REGISTER_TYPE, $CommingUserData->id, $image, $langMessages, $type);

        $this->changeLanguageAfterEmail($oldLang);
    }

    function form_data_mail($CommingUserData='',$FormListing='',$FormListingData){
        $oldLang = $this->changeLanguageForEmail($FormListingData['owner_id']);
        $message = "";
        $formId = $FormListingData['form_id'];
        $form_body = $FormListingData['form_body'];
        $message.= $this->decode_form_message($form_body,$FormListing,$CommingUserData); // creating form fields
        $name = $this->getUsername($FormListingData['owner_id']);
        $emails = explode(',', $FormListingData['to_email']);

        /*if(count($emails) == 1){
            $arr = explode("@" , $emails[0]);
            $name = ucwords(str_replace("_", " ", $arr[0]));
        }else{
            $name = "";
        }*/

        $data = array( 'email' => $emails, 'subject' => __(New_social_account_registration), 'name' => $name, 'email_heading' => __(New_social_account_registration), 'message' => $message );
        $response = $this->send_email_html($data,'form_submited');
        $this->changeLanguageAfterEmail($oldLang);
    }

    function decode_form_message($form_body,$form_data,$CommingUserData=''){
        $groupsModel = TableRegistry::get('Groups');
        if(is_array($CommingUserData)){
            $group = $groupsModel->find('all')->where( [ 'id' => $CommingUserData['group_id'] ])->first();

            $form_body_enoceded = json_decode(json_decode($form_body), true);
            $message = '';
            
            $session = $this->request->session();
            //$message .= '<br /><strong>Name:</strong> ' . $session->read( 'fb.name' );
            $message .= '<br /><strong>'.__(name).':</strong> ' . $CommingUserData['fb_name'];
            //$message .= '<br /><strong>Your account:</strong> ' . '<a href="'.HTTP_ROOT.'applications/index">'. $group->group_name . '</a>';
            $message .= '<br /><strong>'.__(Account).':</strong> ' . '<a href="'.HTTP_ROOT.'applications/index">'. $group->group_name . '</a>';
            if($CommingUserData['flag'] != "youtube"){
                 $message .= '<br /><strong>'.ucfirst($CommingUserData['flag']) .':</strong> ' . "<a href='https://facebook.com/".$CommingUserData['user_identifier_id']."'>".$CommingUserData['fb_name']."</a>";
            }else{
                 $message .= '<br /><strong>'.ucfirst($CommingUserData['flag']) .':</strong> ' . "<a href='https://www.youtube.com/channel/".$CommingUserData['channelId']."'>".$CommingUserData['fb_name']."</a>";
            }
           
            if(!empty($form_data)){
                foreach ($form_data as $key => $value) 
                {
                    if($key == "groups" || $key == "pages" || $key == "events" || $key == "wowza_aplication_id" || $key == "wowza_aplication" || $key == "wowza_stream_source"){
                          continue;
                    }
                    $label = $form_body_enoceded[array_search($key, array_column($form_body_enoceded, 'name'))]['label'];  // getting form lable on the bases of field submitter by user
                  if($value !="" && $label != ""){
                    $message .= '<br /><strong>'.ucfirst($label) .':</strong> ' . $value;
                  }
                    
                }
            }
        }else{
        $group = $groupsModel->find('all')->where( [ 'id' => $CommingUserData->group_id ])->first();

        $form_body_enoceded = json_decode(json_decode($form_body), true);
        $message = '';
        
        $session = $this->request->session();
        //$message .= '<br /><strong>Name:</strong> ' . $session->read( 'fb.name' );
        $message .= '<br /><strong>'.__(name).':</strong> ' . $session->read( 'fb.name' );
        //$message .= '<br /><strong>Your account:</strong> ' . '<a href="'.HTTP_ROOT.'applications/index">'. $group->group_name . '</a>';
        $message .= '<br /><strong>'.__(Account).':</strong> ' . '<a href="'.HTTP_ROOT.'applications/index">'. $group->group_name . '</a>';
        if($CommingUserData->flag != "youtube"){
             $message .= '<br /><strong>'.ucfirst($CommingUserData->flag) .':</strong> ' . "<a href='https://facebook.com/".$CommingUserData->user_identifier_id."'>".$CommingUserData->fb_name."</a>";
        }else{
             $message .= '<br /><strong>'.ucfirst($CommingUserData->flag) .':</strong> ' . "<a href='https://www.youtube.com/channel/".$CommingUserData->channelId."'>".$CommingUserData->fb_name."</a>";
        }
       
        if(!empty($form_data)){
            foreach ($form_data as $key => $value) 
            {
                if($key == "groups" || $key == "pages" || $key == "events" || $key == "wowza_aplication_id" || $key == "wowza_aplication" || $key == "wowza_stream_source"){
                      continue;
                }
                $label = $form_body_enoceded[array_search($key, array_column($form_body_enoceded, 'name'))]['label'];  // getting form lable on the bases of field submitter by user
              if($value !="" && $label != ""){
                $message .= '<br /><strong>'.ucfirst($label) .':</strong> ' . $value;
              }
                
            }
        }
             
        }
        return $message; 
    }
    
    /*function decode_form_message($form_body,$form_data,$CommingUserData='')
    {
        $groupsModel = TableRegistry::get('Groups');
        $group = $groupsModel->find('all')->where( [ 'id' => $CommingUserData->group_id ])->first();

        $form_body_enoceded = json_decode(json_decode($form_body), true);
        $message = '';
        
        $session = $this->request->session();
        //$message .= '<br /><strong>Name:</strong> ' . $session->read( 'fb.name' );
        $message .= '<br /><strong>Nome:</strong> ' . $session->read( 'fb.name' );
        //$message .= '<br /><strong>Your account:</strong> ' . '<a href="'.HTTP_ROOT.'applications/index">'. $group->group_name . '</a>';
        $message .= '<br /><strong>Sua conta:</strong> ' . '<a href="'.HTTP_ROOT.'applications/index">'. $group->group_name . '</a>';
        $message .= '<br /><strong>'.ucfirst($CommingUserData->flag) .':</strong> ' . "<a href='https://facebook.com/".$CommingUserData->user_identifier_id."'>".$CommingUserData->fb_name."</a>";
        if(!empty($form_data)){
            foreach ($form_data as $key => $value) 
            {
                if($key == "groups" || $key == "pages" || $key == "events" || $key == "wowza_aplication_id" || $key == "wowza_aplication" || $key == "wowza_stream_source"){
                      continue;
                }
                $label = $form_body_enoceded[array_search($key, array_column($form_body_enoceded, 'name'))]['label'];  // getting form lable on the bases of field submitter by user
                $message .= '<br /><strong>'.ucfirst($label) .':</strong> ' . $value;
            }
        }
        return $message; 
    }*/

    public function addWowzaFbTarget($value='')
    {
        $this->viewBuilder()->layout('embed');
    }

    public function editCommon($id = '',$sid = ''){
         $session = $this->request->session();
        if(  !empty( $id = $_GET['id'] )  && !empty( $fid = $_GET['fid']  ) )
        {
            $CommingUsers = TableRegistry::get("comming_users");
            $editStreamData = $CommingUsers->find('all')->where( [ 'id' => $id, 'form_id' => $fid ])->first();
            $FormListing = TableRegistry::get("form_listings");
            $FormListingData=$FormListing->find('all')->where(['form_id'=> $fid ])->first();
			$session->write('newNetwork', true);
            
            $this->set('formListingData',$FormListingData);
            $this->set('editStreamData',$editStreamData);

            if( isset( $this->request->data ) && !empty( $this->request->data ) )
            {   
                $stream_title       = $this->request->data('stream_title');
                $stream_description = $this->request->data('stream_description');
                $flag = $editStreamData['flag'];
                $response = $this->googleSteamRegister($stream_title,$stream_description,$editStreamData->flag_option,$fid,$editStreamData->access_token,$edit = true);
                if(!$response){
                      $this->redirect(HTTP_ROOT.'Commingusers/view-form?id='.$editStreamData['form_id'] . '&s=1');
                }
                $updateCommingUsers = $CommingUsers->newEntity();
                $updateCommingUsers->stream_title = $stream_title;
                $updateCommingUsers->stream_description = $stream_description;
               
                $updateCommingUsers->id = $id;
                if($CommingUsers->save($updateCommingUsers) )
                {
                    $session->write('successfull', __(activesaved));
                    $this->redirect(HTTP_ROOT.'Commingusers/view-form?id='.$editStreamData['form_id'] . '&s=1');
                    header('location:'.HTTP_ROOT.'Commingusers/view-form?id='.$editStreamData['form_id'] . '&s=1');
                    echo "<script>location.href = '" .HTTP_ROOT.'Commingusers/view-form?id='.$editStreamData['form_id'] . "&s=1';</script>";
                                //echo $editStreamData['app_name'].' '.$editStreamData['str_src_name'].' '.$new_stream_url;
                                die(__(Redirect_issue));
                } 
                else 
                {
                    echo __(db_update_error);exit;
                }
              
            }
            $this->viewBuilder()->layout('embed');
            $this->render('edit_stream');
        }

    }

    /*
    * This function is for register youtube broadcast
    * @CreatedBy gourav soni
    * @Param string $streamTitle, $streamDescription
    */
    private function googleSteamRegister($streamTitle,$streamDescription,$streamPrivacy,$formId,$accesstoken,$edit = false,$getChannelName = false){
        $response = array();
        $session = $this->request->session();

        // Set title description when empty
        $FormListing = TableRegistry::get("form_listings");
        $FormListingData=$FormListing->find('all')->where(['form_id'=> $formId ])->first();
        
        $ac = ($session->read( 'social.accesstoken') != "") ? $session->read( 'social.accesstoken') : $accesstoken;
        $option = array(
            //'part' => 'id,snippet,contentDetails,status', 
            'part' => 'snippet,contentDetails,status',
            'mine' => 'true',
            'broadcastType' => 'persistent'// add all instead of persistent
        );
        $headerArr = array('Authorization: Bearer '.$ac);
        $postBroadcastResponse = $this->getCurl('https://www.googleapis.com/youtube/v3/liveBroadcasts?'.http_build_query($option, 'a', '&'),$headerArr);
        $broadcastOp = json_decode($postBroadcastResponse);



         ///write log for youtube api limit reach//////
        $log_data_new = array('controller' => 'Comminguser','functionName' => 'googleSteamRegister', 'apiName' =>  'https://www.googleapis.com/youtube/v3/liveBroadcasts' );
        $textdata = PHP_EOL. date("Y-m-d H:i:s")."-> Data -> ". print_r($log_data_new, true);
        $my_file = fopen("../webroot/youtube_limit_reach.txt", "a") or die("Unable to open file!");
        fwrite($my_file, $textdata);        
        fclose($my_file);
        ///write log for youtube api limit reach//////

        if(isset($broadcastOp->error) ){

            ///write log for youtube api response//////
            $log_data_new = array('controller' => 'Comminguser','functionName' => 'googleSteamRegister', 'apiName' =>  'https://www.googleapis.com/youtube/v3/liveBroadcasts','accesstoken' => $ac,'formId' => $formId,'refToken' => $session->read( 'social.reftoken'),'response' => $broadcastOp ,'error' => $broadcastOp->error->errors[0]->message);

            $textdata = PHP_EOL. date("Y-m-d H:i:s")."-> Data -> ". print_r($log_data_new, true);
            $my_file = fopen("../webroot/target_registration_response.txt", "a") or die("Unable to open file!");
            fwrite($my_file, $textdata);        
            fclose($my_file);
            ///write log for youtube api response//////

            if($broadcastOp->error->errors[0]->reason == 'dailyLimitExceeded' && !$edit){
                $session->write('social_selected_error', "Daily Limit Exceeded.");
                return false;
            }else{
                if(!$edit)
                //$this->commonLogOut();
                $session->write('social_selected_error',$broadcastOp->error->errors[0]->message);

                return false;
            }
        }

        // Check title and description are blank
        if((isset($broadcastOp->items[0]->snippet->description) && ($broadcastOp->items[0]->snippet->description == "") ) || ( isset($broadcastOp->items[0]->snippet->title) && ($broadcastOp->items[0]->snippet->title == "") ) )
        {
            if(!$edit)
            //$this->commonLogOut();
            $session->write('social_selected_error','required_t_n_d');
            return false;
        }

        //echo "<pre>1"; print_r($broadcastOp);
        $boundStreamId = $broadcastOp->items[0]->contentDetails->boundStreamId;
        $response['broadCastId'] = $broadcastOp->items[0]->id;
        $response['imageUrl'] = $broadcastOp->items[0]->snippet->thumbnails->high->url;
        $response['channelId'] = $broadcastOp->items[0]->snippet->channelId;

        if($broadcastOp->items[0]->snippet->title != ""){
            $streamTitle = $broadcastOp->items[0]->snippet->title;
        }

        if($broadcastOp->items[0]->snippet->description != ""){
            $streamDescription = $broadcastOp->items[0]->snippet->description;
        }

        $usrModel = TableRegistry::get("users");
        $usrDt = $usrModel->find('all')->where(['id'=>$FormListingData['owner_id']])->first();
        
        if($streamTitle == ""){
          $streamTitle = $usrDt->name." Live";
        }

        if($streamDescription == ""){
          $streamDescription =  $usrDt->name." Live";
        }

        $option = array(
            'part' => 'snippet,statistics', 
            'id' => $response['channelId']
        );
        $channelInfo = $this->getCurl('https://www.googleapis.com/youtube/v3/channels?'.http_build_query($option, 'a', '&'),$headerArr);

        ///write log for youtube api limit reach//////
        $log_data_new = array('controller' => 'Comminguser','functionName' => 'googleSteamRegister', 'apiName' =>  'https://www.googleapis.com/youtube/v3/channels' );
        $textdata = PHP_EOL. date("Y-m-d H:i:s")."-> Data -> ". print_r($log_data_new, true);
        $my_file = fopen("../webroot/youtube_limit_reach.txt", "a") or die("Unable to open file!");
        fwrite($my_file, $textdata);        
        fclose($my_file);
        ///write log for youtube api limit reach//////

        $channelInfo = json_decode($channelInfo);
        $response['channelName'] = $channelInfo->items[0]->snippet->title;
        if($getChannelName == true){
            return $response['channelName'];
        }
        $response['channelSub'] = str_replace(',',"",$channelInfo->items[0]->statistics->subscriberCount);//$channelInfo->items[0]->statistics->subscriberCount;
        $option = array(
            'part' => 'snippet,cdn', 
            'id' => $boundStreamId
        );
        $postStreamResponse = $this->getCurl('https://www.googleapis.com/youtube/v3/liveStreams?'.http_build_query($option, 'a', '&'),$headerArr);
        $streamOp = json_decode($postStreamResponse);

        ///write log for youtube api limit reach//////
        $log_data_new = array('controller' => 'Comminguser','functionName' => 'googleSteamRegister', 'apiName' =>  'https://www.googleapis.com/youtube/v3/liveStreams' );
        $textdata = PHP_EOL. date("Y-m-d H:i:s")."-> Data -> ". print_r($log_data_new, true);
        $my_file = fopen("../webroot/youtube_limit_reach.txt", "a") or die("Unable to open file!");
        fwrite($my_file, $textdata);        
        fclose($my_file);
        ///write log for youtube api limit reach//////

       // echo "<pre>2"; print_r($streamOp);
        $response['rtmpUrl'] = $streamOp->items[0]->cdn->ingestionInfo->ingestionAddress;
        $response['streamName'] = $streamOp->items[0]->cdn->ingestionInfo->streamName;
        $response['streamId'] = $streamOp->items[0]->id;
        if(isset($streamOp->error) ){

            ///write log for youtube api response//////
            $log_data_new = array('controller' => 'Comminguser','functionName' => 'googleSteamRegister', 'apiName' =>  'https://www.googleapis.com/youtube/v3/liveStreams','accesstoken' => $ac,'formId' => $formId,'refToken' => $session->read( 'social.reftoken'),'response' => $streamOp ,'error' => $streamOp->error->errors[0]->message);

            $textdata = PHP_EOL. date("Y-m-d H:i:s")."-> Data -> ". print_r($log_data_new, true);
            $my_file = fopen("../webroot/target_registration_response.txt", "a") or die("Unable to open file!");
            fwrite($my_file, $textdata);        
            fclose($my_file);
            ///write log for youtube api response//////

            if(!$edit)
            //$this->commonLogOut();
            $session->write('social_selected_error',$streamOp->error->errors[0]->message);
            return false;
        }

        // Set date//
        $scheduledate = $broadcastOp->items[0]->snippet->scheduledStartTime;
          $schedule_ex = explode(':',$scheduledate);

          
          if(strlen($schedule_ex[2]) == 3){
              $scheduledStartTime = substr($schedule_ex[2],0,2).".000Z";
              $scheduledStartTime = $schedule_ex[0].":".$schedule_ex[1].":".$scheduledStartTime;
          }else{
              $scheduledStartTime = $broadcastOp->items[0]->snippet->scheduledStartTime;
          }

          $publishdate = $broadcastOp->items[0]->snippet->publishedAt;
          $publish_ex = explode(':',$publishdate);
          
          if(strlen($publish_ex[2]) == 3){
              $publishedAt = substr($publish_ex[2],0,2).".000Z";
              $publishedAt = $publish_ex[0].":".$publish_ex[1].":".$publishedAt;
          }else{
              $publishedAt = $broadcastOp->items[0]->snippet->publishedAt;
            }

        $liveBroadcast->id = $response['broadCastId'];
        $liveBroadcast->snippet['title']         = $streamTitle;
        $liveBroadcast->snippet['scheduledStartTime']         = $scheduledStartTime;
        $liveBroadcast->status['privacyStatus']         = $streamPrivacy;
        $liveBroadcast->snippet['publishedAt']         =  $publishedAt;
        $liveBroadcast->snippet['description']         = $streamDescription;
        $liveBroadcast->contentDetails['enableEmbed']         = false;
        $liveBroadcast->contentDetails['enableDvr']         = true;
        $liveBroadcast->contentDetails['recordFromStart']         = true;
        $liveBroadcast->contentDetails['enableContentEncryption']         = true;
        $liveBroadcast->contentDetails['startWithSlate']         = true;
        $liveBroadcast->contentDetails['monitorStream']['enableMonitorStream'] =  true;
        $liveBroadcast->contentDetails['monitorStream']['broadcastStreamDelayMs'] =  0;
        $postParms = json_encode($liveBroadcast);

        $headerArr = array('Content-Type: application/json','Authorization: Bearer '.$ac);
        $postBroadcastResponse = $this->putCurl('https://www.googleapis.com/youtube/v3/liveBroadcasts?part=id,snippet,contentDetails,status',$postParms,$headerArr);
        $broadcastOp = json_decode($postBroadcastResponse);
        if(isset($broadcastOp->error) ){

             ///write log for youtube api response//////
            $log_data_new = array('controller' => 'Comminguser','functionName' => 'googleSteamRegister', 'apiName' =>  'https://www.googleapis.com/youtube/v3/liveBroadcasts/post','accesstoken' => $ac,'formId' => $formId,'refToken' => $session->read( 'social.reftoken'),'response' => $broadcastOp ,'error' => $broadcastOp->error->errors[0]->message);

            $textdata = PHP_EOL. date("Y-m-d H:i:s")."-> Data -> ". print_r($log_data_new, true);
            $my_file = fopen("../webroot/target_registration_response.txt", "a") or die("Unable to open file!");
            fwrite($my_file, $textdata);        
            fclose($my_file);
            ///write log for youtube api response//////

            if(!$edit)
            $this->commonLogOut();
            $session->write('social_selected_error',$broadcastOp->error->errors[0]->message);
            return false;
        }
        return $response;      
    }

    public function editStream($id='', $sid='')
    {
        $this->viewBuilder()->layout('embed');
         $session = $this->request->session();

        if(  !empty( $id = $_GET['id'] )  && !empty( $fid = $_GET['fid']  ) )
        {
            $CommingUsers = TableRegistry::get("comming_users");
            $editStreamData = $CommingUsers->find('all')->where( [ 'id' => $id, 'form_id' => $fid ])->first();
            $FormListing = TableRegistry::get("form_listings");
            $FormListingData=$FormListing->find('all')->where(['form_id'=> $fid ])->first();
           
            
            $this->set('formListingData',$FormListingData);
            $this->set('editStreamData',$editStreamData);

            if( isset( $this->request->data ) && !empty( $this->request->data ) )
            {   
                $stream_title       = $this->request->data('stream_title');
                $stream_description = $this->request->data('stream_description');
                $flag = $editStreamData['flag'];

                if($flag == 'timeline') 
                {
                    $option = $editStreamData['flag_option'];
                    $identifier =  trim($editStreamData['user_identifier_id']);
                    $access_token = $editStreamData['access_token'];
                        $result = shell_exec("curl " . FB_GRAPH_API_HOST . $identifier."'/live_videos?access_token=".$access_token."' -H 'Host: graph.facebook.com' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5'  -H 'Content-Type: application/x-www-form-urlencoded' -H 'Referer: https://developers.facebook.com' -H 'origin: https://developers.facebook.com' --data 'debug=all&format=json&method=post&pretty=0&title=".$stream_title."&description=".$stream_description."&privacy=%7B%22value%22%3A%22".$option."%22%7D&suppress_http_code=1'");
                    $data = json_decode($result,true);
                    if($data['stream_url'] == '' || $data['message']){

                        if (!empty($data['error']['message'])) // facebook meesage passed to user
                        {
                            echo "<h4>".__(facebook_respone)."</h4>";
                            echo $data['error']['message'];
                            echo "</br>";
                        }
                        echo __(no_rtmp)."<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";exit;

                    }
                    $new_stream_url = $data['stream_url'];
                    /* Get Stream Key */
                    $explode_stream = explode('/rtmp/',$new_stream_url);
                    $stream_key = $explode_stream[1];
                    /* ************ */
                    $new_stream_url_secure = $data['secure_stream_url'];
                    $rmtp_stream_id = $data['id'];      
                    
                } 
                else if($flag == 'page' || $flag == 'group') 
                {
                    $option = $editStreamData['flag_option'];
                    $identifier = $option;
                    $access_token = $editStreamData['access_token'];
                    
                    
                    $result = shell_exec("curl " . FB_GRAPH_API_HOST . $identifier."'/live_videos?access_token=".$access_token."' -H 'Host: graph.facebook.com' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5'  -H 'Content-Type: application/x-www-form-urlencoded' -H 'Referer: https://developers.facebook.com' -H 'origin: https://developers.facebook.com' --data 'debug=all&format=json&method=post&pretty=0&title=".$stream_title."&description=".$stream_description."&suppress_http_code=1'");
                    
                    $data = json_decode($result,true);

                    if($data['stream_url'] == '' || $data['message']){

                        if (!empty($data['error']['message'])) // facebook meesage passed to user
                        {
                            if($data['error']['type'] == 'OAuthException'){
                                $session->write('social_selected_error', __(canadmin));
                                return $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=>$formId]);
                            }
                            echo "<h4>".__(facebook_respone)."</h4>";
                            echo $data['error']['message'];
                            echo "</br>";
                        }
                        echo __(no_rtmp)."<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";exit;
                    }

                    $new_stream_url = $data['stream_url'];
                    /* Get Stream Key */
                    $explode_stream = explode('/rtmp/',$new_stream_url);
                    $stream_key = $explode_stream[1];
                    /* ************ */
                    $new_stream_url_secure = $data['secure_stream_url'];
                    $rmtp_stream_id = $data['id'];
                } else if($flag != "youtube"){
                    $identifier =  str_replace('?fields=cover','',$_SESSION["identifier"]);


                    $access_token = $session->read( 'fb.accesstoken' );


                    $result = shell_exec('curl -k -X POST ' . FB_GRAPH_API_HOST .$identifier.'/live_videos \
                    -F "access_token='.$access_token.'" \
                    -F "published=true"');
                    $data = json_decode($result,true);
                    $new_stream_url = $data['stream_url'];

                    if($data['stream_url'] == '' || $data['message']){

                    if (!empty($data['error']['message'])) // facebook meesage passed to user
                    {
                        echo "<h4>".__(facebook_respone)."</h4>";
                        echo $data['error']['message'];
                        echo "</br>";
                    }
                    echo __(no_rtmp)."<a href='" . $_SERVER["HTTP_REFERER"] . "'>".__(go_back)."</a>";exit;
                    }

                    /* Get Stream Key */
                    $explode_stream = explode('/rtmp/',$new_stream_url);
                    $stream_key = $explode_stream[1];
                    /* ************ */
                    $new_stream_url_secure = $data['secure_stream_url'];
                    $rmtp_stream_id = $data['id'];
                }
                else 
                {
                     $response = $this->googleSteamRegister($stream_title,$stream_description,$editStreamData->flag_option,$fid,$editStreamData->accesstoken);

                }

                $new_stream_key = explode('rtmp://rtmp-api.facebook.com:80/rtmp/',$new_stream_url);
                $updateCommingUsers = $CommingUsers->newEntity();
                $updateCommingUsers->stream_title = $stream_title;
                $updateCommingUsers->stream_description = $stream_description;
                $updateCommingUsers->rtmp_url = $new_stream_url;
                $updateCommingUsers->new_stream_url_secure = $new_stream_url_secure;
                $updateCommingUsers->stream_key = $new_stream_key[1];
                $updateCommingUsers->id = $id;
                if($CommingUsers->save($updateCommingUsers) )
                {
                    $removed = $this->removeWowzaStream($editStreamData['app_name'], $editStreamData['target_name'], $editStreamData['str_src_name'], $new_stream_url);

                    $enabled = "false";
                    if($editStreamData['live_status'] == 1){
                        $enabled = "true";
                    }
                    
                    $added = $this->addWowzaStream($editStreamData['app_name'], $editStreamData['target_name'], $editStreamData['str_src_name'], $new_stream_url_secure, $enabled);



        
                    $this->redirect(HTTP_ROOT.'Commingusers/view-form?id='.$editStreamData['form_id'] . '&s=1');
                    header('location:'.HTTP_ROOT.'Commingusers/view-form?id='.$editStreamData['form_id'] . '&s=1');
                    echo "<script>location.href = '" .HTTP_ROOT.'Commingusers/view-form?id='.$editStreamData['form_id'] . "&s=1';</script>";
                                //echo $editStreamData['app_name'].' '.$editStreamData['str_src_name'].' '.$new_stream_url;
                                die(__(Redirect_issue));
                } 
                else 
                {
                    echo __(db_update_error);exit;
                }

            }           
        }
    }



    public function deleteStream(){
        $session = $this->request->session();
        if(!empty($_GET['id']) ){
            $id         = $_REQUEST['id'];
            $fb_eamil   = $_REQUEST['eid'];
			$commingUser = TableRegistry::get("comming_users");
			$session->write('newNetwork', true);

            if($userStream = $commingUser->find('all')->where(['id' => $id, 'fb_email' => $fb_eamil ])->first()){                
                $removed = $this->removeWowzaStream($userStream['app_name'], $userStream['target_name'], $userStream['str_src_name']);

                if (!empty($removed)) {
                    $commingUser->delete($userStream);

                    $parentUserStream = $commingUser->find('all')->where(['target_name' => $userStream['target_name'] ])->first();
                    if (!empty($parentUserStream)) 
                    {
                        $removed = $this->removeWowzaStream($parentUserStream['app_name'], $parentUserStream['target_name'], $parentUserStream['str_src_name']);
                        if ($removed) { 
                            $commingUser->delete($parentUserStream);
                        }
                    }

                    $this->resetApplcationStock($userStream['group_id'],$userStream['app_name']);
                    
                    $this->redirect(HTTP_ROOT.'Commingusers/view-form?id=' .$_GET['fid'] .'&d=1');
                    header('location:'.$_SERVER['HTTP_REFERER'].'&d=1');
                    echo "<script>location.href = '" .HTTP_ROOT.'Commingusers/view-form?id=' .$_GET['fid'] . "&d=1';</script>";
                }else{
                    $session->write('social_selected_error', __(serverError));
                    return $this->redirect(['controller' => 'Commingusers', 'action' => 'View-form','id'=> $_GET['fid']]);
                }
            }else{
                $this->redirect($_SERVER['HTTP_REFERER'].'&d=2');
                echo "<script>location.href = '" .HTTP_ROOT.'Commingusers/view-form?id=' .$_GET['fid'] . "&d=2';</script>";
                die('Error: 403');
            } 
        }     
        $this->redirect(HTTP_ROOT.'Commingusers/view-form?id='. $_GET['fid'].'&d=3');
        echo "<script>location.href = '" .HTTP_ROOT.'Commingusers/view-form?id=' .$_GET['fid'] . "&d=';</script>";
        die('<h2>404! Not Found</h2>');         
    }

    public function viewBroadcastsList(){
        $this->autoRender = false;
        $requestData= $this->request->data;

        extract($this->request->session()->read('query'));

        if($this->Auth->user('role') == 1 || $this->Auth->user('role') == 2){
            $columns = array(
            0 => 'id',
            1 => 'startDate',
            2 => '',
            3 => '',
            4 => 'Broadcasted',
            5 => '',
            6 => 'Appname',
            7 => 'TotalInteraction',
            8 => 'commentSum',
            9 => 'viewSum',
            10 => 'loveSum',
            11 => 'hahaSum',
            12 => 'wowSum',
            13 => 'sadSum',
            14 => 'angrySum',
            15 => 'shareSum',
            16 => 'BReached',
            17 => 'Error',
            18 => 'Success',
            19 => '',
            20 => 'lastUpdates'
        );
        }
        else{
        $columns = array(
            0 => 'id',
            1 => 'startDate',
            2 => '',
            3 => '',
            4 => 'Broadcasted',
            5 => '',
            6 => 'TotalInteraction',
            7 => 'commentSum',
            8 => 'viewSum',
            9 => 'loveSum',
            10 => 'hahaSum',
            11 => 'wowSum',
            12 => 'sadSum',
            13 => 'angrySum',
            14 => 'shareSum',
            15 => 'BReached',
            16 => 'Error',
            17 => 'Success',
            18 => '',
            19 => 'lastUpdates'
        );
    }
        $searchColumns = array('LiveStream.id', 'LiveStream.stream_started', 'LiveStream.stream_ends');

        $conn = ConnectionManager::get('default');
        $results = $conn->execute($count)->fetchAll('assoc');        
        $totalData = isset($results[0]['count']) ? $results[0]['count'] : 0;
        $totalFiltered = $totalData;

        // Check user is ich and active or not
        $userId = $this->Auth->user('id');
        $usersModel = TableRegistry::get("Users");
        
        $user_details = $usersModel->find('all')->where(['id' => $userId])->first();
        if(count($user_details) == 1){
            $userPlansModel = TableRegistry::get("user_plans");
            $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$userId, 'is_expired'=>0])->first();
            if(count($userPlansData) == 0){
                if($user_details->registerFrom == "ICH"){
                    $totalData = 1;
                    $totalFiltered = $totalData;
                }else{
                $totalData = 3;
                $totalFiltered = $totalData;
            }
        }
        }
        // Check user is ich and active or not

        $sidx = $columns[$requestData['order'][0]['column']];
        $sord = $requestData['order'][0]['dir'];

        $search = $requestData['search']['value'];
        
        $where_search = "";
        if($search){
            $i = 0;

            foreach ($searchColumns as $value) {
                if($value){
                    if($i == 0){
                        $where_search .= " AND ( " . $value . " LIKE '%".$search."%'";
                    }else{
                        $where_search .= " OR " . $value . " LIKE '%".$search."%'";
                    }
                    if($i == count($searchColumns) - 1){
                        $where_search .= ")";
                    }
                    $i++;
                }
            }            
        }
        
        $start = $requestData['start'];
        $length = $requestData['length'];

        $SQL = $detail.$where_search." GROUP BY LiveStream.id ORDER BY $sidx $sord ";

        if($length > -1){
            $SQL .= "LIMIT $start , $length ";
        }

        $results = $conn->execute( $SQL )->fetchAll('assoc');
        $i = 0;
        $data = array();
        foreach ( $results as $row){

            /*
              Title : Used to get interactions from broadcast-detail table on "view-broadcasts" page
              author : Sandhya Ghatoda
              Created Date : 20-11-2019
            */

            $qwry = "SELECT IFNULL(SUM(vlike),0) as vlikeSum, IFNULL(SUM(view),0) as viewSum, IFNULL(SUM(comment),0) as commentSum, IFNULL(SUM(love),0) as loveSum, IFNULL(SUM(wow),0) as wowSum, IFNULL(SUM(angry),0) as angrySum, IFNULL(SUM(sad),0) as sadSum, IFNULL(SUM(haha),0) as hahaSum, IFNULL(SUM(dislike),0) as dislikeSum, IFNULL(SUM(share),0) as shareSum from `broadcast_details` where stream_id = ".$row["id"]." AND type='fb'";

            $res = $conn->execute( $qwry )->fetchAll('obj');

            $qwrt = "SELECT IFNULL(SUM(user.followers),0) as BReached_new, SUM(if(broadcast_details.status = 'UNPUBLISHED', user.followers, 0)) AS Error_new, SUM(if(broadcast_details.status != 'UNPUBLISHED', user.followers, 0)) AS Success_new FROM `broadcast_details` LEFT JOIN `comming_users` as user ON broadcast_details.target_id = user.id join live_streams as LiveStream on LiveStream.id=broadcast_details.stream_id where broadcast_details.type = 'fb' and LiveStream.id = ".$row["id"];
            $qwrt_res = $conn->execute( $qwrt )->fetchAll('obj');

            $nestedData= [];
            $nestedData[] = "<a href=".HTTP_ROOT."commingusers/broadcast-details/".$row["id"].">".$row["id"]."</a>";
            $nestedData[] = ($row["startDate"] != "") ? appDate($row["startDate"]) : "";
            $nestedData[] = appTime($row["stream_started"]);
            $nestedData[] = appTime($row['stream_ends']);
            $nestedData[] = $row['Broadcasted'];
            if ($row['fb_td_status'] == 1){
                $nestedData[] = "<a href=".HTTP_ROOT."commingusers/fb-title/".$row["id"]." data-target='#myModal' data-toggle='ajaxModal'>".__(Read)."</a>";
            }else{
                $nestedData[] = __(Many);
            }

            if($this->Auth->user('role') == 1 || $this->Auth->user('role') == 2){
                $nestedData[] = "<a href=".HTTP_ROOT."formbuilder/listing/".$row["group_id"].">".$row["stream_app"]."</a>";
            }

            //$dataTotal->angry+$dataTotal->comment+$dataTotal->view+$dataTotal->vlike+$dataTotal->love+$dataTotal->haha+$dataTotal->wow+$dataTotal->sad+$dataTotal->dislike
             $nestedData[] =$res[0]->angrySum+$res[0]->shareSum+$res[0]->commentSum+$res[0]->vlikeSum+$res[0]->loveSum+$res[0]->hahaSum+$res[0]->wowSum+$res[0]->sadSum;// 'angry='.$row['angryBSum'].'commentBSum='.$row['commentBSum'].'vlikeBSum='.$row['vlikeBSum'].'viewBSum='.$row['viewBSum'].'loveBSum='.$row['loveBSum'].'hahaBSum='.$row['hahaBSum'].'wowBSum='.$row['wowBSum'].'sadBSum='.$row['sadBSum'].'dislikeSum='.$row['dislikeSum'];//$row['angryBSum']+$row['commentBSum']+$row['vlikeBSum']+$row['viewBSum']+$row['loveBSum']+$row['hahaBSum']+$row['wowBSum']+$row['sadBSum']+$row['dislikeSum'];//$row['TotalInteraction'];
            $nestedData[] = $res[0]->commentSum;
            //$nestedData[] = $res[0]->viewSum;
            $nestedData[] = $res[0]->vlikeSum;
            $nestedData[] = $res[0]->loveSum;
            $nestedData[] = $res[0]->hahaSum;
            $nestedData[] = $res[0]->wowSum;
            $nestedData[] = $res[0]->sadSum;
            $nestedData[] = $res[0]->angrySum;
            $nestedData[] = $res[0]->shareSum;
            $nestedData[] = ($qwrt_res[0]->BReached_new) ? $qwrt_res[0]->BReached_new : 0;
            $nestedData[] = ($qwrt_res[0]->Error_new) ? $qwrt_res[0]->Error_new : 0;
            $nestedData[] = ($qwrt_res[0]->Success_new) ? $qwrt_res[0]->Success_new : 0;
           // $nestedData[] = $row['Total'];
            if($qwrt_res[0]->BReached_new > 0 ) {
                $per = round($qwrt_res[0]->Success_new/$qwrt_res[0]->BReached_new, 6)*100;
                $nestedData[] = number_format($per,2,',', '') . "%";
            }else{                
                $nestedData[] = number_format(0,2,',', '') . "%";
            }   
            $nestedData[] = appDateTimeConvert($row['lastUpdates']);
            $data[] = $nestedData;
            $i++;
        }
        $json_data = array(
            "draw"            => intval( $requestData['draw'] ),
            "recordsTotal"    => intval( $totalData ),
            "recordsFiltered" => intval( $totalFiltered ),
            "data"            => $data
        );
        echo json_encode($json_data);exit;
    }


    public function viewBroadcasts(){
        $this->viewBuilder()->layout('admin_inner');
        $this->loadComponent('Paginator');
        $liveStreamModel = TableRegistry::get("live_streams");
        $CommingUsersModel = TableRegistry::get("comming_users");
        $BroadcastDetailModel = TableRegistry::get("broadcast_details");
        $applicationsModel = TableRegistry::get('Applications');
        $applications = $applicationsModel->find('all')->toArray();

		$userId = $this->Auth->user('id');
        if($this->Auth->user('parent_id') != 0){
            $userId = $this->Auth->user('parent_id');
        }
		$userRole = $this->Auth->user('role');
        $userSegment = $this->Auth->user('segment_id');
		$SegmentModel = TableRegistry::get('Segments');
		$SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
		$userSegmentId=0;
		if(!empty($SegmentData)){
		  $userSegmentId = $SegmentData->segment_admin;
		}
		$usersModel = TableRegistry::get("Users");
	    $usersList  = $usersModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'id'
        ])->where(['segment_id' =>$userSegment,'role !=' =>5])->toArray();
	    $usersList[$userId] = $this->Auth->user('id');
		if($userRole==5){
		  if($userId==$userSegmentId){ 	
            $applications = $applicationsModel->find('all')->contain(['Users','Groups'])->where(['Users.id IN'=> $usersList])->toArray();
		  }else{
			$applications = $applicationsModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$userId])->toArray();  
		  }  
		}else{
          $applications = $applicationsModel->find('all')->toArray();
		}
        //echo "<pre>"; print_r($applications);die;
        $apps[0] = 'All';
        foreach($applications as $key => $value){
            $apps[$value['application_name']] = ucfirst( $value['application_name'] );
        }

        //////////////Start - update youtube reaches//////////

        $FormModel = TableRegistry::get("form_listings");
        $form_data = $FormModel->find('all')->where(['owner_id'=>$userId])->first();

        $commingUsersModel = TableRegistry::get("comming_users");
        $commingUsers = $commingUsersModel->find('all')->where(['form_id' => $form_data->form_id, 'flag'=> "youtube"])->toArray(); 
         if (!empty($commingUsers)){
            foreach ($commingUsers as $user){
                if($user->channelId != ""){
                    $option = array(
                         'part' => 'snippet,statistics', 
                         'id' => $user->channelId,
                         //'key' => "AIzaSyB7qIKLbjvc0DdP3HNpltuMNdBzBo_afUI"
             'key' => YOUTUBE_KEY
                        );
                 $channelInfo = $this->getCurl('https://www.googleapis.com/youtube/v3/channels?'.http_build_query($option, 'a', '&'));
                 $channelInfo = json_decode($channelInfo);

                 ///write log for youtube api limit reach//////
                $log_data_new = array('controller' => 'Comminguser','functionName' => 'viewBroadcasts', 'apiName' =>  'https://www.googleapis.com/youtube/v3/channels' );
                $textdata = PHP_EOL. date("Y-m-d H:i:s")."-> Data -> ". print_r($log_data_new, true);
                $my_file = fopen("../webroot/youtube_limit_reach.txt", "a") or die("Unable to open file!");
                fwrite($my_file, $textdata);        
                fclose($my_file);
                ///write log for youtube api limit reach//////
                 
                 $channelSub = $channelInfo->items[0]->statistics->subscriberCount;
                    $userStatus = $commingUsersModel->newEntity();  
                    $userStatus->id = $user['id'];
                    $userStatus->followers = $channelSub;
                    $commingUsersModel->save($userStatus);
                        
                    
                }
            
            }
        }

        /////////////Stop - update youtube reaches////////

        // Check user is ich and active or not
        $isIchExpired = 0;
        $isAnyExpired = 0;
        $user_details = $usersModel->find('all')->where(['id' => $userId])->first();
        if(count($user_details) == 1){
            $userPlansModel = TableRegistry::get("user_plans");
            $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$userId, 'is_expired'=>0])->first();
            if(count($userPlansData) == 1){
                $isIchExpired = 0;
                $isAnyExpired = 0;
            }else{
                if($user_details->registerFrom == "ICH"){
                $isIchExpired = 1;
                }else{
                    if($userRole != 1 && $userRole != 2){
                    $isAnyExpired = 1;
                    }else{
                        $isAnyExpired = 0;
                    }
                }
            }
        }
        $this->set('isIchExpired', $isIchExpired);
        $this->set('isAnyExpired', $isAnyExpired);
        $this->set('userRole', $userRole);
        // Check user is ich and active or not

        $this->set('applications', $apps);        
        $this->set('startDate', date( "m/1/Y 00:00:00"));
        $this->set('endDate', appTime(date( "m/d/Y H:i:s")));
        $this->set('submited', '0');
        $where_count_join = "stream_status = 2";
        $where_join = "stream_status = 2";
        $where =  array('stream_status' => 2 );
        if(isset($this->request->data)&& !empty($this->request->data)){
            $reportrange = $this->request->data['reportrange'];
            $range = explode(' - ', $reportrange);
            $range1 = str_replace("/", "-",$range[0]);
            $range2 = str_replace("/", "-",$range[1]);
            $start = date( "Y-m-d H:i:s", strtotime(convertToUtc($range1)));
            $end = date( "Y-m-d H:i:s", strtotime(convertToUtc($range2)));            
        
            $where = array_merge($where, array('stream_started >=' => $start, 'stream_started <=' => $end) );

            $where_count_join .= " AND stream_started >= '" .$start. "' AND stream_started <= '" . $end . "'";
            $where_join .= " AND stream_started >= '" .$start. "' AND stream_started <= '" . $end . "'";

            $this->set('startDate',date( "d/m/Y H:i:s", strtotime($range1) ));
            $this->set('endDate',date( "d/m/Y H:i:s", strtotime($range2) ));
            $this->set('submited', '1');
        }

        $appId = @$this->request->data['appid'];
        if ($appId) {
            $where = array_merge( $where, array('stream_app' => $appId) );
            $where_count_join .= " AND stream_app = " .$appId;
            $where_join .= " AND stream_app = " .$appId;
        }

		$userSegment = $this->Auth->user('segment_id');

        if(!in_array( $this->user('role'), ['1', '2'])) {
            $groupsModel = TableRegistry::get("Groups");
			if($userRole==5){
			  if($userId==$userSegmentId && !empty($usersList)){ 	
				  $groupDataList = $groupsModel->find('list', ['keyField' => 'id','valueField' => 'id'])->where(['owner_id IN'=>$usersList])->toArray();
				  if(!empty($groupDataList)){
					 $implodeGroupList =implode(",",$groupDataList); 	  
				  $where = array_merge( $where, array('group_id IN' => $groupDataList ) );
  				  $where_count_join .= " AND live_streams.group_id IN (" .$implodeGroupList.")";
				  $where_join .= " AND LiveStream.group_id IN (" .$implodeGroupList.")";
				  }
			  }else{
				$groupData = $groupsModel->find('all')->where(['owner_id'=>$userId])->first();
				if(!empty($groupData)){
					$where = array_merge( $where, array('group_id' => $groupData['id'] ) );
					$where_count_join .= " AND live_streams.group_id = " .$groupData['id'];
					$where_join .= " AND LiveStream.group_id = " .$groupData['id'];
				}else{
					$where = array('group_id' => 0);
				    $where_count_join .= " AND live_streams.group_id = 0";
                    $where_join .= " AND LiveStream.group_id = 0";
				}	
			  }
			}else{
            $groupData = $groupsModel->find('all')->where(['owner_id'=>$this->getManager()])->first();
            $where = array_merge( $where, array('group_id' => $groupData['id'] ) );
            $where_count_join .= " AND live_streams.group_id = " .$groupData['id'];
            $where_join .= " AND LiveStream.group_id = " .$groupData['id'];
        }

        }
		
		//echo "<pre>";
		//print_r($where);die;
		

        $conn = ConnectionManager::get('default');
        $TotalReachedQuery = "SELECT IFNULL(SUM(user.followers),0) as Reached FROM `live_streams` as LiveStream LEFT JOIN `broadcast_details` as detail ON detail.stream_id = LiveStream.id LEFT JOIN `comming_users` as user ON detail.target_id = user.id where " . $where_join." AND detail.status != 'UNPUBLISHED' AND detail.type = 'fb'";
        $TotalBroadcastQuery = "SELECT SUM(TIME_TO_SEC(TIMEDIFF(LiveStream.stream_ends, LiveStream.stream_started))) as diff FROM `live_streams` as LiveStream where " . $where_join;

              $FormListingModel = TableRegistry::get("form_listings");
        $FormListingData = $FormListingModel->find('all')->where(['owner_id'=>$userId])->first();
        $userRole = $this->Auth->user('role');
        if($userRole != 1 && $userRole != 2){
            $this->set('form_id',$FormListingData->form_id);
            $liveBroadDataCount = $liveStreamModel->find('all')->where([$where])->count();
        $broadcastResults = $conn->execute( $TotalBroadcastQuery )->fetch('assoc');
            $followersResults = $conn->execute( $TotalReachedQuery )->fetch('assoc');
            $this->set('followersSum', $followersResults['Reached']);
        $this->set('totalBroadCasted', secToStamp($broadcastResults['diff']));
            $this->set('liveBroadDataCount', $liveBroadDataCount);    
        }else{
            $this->set('form_id',"");
            $globalDataModel = TableRegistry::get("global_data");
            $globalData = $globalDataModel->newEntity();
            $data = $globalDataModel->find('all')->first();
            if(!empty($data)){
               $followersResults = $data->followersSum;
               $broadcastResults = $data->totalBroadCasted;
               $liveBroadDataCount = $data->liveBroadDataCount;
            }
            $this->set('followersSum', $followersResults);
            $this->set('totalBroadCasted', secToStamp($broadcastResults));
            $this->set('liveBroadDataCount', $liveBroadDataCount);
        }

        $query = array();               
        $query['count']  = "select count(*) AS count from (SELECT live_streams.id  FROM `broadcast_details` LEFT JOIN `comming_users` as user ON broadcast_details.target_id = user.id join live_streams on live_streams.id=broadcast_details.stream_id where " . $where_count_join."
GROUP BY live_streams.id) as countedtable";

        $query['detail'] = "SELECT
            LiveStream.id, 
            LiveStream.group_id, 
            LiveStream.stream_app,
            LiveStream.stream_started,
            LiveStream.lastUpdates,
            LiveStream.stream_ends,
            LiveStream.startDate,
            LiveStream.Total,
            LiveStream.TotalInteraction,
            LiveStream.Error,
            LiveStream.Success,
            LiveStream.Reached, 
            LiveStream.vlikeSum, 
            LiveStream.shareSum,
            LiveStream.viewSum, 
            LiveStream.commentSum, 
            LiveStream.loveSum, 
            LiveStream.wowSum, 
            LiveStream.angrySum, 
            LiveStream.sadSum, 
            LiveStream.hahaSum,
            LiveStream.Broadcasted,
            LiveStream.fb_td_status,
            sum(broadcast_details.angry) as angryBSum,
            sum(broadcast_details.share) as shareBSum,
            sum(broadcast_details.comment) as commentBSum,
            sum(broadcast_details.view) as viewBSum,
            sum(broadcast_details.vlike) as vlikeBSum,
            sum(broadcast_details.love) as loveBSum,
            sum(broadcast_details.haha) as hahaBSum,
            sum(broadcast_details.wow) as wowBSum,
            sum(broadcast_details.sad) as sadBSum,
            sum(broadcast_details.dislike) as dislikeSum,
            IFNULL(SUM(user.followers),0) as BReached
            FROM `broadcast_details` LEFT JOIN `comming_users` as user ON broadcast_details.target_id = user.id join live_streams as LiveStream on LiveStream.id=broadcast_details.stream_id
            where " . $where_join;
        $this->request->session()->write('query', $query);


        $this->set('page','broadcasts');
        $this->set('pg','view_broadcasts_fb');
        
        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
    }

    function broadcastDetails($stream_id=''){
        $this->viewBuilder()->layout('admin_inner');
        $BroadcastDetailModel = TableRegistry::get("broadcast_details");
        $targets = $BroadcastDetailModel->find()->select(['broadcast_details.target_id'])->where(['stream_id' => $stream_id, 'type' => 'fb'])->toArray();
        $CommingUsersModel = TableRegistry::get("comming_users");
        $targets_in_arr = array();
        foreach ($targets as $t) {
            array_push($targets_in_arr, $t->target_id);
        }
        if(!empty($targets_in_arr)){
            $liveBroadData = $CommingUsersModel->find()->where(['id IN'=> $targets_in_arr])->toArray(); 
        }else{
            $liveBroadData = array();
        }

        $liveStreamsModel = TableRegistry::get("live_streams");        
        $liveStreamsData = $liveStreamsModel->find('all')->where( ['id' => $stream_id] )->first();

        $GroupsModel = TableRegistry::get("groups");        
        $GroupsData = $GroupsModel->find('all')->where( ['id' => $liveStreamsData['group_id']] )->first();

        $FormListingModel = TableRegistry::get("form_listings");
        $FormListingData = $FormListingModel->find('all')->where(['form_id'=>$liveBroadData[0]['form_id']])->first();

        // Check account is expired after 90 days
        $is90Expired = 0;
        if(count($FormListingData) == 1){
            
            if($FormListingData->disabled == 1){
                $is90Expired = 1;
            }
        }
        $this->set('is90Expired',$is90Expired);

        // Check account is expired after 90 days

        //$this->updateInteractions($stream_id);
        $this->set('liveBroadData',$liveBroadData);
		//echo "<pre>"; print_r($FormListingData); die;
         $this->set('stream_id',$stream_id);
        $this->set('FormListingData',$FormListingData);
        $this->set('liveStreamsData',$liveStreamsData);
        $this->set('groupsData',$GroupsData);
        $this->set('startDate', $liveStreamsData->stream_started->format('Y-m-d H:i:s'));
        $this->set('endDate', $liveStreamsData->stream_ends->format('Y-m-d H:i:s'));
        $this->set('page','broadcasts');
        $this->set('pg','broadcast_fb');
        
        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
    }

    function broadcastTitle($target_id = '', $stream_id){
        $this->viewBuilder()->layout('');
        $BroadcastDetail_model = TableRegistry::get("broadcast_details");
        $liveBroadData = $BroadcastDetail_model->find('all')->where(['target_id' => $target_id, 'stream_id' => $stream_id])->first();        
        $this->set('target_id',$target_id);
        $this->set('liveBroadData',$liveBroadData);
        $this->render('modal/title');
    }

    function fbTitle($liveStreamID='')
    {
        $this->viewBuilder()->layout('');
        $liveStreamsListingModel = TableRegistry::get("live_streams");
        $liveStream = $liveStreamsListingModel->find('all')->where(['id' => $liveStreamID])->first();
        $this->set('liveStream',$liveStream);
        $this->render('modal/broadcast_title');
    }

    public function updateSpecificInteractions(){

        $userId = $this->Auth->user('id');
        if($this->Auth->user('parent_id') != 0){
            $userId = $this->Auth->user('parent_id');
        }
        $userRole = $this->Auth->user('role');
        $userSegment = $this->Auth->user('segment_id');
        $SegmentModel = TableRegistry::get('Segments');
        $SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
        $userSegmentId=0;
        if(!empty($SegmentData)){
          $userSegmentId = $SegmentData->segment_admin;
        }
        $usersModel = TableRegistry::get("Users");
        $usersList  = $usersModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'id'
        ])->where(['segment_id' =>$userSegment,'role !=' =>5])->toArray();
        $usersList[$userId] = $this->Auth->user('id');

        $where_join = "stream_status = 2";
        $where =  array('stream_status' => 2 );
        
        /*if(isset($_POST['reportrange']) && ($_POST['reportrange'] != '')){
            $reportrange = $_POST['reportrange'];
            $range = explode(' - ', $reportrange);
            $range1 = str_replace("/", "-",$range[0]);
            $range2 = str_replace("/", "-",$range[1]);
            $start = date( "Y-m-d H:i:s", strtotime(convertToUtc($range1)));
            $end = date( "Y-m-d H:i:s", strtotime(convertToUtc($range2)));            
        
            $where = array_merge($where, array('stream_started >=' => $start, 'stream_started <=' => $end) );

            
            $where_join .= " AND stream_started >= '" .$start. "' AND stream_started <= '" . $end . "'";
        }*/

            if(!in_array( $this->user('role'), ['1', '2'])) {
            $groupsModel = TableRegistry::get("Groups");
            if($userRole==5){
              if($userId==$userSegmentId && !empty($usersList)){    
                  $groupDataList = $groupsModel->find('list', ['keyField' => 'id','valueField' => 'id'])->where(['owner_id IN'=>$usersList])->toArray();
                  if(!empty($groupDataList)){
                     $implodeGroupList =implode(",",$groupDataList);      
                  $where = array_merge( $where, array('group_id IN' => $groupDataList ) );
                  
                  $where_join .= " AND LiveStream.group_id IN (" .$implodeGroupList.")";
                  }
              }else{
                $groupData = $groupsModel->find('all')->where(['owner_id'=>$userId])->first();
                if(!empty($groupData)){
                    $where = array_merge( $where, array('group_id' => $groupData['id'] ) );
                    
                    $where_join .= " AND LiveStream.group_id = " .$groupData['id'];
                }else{
                    $where = array('group_id' => 0);
                    
                    $where_join .= " AND LiveStream.group_id = 0";
                }   
              }
            }else{
                $groupData = $groupsModel->find('all')->where(['owner_id'=>$this->getManager()])->first();
                $where = array_merge( $where, array('group_id' => $groupData['id'] ) );
                
                $where_join .= " AND LiveStream.group_id = " .$groupData['id'];
            }

        }

         $query = "SELECT LiveStream.id FROM `broadcast_details` join live_streams as LiveStream on LiveStream.id=broadcast_details.stream_id where " . $where_join;
        
        //$searchColumns = array('LiveStream.id', 'LiveStream.stream_started', 'LiveStream.stream_ends');

        $SQL = $query." GROUP BY LiveStream.id ORDER BY LiveStream.id DESC ";
        $length = $_POST['selected_val'];
        if($length > -1){
            $SQL .= "LIMIT $length ";
        }

        $conn = ConnectionManager::get('default');
        $results = $conn->execute( $SQL )->fetchAll('assoc');

        $stream_id = array();
        foreach ( $results as $row){
            array_push($stream_id, $row["id"]);
        }

        $interaction_res = $this->updateInteractionsByAjax($stream_id,$_POST['form_id'], $_POST['report_type']);
        
        if($interaction_res){
        $data = [
            'status' => true
            
            ];
        }else{
            $data = [
            'status' => false
            
            ];
        }

        echo json_encode($data);die; 
    }

    function broadcastDetailsYoutube($stream_id=''){
        $this->viewBuilder()->layout('admin_inner');

        if($this->Auth->user('role') == 3){
            $end_date = date('Y-m-d H:i:s', strtotime('-1 month'));
            $conn = ConnectionManager::get('default');
            $bd_query = "SELECT detail.target_id FROM `live_streams` as LiveStream LEFT JOIN `broadcast_details` as detail ON detail.stream_id = LiveStream.id where detail.stream_id = ".$stream_id." AND detail.type = 'yt' AND LiveStream.stream_started > '".$end_date."'";
            $targets = $conn->execute( $bd_query )->fetchAll('obj');
        }else{

        $BroadcastDetailModel = TableRegistry::get("broadcast_details");
        $targets = $BroadcastDetailModel->find()->select(['broadcast_details.target_id'])->where(['stream_id' => $stream_id, 'type' => 'yt'])->toArray();
        }
        
        $CommingUsersModel = TableRegistry::get("comming_users");
        $targets_in_arr = array();
        foreach ($targets as $t) {
            array_push($targets_in_arr, $t->target_id);
        }
        if(!empty($targets_in_arr)){
            $liveBroadData = $CommingUsersModel->find()->where(['id IN'=> $targets_in_arr])->toArray(); 
        }else{
            $liveBroadData = array();
        }

        $liveStreamsModel = TableRegistry::get("live_streams");        
        $liveStreamsData = $liveStreamsModel->find('all')->where( ['id' => $stream_id] )->first();

        $GroupsModel = TableRegistry::get("groups");        
        $GroupsData = $GroupsModel->find('all')->where( ['id' => $liveStreamsData['group_id']] )->first();

        $FormListingModel = TableRegistry::get("form_listings");
        $FormListingData = $FormListingModel->find('all')->where(['form_id'=>$liveBroadData[0]['form_id']])->first();

        // Check account is expired after 90 days
        $is90Expired = 0;
        if(count($FormListingData) == 1){
            
            if($FormListingData->disabled == 1){
                $is90Expired = 1;
            }
        }
        $this->set('is90Expired',$is90Expired);

        // Check account is expired after 90 days

        //$this->updateInteractions($stream_id);
        $this->set('liveBroadData',$liveBroadData);
        //echo "<pre>"; print_r($FormListingData); die;
         $this->set('stream_id',$stream_id);
        $this->set('FormListingData',$FormListingData);
        $this->set('liveStreamsData',$liveStreamsData);
        $this->set('groupsData',$GroupsData);
        $this->set('startDate', $liveStreamsData->stream_started->format('Y-m-d H:i:s'));
        $this->set('endDate', $liveStreamsData->stream_ends->format('Y-m-d H:i:s'));
        $this->set('page','broadcasts');
        $this->set('pg','broadcast_yt');
        
        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
    }

    function broadcastDetailsAll($stream_id=''){
        $this->viewBuilder()->layout('admin_inner');
        
        if($this->Auth->user('role') == 3){
            $end_date = date('Y-m-d H:i:s', strtotime('-1 month'));
            $conn = ConnectionManager::get('default');
            $bd_query = "SELECT detail.target_id FROM `live_streams` as LiveStream LEFT JOIN `broadcast_details` as detail ON detail.stream_id = LiveStream.id where detail.stream_id = ".$stream_id." AND detail.type = 'yt' AND LiveStream.stream_started > '".$end_date."'";
            $targets['ytData'] = $conn->execute( $bd_query )->fetchAll('obj');

             $bd_query1 = "SELECT detail.target_id FROM `live_streams` as LiveStream LEFT JOIN `broadcast_details` as detail ON detail.stream_id = LiveStream.id where detail.stream_id = ".$stream_id." AND detail.type = 'fb'";
            $targets['fbData'] = $conn->execute( $bd_query1 )->fetchAll('obj');

            $targets = array_merge($targets['ytData'],$targets['fbData']);

        }else{

        $BroadcastDetailModel = TableRegistry::get("broadcast_details");
        $targets = $BroadcastDetailModel->find()->select(['broadcast_details.target_id'])->where(['stream_id' => $stream_id])->toArray();
        }

        $CommingUsersModel = TableRegistry::get("comming_users");
        $targets_in_arr = array();
        foreach ($targets as $t) {
            array_push($targets_in_arr, $t->target_id);
        }
        if(!empty($targets_in_arr)){
            $liveBroadData = $CommingUsersModel->find()->where(['id IN'=> $targets_in_arr])->toArray(); 
        }else{
            $liveBroadData = array();
        }

        $liveStreamsModel = TableRegistry::get("live_streams");        
        $liveStreamsData = $liveStreamsModel->find('all')->where( ['id' => $stream_id] )->first();

        $GroupsModel = TableRegistry::get("groups");        
        $GroupsData = $GroupsModel->find('all')->where( ['id' => $liveStreamsData['group_id']] )->first();

        $FormListingModel = TableRegistry::get("form_listings");
        $FormListingData = $FormListingModel->find('all')->where(['form_id'=>$liveBroadData[0]['form_id']])->first();

         // Check account is expired after 90 days
        $is90Expired = 0;
        if(count($FormListingData) == 1){
            
            if($FormListingData->disabled == 1){
                $is90Expired = 1;
            }
        }
        $this->set('is90Expired',$is90Expired);

        // Check account is expired after 90 days
        
        //$this->updateInteractions($stream_id);
        $this->set('liveBroadData',$liveBroadData);
        //echo "<pre>"; print_r($FormListingData); die;
         $this->set('stream_id',$stream_id);
        $this->set('FormListingData',$FormListingData);
        $this->set('liveStreamsData',$liveStreamsData);
        $this->set('groupsData',$GroupsData);
        $this->set('startDate', $liveStreamsData->stream_started->format('Y-m-d H:i:s'));
        $this->set('endDate', $liveStreamsData->stream_ends->format('Y-m-d H:i:s'));
        $this->set('page','broadcasts');
        $this->set('pg','broadcast_all');
        
        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
    }

        // Global report changes

    public function viewBroadcastsListAll(){
        $this->autoRender = false;
        $requestData= $this->request->data;

        extract($this->request->session()->read('query'));

        if($this->Auth->user('role') == 1 || $this->Auth->user('role') == 2){
            $columns = array(
            0 => 'id',
            1 => 'startDate',
            2 => '',
            3 => '',
            4 => 'Broadcasted',
            5 => '',
            6 => 'Appname',
            7 => 'TotalInteraction',
            8 => 'commentSum',
            9 => 'viewSum',
            10 => 'loveSum',
            11 => 'hahaSum',
            12 => 'wowSum',
            13 => 'sadSum',
            14 => 'angrySum',
            15 => 'shareSum',
            16 => 'BReached',
            17 => 'Error',
            18 => 'Success',
            19 => '',
            20 => 'lastUpdates'
        );
        }
        else{
        $columns = array(
            0 => 'id',
            1 => 'startDate',
            2 => '',
            3 => '',
            4 => 'Broadcasted',
            5 => '',
            6 => 'TotalInteraction',
            7 => 'commentSum',
            8 => 'viewSum',
            9 => 'loveSum',
            10 => 'hahaSum',
            11 => 'wowSum',
            12 => 'sadSum',
            13 => 'angrySum',
            14 => 'shareSum',
            15 => 'BReached',
            16 => 'Error',
            17 => 'Success',
            18 => '',
            19 => 'lastUpdates'
        );
        }
        $searchColumns = array('LiveStream.id', 'LiveStream.stream_started', 'LiveStream.stream_ends');

        $conn = ConnectionManager::get('default');
        $results = $conn->execute($count)->fetchAll('assoc');        
        $totalData = isset($results[0]['count']) ? $results[0]['count'] : 0;
        $totalFiltered = $totalData;

         // Check user is ich and active or not
        $userId = $this->Auth->user('id');
        $usersModel = TableRegistry::get("Users");
        
        $user_details = $usersModel->find('all')->where(['id' => $userId])->first();
        if(count($user_details) == 1){
            $userPlansModel = TableRegistry::get("user_plans");
            $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$userId, 'is_expired'=>0])->first();
            if(count($userPlansData) == 0){
                if($user_details->registerFrom == "ICH"){
                    $totalData = 1;
                    $totalFiltered = $totalData;
                }else{
                    $totalData = 3;
                    $totalFiltered = $totalData;
                }
            }
        }
        // Check user is ich and active or not

        $sidx = $columns[$requestData['order'][0]['column']];
        $sord = $requestData['order'][0]['dir'];

        $search = $requestData['search']['value'];
        
        $where_search = "";
        if($search){
            $i = 0;

            foreach ($searchColumns as $value) {
                if($value){
                    if($i == 0){
                        $where_search .= " AND ( " . $value . " LIKE '%".$search."%'";
                    }else{
                        $where_search .= " OR " . $value . " LIKE '%".$search."%'";
                    }
                    if($i == count($searchColumns) - 1){
                        $where_search .= ")";
                    }
                    $i++;
                }
            }            
        }
        
        $start = $requestData['start'];
        $length = $requestData['length'];

        $SQL = $detail.$where_search." GROUP BY LiveStream.id ORDER BY $sidx $sord ";

        if($length > -1){
            $SQL .= "LIMIT $start , $length ";
        }

        $results = $conn->execute( $SQL )->fetchAll('assoc');
        $i = 0;
        $data = array();
        foreach ( $results as $row){

            /*
              Title : Used to get interactions from broadcast-detail table on "view-broadcasts" page
              author : Sandhya Ghatoda
              Created Date : 20-11-2019
            */

            /*$qwry = "SELECT IFNULL(SUM(vlike),0) as vlikeSum, IFNULL(SUM(view),0) as viewSum, IFNULL(SUM(comment),0) as commentSum, IFNULL(SUM(love),0) as loveSum, IFNULL(SUM(wow),0) as wowSum, IFNULL(SUM(angry),0) as angrySum, IFNULL(SUM(sad),0) as sadSum, IFNULL(SUM(haha),0) as hahaSum, IFNULL(SUM(dislike),0) as dislikeSum, IFNULL(SUM(share),0) as shareSum from `broadcast_details` where stream_id = ".$row["id"];

            $res = $conn->execute( $qwry )->fetchAll('obj');*/

            if($this->Auth->user('role') == 3){
                $end_date = date('Y-m-d H:i:s', strtotime('-1 month'));
                $time_check = " AND LiveStream.stream_started > '".$end_date."'";
              }else{
                $time_check = '';
              }

             // youtube interaction 
            $qwry1 = "SELECT IFNULL(SUM(detail.vlike),0) as vlikeSum, IFNULL(SUM(detail.view),0) as viewSum, IFNULL(SUM(detail.comment),0) as commentSum, IFNULL(SUM(detail.love),0) as loveSum, IFNULL(SUM(detail.wow),0) as wowSum, IFNULL(SUM(detail.angry),0) as angrySum, IFNULL(SUM(detail.sad),0) as sadSum, IFNULL(SUM(detail.haha),0) as hahaSum, IFNULL(SUM(detail.dislike),0) as dislikeSum, IFNULL(SUM(detail.share),0) as shareSum from `live_streams` as LiveStream LEFT JOIN `broadcast_details` as detail ON detail.stream_id = LiveStream.id where detail.stream_id = ".$row["id"]." AND detail.type='yt'".$time_check;

            $res1 = $conn->execute( $qwry1 )->fetchAll('obj');

            // facebook interaction 
            $qwry2 = "SELECT IFNULL(SUM(detail.vlike),0) as vlikeSum, IFNULL(SUM(detail.view),0) as viewSum, IFNULL(SUM(detail.comment),0) as commentSum, IFNULL(SUM(detail.love),0) as loveSum, IFNULL(SUM(detail.wow),0) as wowSum, IFNULL(SUM(detail.angry),0) as angrySum, IFNULL(SUM(detail.sad),0) as sadSum, IFNULL(SUM(detail.haha),0) as hahaSum, IFNULL(SUM(detail.dislike),0) as dislikeSum, IFNULL(SUM(detail.share),0) as shareSum from `live_streams` as LiveStream LEFT JOIN `broadcast_details` as detail ON detail.stream_id = LiveStream.id where detail.stream_id = ".$row["id"]." AND detail.type='fb'";

            $res2 = $conn->execute( $qwry2 )->fetchAll('obj');

            // youtube reaches 
            $qwrt1 = "SELECT IFNULL(SUM(user.followers),0) as BReached_new, SUM(if(broadcast_details.status = 'UNPUBLISHED', user.followers, 0)) AS Error_new, SUM(if(broadcast_details.status != 'UNPUBLISHED', user.followers, 0)) AS Success_new FROM `broadcast_details` LEFT JOIN `comming_users` as user ON broadcast_details.target_id = user.id join live_streams as LiveStream on LiveStream.id=broadcast_details.stream_id where broadcast_details.type = 'yt' and LiveStream.id = ".$row["id"].$time_check;
            $qwrt_res1 = $conn->execute( $qwrt1 )->fetchAll('obj');

            // facebook reaches
            $qwrt2 = "SELECT IFNULL(SUM(user.followers),0) as BReached_new, SUM(if(broadcast_details.status = 'UNPUBLISHED', user.followers, 0)) AS Error_new, SUM(if(broadcast_details.status != 'UNPUBLISHED', user.followers, 0)) AS Success_new FROM `broadcast_details` LEFT JOIN `comming_users` as user ON broadcast_details.target_id = user.id join live_streams as LiveStream on LiveStream.id=broadcast_details.stream_id where broadcast_details.type = 'fb' and LiveStream.id = ".$row["id"];
            $qwrt_res2 = $conn->execute( $qwrt2 )->fetchAll('obj');


            $nestedData= [];
            $nestedData[] = "<a href=".HTTP_ROOT."commingusers/broadcast-details-all/".$row["id"].">".$row["id"]."</a>";
            $nestedData[] = ($row["startDate"] != "") ? appDate($row["startDate"]) : "";
            $nestedData[] = appTime($row["stream_started"]);
            $nestedData[] = appTime($row['stream_ends']);
            $nestedData[] = $row['Broadcasted'];
            if ($row['fb_td_status'] == 1){
                $nestedData[] = "<a href=".HTTP_ROOT."commingusers/fb-title/".$row["id"]." data-target='#myModal' data-toggle='ajaxModal'>".__(Read)."</a>";
            }else{
                $nestedData[] = __(Many);
            }

            if($this->Auth->user('role') == 1 || $this->Auth->user('role') == 2){
                $nestedData[] = "<a href=".HTTP_ROOT."formbuilder/listing/".$row["group_id"].">".$row["stream_app"]."</a>";
            }

           
           $total1 =$res1[0]->angrySum+$res1[0]->shareSum+$res1[0]->commentSum+$res1[0]->vlikeSum+$res1[0]->loveSum+$res1[0]->hahaSum+$res1[0]->wowSum+$res1[0]->sadSum;
           $total2 =$res2[0]->angrySum+$res2[0]->shareSum+$res2[0]->commentSum+$res2[0]->vlikeSum+$res2[0]->loveSum+$res2[0]->hahaSum+$res2[0]->wowSum+$res2[0]->sadSum;
           $nestedData[] = $total1+$total2;
          
            $nestedData[] = $res1[0]->commentSum+$res2[0]->commentSum;
            //$nestedData[] = $res[0]->viewSum;
            $nestedData[] = $res1[0]->vlikeSum+$res2[0]->vlikeSum;
            $nestedData[] = $res1[0]->loveSum+$res2[0]->loveSum;
            $nestedData[] = $res1[0]->hahaSum+$res2[0]->hahaSum;
            $nestedData[] = $res1[0]->wowSum+$res2[0]->wowSum;
            $nestedData[] = $res1[0]->sadSum+$res2[0]->sadSum;
            $nestedData[] = $res1[0]->angrySum+$res2[0]->angrySum;
            $nestedData[] = $res1[0]->shareSum+$res2[0]->shareSum;
            $nestedData[] = ($qwrt_res1[0]->BReached_new || $qwrt_res2[0]->BReached_new) ? $qwrt_res1[0]->BReached_new+$qwrt_res2[0]->BReached_new : 0;
            $nestedData[] = ($qwrt_res1[0]->Error_new || $qwrt_res2[0]->Error_new) ? $qwrt_res1[0]->Error_new+$qwrt_res2[0]->Error_new : 0;
            $nestedData[] = ($qwrt_res1[0]->Success_new || $qwrt_res2[0]->Success_new) ? $qwrt_res1[0]->Success_new+$qwrt_res2[0]->Success_new : 0;
           // $nestedData[] = $row['Total'];

            $total_reached = $qwrt_res1[0]->BReached_new+$qwrt_res2[0]->BReached_new;
            if($total_reached > 0 ) {
                $per = round(($qwrt_res1[0]->Success_new+$qwrt_res2[0]->Success_new)/$total_reached, 6)*100;
                $nestedData[] = number_format($per,2,',', '') . "%";
            }else{                
                $nestedData[] = number_format(0,2,',', '') . "%";
            }   
            $nestedData[] = appDateTimeConvert($row['lastUpdates']);
            $data[] = $nestedData;
            $i++;
        }
        $json_data = array(
            "draw"            => intval( $requestData['draw'] ),
            "recordsTotal"    => intval( $totalData ),
            "recordsFiltered" => intval( $totalFiltered ),
            "data"            => $data
        );
        echo json_encode($json_data);exit;
    }

    public function viewBroadcastsAll(){
        $this->viewBuilder()->layout('admin_inner');
        $this->loadComponent('Paginator');
        $liveStreamModel = TableRegistry::get("live_streams");
        $CommingUsersModel = TableRegistry::get("comming_users");
        $BroadcastDetailModel = TableRegistry::get("broadcast_details");
        $applicationsModel = TableRegistry::get('Applications');
        $applications = $applicationsModel->find('all')->toArray();

        $userId = $this->Auth->user('id');
        if($this->Auth->user('parent_id') != 0){
            $userId = $this->Auth->user('parent_id');
        }
        $userRole = $this->Auth->user('role');
        $userSegment = $this->Auth->user('segment_id');
        $SegmentModel = TableRegistry::get('Segments');
        $SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
        $userSegmentId=0;
        if(!empty($SegmentData)){
          $userSegmentId = $SegmentData->segment_admin;
        }
        $usersModel = TableRegistry::get("Users");
        $usersList  = $usersModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'id'
        ])->where(['segment_id' =>$userSegment,'role !=' =>5])->toArray();
        $usersList[$userId] = $this->Auth->user('id');
        if($userRole==5){
          if($userId==$userSegmentId){  
            $applications = $applicationsModel->find('all')->contain(['Users','Groups'])->where(['Users.id IN'=> $usersList])->toArray();
          }else{
            $applications = $applicationsModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$userId])->toArray();  
          }  
        }else{
          $applications = $applicationsModel->find('all')->toArray();
        }
        //echo "<pre>"; print_r($applications);die;
        $apps[0] = 'All';
        foreach($applications as $key => $value){
            $apps[$value['application_name']] = ucfirst( $value['application_name'] );
        }

        //////////////Start - update youtube reaches//////////

        $FormModel = TableRegistry::get("form_listings");
        $form_data = $FormModel->find('all')->where(['owner_id'=>$userId])->first();

        $commingUsersModel = TableRegistry::get("comming_users");
        $commingUsers = $commingUsersModel->find('all')->where(['form_id' => $form_data->form_id, 'flag'=> "youtube"])->toArray(); 
         if (!empty($commingUsers)){
            foreach ($commingUsers as $user){
                if($user->channelId != ""){
                    $option = array(
                         'part' => 'snippet,statistics', 
                         'id' => $user->channelId,
                         //'key' => "AIzaSyB7qIKLbjvc0DdP3HNpltuMNdBzBo_afUI"
             'key' => YOUTUBE_KEY
                        );
                 $channelInfo = $this->getCurl('https://www.googleapis.com/youtube/v3/channels?'.http_build_query($option, 'a', '&'));
                 $channelInfo = json_decode($channelInfo);

                 ///write log for youtube api limit reach//////
                $log_data_new = array('controller' => 'Comminguser','functionName' => 'viewBroadcasts', 'apiName' =>  'https://www.googleapis.com/youtube/v3/channels' );
                $textdata = PHP_EOL. date("Y-m-d H:i:s")."-> Data -> ". print_r($log_data_new, true);
                $my_file = fopen("../webroot/youtube_limit_reach.txt", "a") or die("Unable to open file!");
                fwrite($my_file, $textdata);        
                fclose($my_file);
                ///write log for youtube api limit reach//////
                 
                 $channelSub = $channelInfo->items[0]->statistics->subscriberCount;
                    $userStatus = $commingUsersModel->newEntity();  
                    $userStatus->id = $user['id'];
                    $userStatus->followers = $channelSub;
                    $commingUsersModel->save($userStatus);
                        
                    
                }
            
            }
        }

        /////////////Stop - update youtube reaches////////

          // Check user is ich and active or not
        $isIchExpired = 0;
        $isAnyExpired = 0;
        $user_details = $usersModel->find('all')->where(['id' => $userId])->first();
        if(count($user_details) == 1){
            $userPlansModel = TableRegistry::get("user_plans");
            $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$userId, 'is_expired'=>0])->first();
            if(count($userPlansData) == 1){
                $isIchExpired = 0;
                $isAnyExpired = 0;
            }else{
                if($user_details->registerFrom == "ICH"){
                    $isIchExpired = 1;
                }else{
                    if($userRole != 1 && $userRole != 2){
                        $isAnyExpired = 1;
                    }else{
                        $isAnyExpired = 0;
                    }
                }
            }
        }
        $this->set('isIchExpired', $isIchExpired);
        $this->set('isAnyExpired', $isAnyExpired);
        $this->set('userRole', $userRole);
        // Check user is ich and active or not

        $this->set('applications', $apps);        
        $this->set('startDate', date( "m/1/Y 00:00:00"));
        $this->set('endDate', appTime(date( "m/d/Y H:i:s")));
        $this->set('submited', '0');
        $where_count_join = "stream_status = 2";
        $where_join = "stream_status = 2";
        $where =  array('stream_status' => 2 );
        if(isset($this->request->data)&& !empty($this->request->data)){
            $reportrange = $this->request->data['reportrange'];
            $range = explode(' - ', $reportrange);
            $range1 = str_replace("/", "-",$range[0]);
            $range2 = str_replace("/", "-",$range[1]);
            $start = date( "Y-m-d H:i:s", strtotime(convertToUtc($range1)));
            $end = date( "Y-m-d H:i:s", strtotime(convertToUtc($range2)));            
        
            $where = array_merge($where, array('stream_started >=' => $start, 'stream_started <=' => $end) );

            $where_count_join .= " AND stream_started >= '" .$start. "' AND stream_started <= '" . $end . "'";
            $where_join .= " AND stream_started >= '" .$start. "' AND stream_started <= '" . $end . "'";

            $this->set('startDate',date( "d/m/Y H:i:s", strtotime($range1) ));
            $this->set('endDate',date( "d/m/Y H:i:s", strtotime($range2) ));
            $this->set('submited', '1');
        }

        $appId = @$this->request->data['appid'];
        if ($appId) {
            $where = array_merge( $where, array('stream_app' => $appId) );
            $where_count_join .= " AND stream_app = " .$appId;
            $where_join .= " AND stream_app = " .$appId;
        }

        $userSegment = $this->Auth->user('segment_id');

        if(!in_array( $this->user('role'), ['1', '2'])) {
            $groupsModel = TableRegistry::get("Groups");
            if($userRole==5){
              if($userId==$userSegmentId && !empty($usersList)){    
                  $groupDataList = $groupsModel->find('list', ['keyField' => 'id','valueField' => 'id'])->where(['owner_id IN'=>$usersList])->toArray();
                  if(!empty($groupDataList)){
                     $implodeGroupList =implode(",",$groupDataList);      
                  $where = array_merge( $where, array('group_id IN' => $groupDataList ) );
                  $where_count_join .= " AND live_streams.group_id IN (" .$implodeGroupList.")";
                  $where_join .= " AND LiveStream.group_id IN (" .$implodeGroupList.")";
                  }
              }else{
                $groupData = $groupsModel->find('all')->where(['owner_id'=>$userId])->first();
                if(!empty($groupData)){
                    $where = array_merge( $where, array('group_id' => $groupData['id'] ) );
                    $where_count_join .= " AND live_streams.group_id = " .$groupData['id'];
                    $where_join .= " AND LiveStream.group_id = " .$groupData['id'];
                }else{
                    $where = array('group_id' => 0);
                    $where_count_join .= " AND live_streams.group_id = 0";
                    $where_join .= " AND LiveStream.group_id = 0";
                }   
              }
            }else{
            $groupData = $groupsModel->find('all')->where(['owner_id'=>$this->getManager()])->first();
            $where = array_merge( $where, array('group_id' => $groupData['id'] ) );
            $where_count_join .= " AND live_streams.group_id = " .$groupData['id'];
            $where_join .= " AND LiveStream.group_id = " .$groupData['id'];
        }

        }
        
        //echo "<pre>";
        //print_r($where);die;
        

        $conn = ConnectionManager::get('default');
        $TotalReachedQuery = "SELECT IFNULL(SUM(user.followers),0) as Reached FROM `live_streams` as LiveStream LEFT JOIN `broadcast_details` as detail ON detail.stream_id = LiveStream.id LEFT JOIN `comming_users` as user ON detail.target_id = user.id where " . $where_join." AND detail.status != 'UNPUBLISHED'";
        $TotalBroadcastQuery = "SELECT SUM(TIME_TO_SEC(TIMEDIFF(LiveStream.stream_ends, LiveStream.stream_started))) as diff FROM `live_streams` as LiveStream where " . $where_join;

              $FormListingModel = TableRegistry::get("form_listings");
        $FormListingData = $FormListingModel->find('all')->where(['owner_id'=>$userId])->first();
        $userRole = $this->Auth->user('role');
        if($userRole != 1 && $userRole != 2){
            $this->set('form_id',$FormListingData->form_id);
            $liveBroadDataCount = $liveStreamModel->find('all')->where([$where])->count();
        $broadcastResults = $conn->execute( $TotalBroadcastQuery )->fetch('assoc');
            $followersResults = $conn->execute( $TotalReachedQuery )->fetch('assoc');
            $this->set('followersSum', $followersResults['Reached']);
        $this->set('totalBroadCasted', secToStamp($broadcastResults['diff']));
            $this->set('liveBroadDataCount', $liveBroadDataCount);    
        }else{
            $this->set('form_id',"");
            $globalDataModel = TableRegistry::get("global_data");
            $globalData = $globalDataModel->newEntity();
            $data = $globalDataModel->find('all')->first();
            if(!empty($data)){
               $followersResults = $data->followersSum;
               $broadcastResults = $data->totalBroadCasted;
               $liveBroadDataCount = $data->liveBroadDataCount;
            }
            $this->set('followersSum', $followersResults);
            $this->set('totalBroadCasted', secToStamp($broadcastResults));
            $this->set('liveBroadDataCount', $liveBroadDataCount);
        }

        $query = array();               
        $query['count']  = "select count(*) AS count from (SELECT live_streams.id  FROM `broadcast_details` LEFT JOIN `comming_users` as user ON broadcast_details.target_id = user.id join live_streams on live_streams.id=broadcast_details.stream_id where " . $where_count_join."
GROUP BY live_streams.id) as countedtable";

        $query['detail'] = "SELECT
            LiveStream.id, 
            LiveStream.group_id, 
            LiveStream.stream_app,
            LiveStream.stream_started,
            LiveStream.lastUpdates,
            LiveStream.stream_ends,
            LiveStream.startDate,
            LiveStream.Total,
            LiveStream.TotalInteraction,
            LiveStream.Error,
            LiveStream.Success,
            LiveStream.Reached, 
            LiveStream.vlikeSum, 
            LiveStream.shareSum,
            LiveStream.viewSum, 
            LiveStream.commentSum, 
            LiveStream.loveSum, 
            LiveStream.wowSum, 
            LiveStream.angrySum, 
            LiveStream.sadSum, 
            LiveStream.hahaSum,
            LiveStream.Broadcasted,
            LiveStream.fb_td_status,
            sum(broadcast_details.angry) as angryBSum,
            sum(broadcast_details.share) as shareBSum,
            sum(broadcast_details.comment) as commentBSum,
            sum(broadcast_details.view) as viewBSum,
            sum(broadcast_details.vlike) as vlikeBSum,
            sum(broadcast_details.love) as loveBSum,
            sum(broadcast_details.haha) as hahaBSum,
            sum(broadcast_details.wow) as wowBSum,
            sum(broadcast_details.sad) as sadBSum,
            sum(broadcast_details.dislike) as dislikeSum,
            IFNULL(SUM(user.followers),0) as BReached
            FROM `broadcast_details` LEFT JOIN `comming_users` as user ON broadcast_details.target_id = user.id join live_streams as LiveStream on LiveStream.id=broadcast_details.stream_id
            where " . $where_join;
        $this->request->session()->write('query', $query);


        $this->set('page','broadcasts');
        $this->set('pg','view_broadcasts_all');
        
        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
    }

    // Start- YT global report
    public function viewBroadcastsListYt(){
        $this->autoRender = false;
        $requestData= $this->request->data;

        extract($this->request->session()->read('query'));

        if($this->Auth->user('role') == 1 || $this->Auth->user('role') == 2){
            $columns = array(
            0 => 'id',
            1 => 'startDate',
            2 => '',
            3 => '',
            4 => 'Broadcasted',
            5 => '',
            6 => 'Appname',
            7 => 'TotalInteraction',
            8 => 'commentSum',
            9 => 'viewSum',
            10 => 'loveSum',
            11 => 'hahaSum',
            12 => 'wowSum',
            13 => 'sadSum',
            14 => 'angrySum',
            15 => 'shareSum',
            16 => 'BReached',
            17 => 'Error',
            18 => 'Success',
            19 => '',
            20 => 'lastUpdates'
        );
        }
        else{
        $columns = array(
            0 => 'id',
            1 => 'startDate',
            2 => '',
            3 => '',
            4 => 'Broadcasted',
            5 => '',
            6 => 'TotalInteraction',
            7 => 'commentSum',
            8 => 'viewSum',
            9 => 'loveSum',
            10 => 'hahaSum',
            11 => 'wowSum',
            12 => 'sadSum',
            13 => 'angrySum',
            14 => 'shareSum',
            15 => 'BReached',
            16 => 'Error',
            17 => 'Success',
            18 => '',
            19 => 'lastUpdates'
        );
    }
        $searchColumns = array('LiveStream.id', 'LiveStream.stream_started', 'LiveStream.stream_ends');

        $conn = ConnectionManager::get('default');
        $results = $conn->execute($count)->fetchAll('assoc');        
        $totalData = isset($results[0]['count']) ? $results[0]['count'] : 0;
        $totalFiltered = $totalData;

        // Check user is ich and active or not
        $userId = $this->Auth->user('id');
        $usersModel = TableRegistry::get("Users");
        
        $user_details = $usersModel->find('all')->where(['id' => $userId])->first();
        if(count($user_details) == 1){
            $userPlansModel = TableRegistry::get("user_plans");
            $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$userId, 'is_expired'=>0])->first();
            if(count($userPlansData) == 0){
                if($user_details->registerFrom == "ICH"){
                    $totalData = 1;
                    $totalFiltered = $totalData;
                }else{
                    $totalData = 3;
                    $totalFiltered = $totalData;
                }
            }
        }
        // Check user is ich and active or not

        $sidx = $columns[$requestData['order'][0]['column']];
        $sord = $requestData['order'][0]['dir'];

        $search = $requestData['search']['value'];
        
        $where_search = "";
        if($search){
            $i = 0;

            foreach ($searchColumns as $value) {
                if($value){
                    if($i == 0){
                        $where_search .= " AND ( " . $value . " LIKE '%".$search."%'";
                    }else{
                        $where_search .= " OR " . $value . " LIKE '%".$search."%'";
                    }
                    if($i == count($searchColumns) - 1){
                        $where_search .= ")";
                    }
                    $i++;
                }
            }            
        }
        
        $start = $requestData['start'];
        $length = $requestData['length'];

        $SQL = $detail.$where_search." GROUP BY LiveStream.id ORDER BY $sidx $sord ";

        if($length > -1){
            $SQL .= "LIMIT $start , $length ";
        }

        $results = $conn->execute( $SQL )->fetchAll('assoc');
        $i = 0;
        $data = array();
        foreach ( $results as $row){

            /*
              Title : Used to get interactions from broadcast-detail table on "view-broadcasts" page
              author : Sandhya Ghatoda
              Created Date : 20-11-2019
            */

          if($this->Auth->user('role') == 3){
            $end_date = date('Y-m-d H:i:s', strtotime('-1 month'));
            $time_check = " AND LiveStream.stream_started > '".$end_date."'";
          }else{
            $time_check = '';
          }

            $qwry = "SELECT IFNULL(SUM(detail.vlike),0) as vlikeSum, IFNULL(SUM(detail.view),0) as viewSum, IFNULL(SUM(detail.comment),0) as commentSum, IFNULL(SUM(detail.love),0) as loveSum, IFNULL(SUM(detail.wow),0) as wowSum, IFNULL(SUM(detail.angry),0) as angrySum, IFNULL(SUM(detail.sad),0) as sadSum, IFNULL(SUM(detail.haha),0) as hahaSum, IFNULL(SUM(detail.dislike),0) as dislikeSum, IFNULL(SUM(detail.share),0) as shareSum from `live_streams` as LiveStream LEFT JOIN `broadcast_details` as detail ON detail.stream_id = LiveStream.id where detail.stream_id = ".$row["id"]." AND detail.type='yt'".$time_check;

            $res = $conn->execute( $qwry )->fetchAll('obj');

            $qwrt = "SELECT IFNULL(SUM(user.followers),0) as BReached_new, SUM(if(broadcast_details.status = 'UNPUBLISHED', user.followers, 0)) AS Error_new, SUM(if(broadcast_details.status != 'UNPUBLISHED', user.followers, 0)) AS Success_new FROM `broadcast_details` LEFT JOIN `comming_users` as user ON broadcast_details.target_id = user.id join live_streams as LiveStream on LiveStream.id=broadcast_details.stream_id where broadcast_details.type = 'yt' and LiveStream.id = ".$row["id"].$time_check;
            $qwrt_res = $conn->execute( $qwrt )->fetchAll('obj');

            $nestedData= [];
            $nestedData[] = "<a href=".HTTP_ROOT."commingusers/broadcast-details-youtube/".$row["id"].">".$row["id"]."</a>";
            $nestedData[] = ($row["startDate"] != "") ? appDate($row["startDate"]) : "";
            $nestedData[] = appTime($row["stream_started"]);
            $nestedData[] = appTime($row['stream_ends']);
            $nestedData[] = $row['Broadcasted'];
            if ($row['fb_td_status'] == 1){
                $nestedData[] = "<a href=".HTTP_ROOT."commingusers/fb-title/".$row["id"]." data-target='#myModal' data-toggle='ajaxModal'>".__(Read)."</a>";
            }else{
                $nestedData[] = __(Many);
            }

            if($this->Auth->user('role') == 1 || $this->Auth->user('role') == 2){
                $nestedData[] = "<a href=".HTTP_ROOT."formbuilder/listing/".$row["group_id"].">".$row["stream_app"]."</a>";
            }

            //$dataTotal->angry+$dataTotal->comment+$dataTotal->view+$dataTotal->vlike+$dataTotal->love+$dataTotal->haha+$dataTotal->wow+$dataTotal->sad+$dataTotal->dislike
            $nestedData[] =$res[0]->angrySum+$res[0]->shareSum+$res[0]->commentSum+$res[0]->vlikeSum+$res[0]->loveSum+$res[0]->hahaSum+$res[0]->wowSum+$res[0]->sadSum;// 'angry='.$row['angryBSum'].'commentBSum='.$row['commentBSum'].'vlikeBSum='.$row['vlikeBSum'].'viewBSum='.$row['viewBSum'].'loveBSum='.$row['loveBSum'].'hahaBSum='.$row['hahaBSum'].'wowBSum='.$row['wowBSum'].'sadBSum='.$row['sadBSum'].'dislikeSum='.$row['dislikeSum'];//$row['angryBSum']+$row['commentBSum']+$row['vlikeBSum']+$row['viewBSum']+$row['loveBSum']+$row['hahaBSum']+$row['wowBSum']+$row['sadBSum']+$row['dislikeSum'];//$row['TotalInteraction'];
            $nestedData[] = $res[0]->commentSum;
            //$nestedData[] = $res[0]->viewSum;
            $nestedData[] = $res[0]->vlikeSum;
            $nestedData[] = $res[0]->loveSum;
            $nestedData[] = $res[0]->hahaSum;
            $nestedData[] = $res[0]->wowSum;
            $nestedData[] = $res[0]->sadSum;
            $nestedData[] = $res[0]->angrySum;
            $nestedData[] = $res[0]->shareSum;
            $nestedData[] = ($qwrt_res[0]->BReached_new) ? $qwrt_res[0]->BReached_new : 0;
            $nestedData[] = ($qwrt_res[0]->Error_new) ? $qwrt_res[0]->Error_new : 0;
            $nestedData[] = ($qwrt_res[0]->Success_new) ? $qwrt_res[0]->Success_new : 0;
           // $nestedData[] = $row['Total'];
            if($qwrt_res[0]->BReached_new > 0 ) {
                $per = round($qwrt_res[0]->Success_new/$qwrt_res[0]->BReached_new, 6)*100;
                $nestedData[] = number_format($per,2,',', '') . "%";
            }else{                
                $nestedData[] = number_format(0,2,',', '') . "%";
            }   
            $nestedData[] = appDateTimeConvert($row['lastUpdates']);
            $data[] = $nestedData;
            $i++;
        }
        $json_data = array(
            "draw"            => intval( $requestData['draw'] ),
            "recordsTotal"    => intval( $totalData ),
            "recordsFiltered" => intval( $totalFiltered ),
            "data"            => $data
        );
        echo json_encode($json_data);exit;
    }


    public function viewBroadcastsYt(){
        $this->viewBuilder()->layout('admin_inner');
        $this->loadComponent('Paginator');
        $liveStreamModel = TableRegistry::get("live_streams");
        $CommingUsersModel = TableRegistry::get("comming_users");
        $BroadcastDetailModel = TableRegistry::get("broadcast_details");
        $applicationsModel = TableRegistry::get('Applications');
        $applications = $applicationsModel->find('all')->toArray();

        $userId = $this->Auth->user('id');
        if($this->Auth->user('parent_id') != 0){
            $userId = $this->Auth->user('parent_id');
        }
        $userRole = $this->Auth->user('role');
        $userSegment = $this->Auth->user('segment_id');
        $SegmentModel = TableRegistry::get('Segments');
        $SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
        $userSegmentId=0;
        if(!empty($SegmentData)){
          $userSegmentId = $SegmentData->segment_admin;
        }
        $usersModel = TableRegistry::get("Users");
        $usersList  = $usersModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'id'
        ])->where(['segment_id' =>$userSegment,'role !=' =>5])->toArray();
        $usersList[$userId] = $this->Auth->user('id');
        if($userRole==5){
          if($userId==$userSegmentId){  
            $applications = $applicationsModel->find('all')->contain(['Users','Groups'])->where(['Users.id IN'=> $usersList])->toArray();
          }else{
            $applications = $applicationsModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$userId])->toArray();  
          }  
        }else{
          $applications = $applicationsModel->find('all')->toArray();
        }
        //echo "<pre>"; print_r($applications);die;
        $apps[0] = 'All';
        foreach($applications as $key => $value){
            $apps[$value['application_name']] = ucfirst( $value['application_name'] );
        }

        //////////////Start - update youtube reaches//////////

        $FormModel = TableRegistry::get("form_listings");
        $form_data = $FormModel->find('all')->where(['owner_id'=>$userId])->first();

        $commingUsersModel = TableRegistry::get("comming_users");
        $commingUsers = $commingUsersModel->find('all')->where(['form_id' => $form_data->form_id, 'flag'=> "youtube"])->toArray(); 
         if (!empty($commingUsers)){
            foreach ($commingUsers as $user){
                if($user->channelId != ""){
                    $option = array(
                         'part' => 'snippet,statistics', 
                         'id' => $user->channelId,
                         //'key' => "AIzaSyB7qIKLbjvc0DdP3HNpltuMNdBzBo_afUI"
             'key' => YOUTUBE_KEY
                        );
                 $channelInfo = $this->getCurl('https://www.googleapis.com/youtube/v3/channels?'.http_build_query($option, 'a', '&'));
                 $channelInfo = json_decode($channelInfo);

                 ///write log for youtube api limit reach//////
                $log_data_new = array('controller' => 'Comminguser','functionName' => 'viewBroadcasts', 'apiName' =>  'https://www.googleapis.com/youtube/v3/channels' );
                $textdata = PHP_EOL. date("Y-m-d H:i:s")."-> Data -> ". print_r($log_data_new, true);
                $my_file = fopen("../webroot/youtube_limit_reach.txt", "a") or die("Unable to open file!");
                fwrite($my_file, $textdata);        
                fclose($my_file);
                ///write log for youtube api limit reach//////
                 
                 $channelSub = $channelInfo->items[0]->statistics->subscriberCount;
                    $userStatus = $commingUsersModel->newEntity();  
                    $userStatus->id = $user['id'];
                    $userStatus->followers = $channelSub;
                    $commingUsersModel->save($userStatus);
                        
                    
                }
            
            }
        }

        /////////////Stop - update youtube reaches////////

        // Check user is ich and active or not
        $isIchExpired = 0;
        $isAnyExpired = 0;
        $user_details = $usersModel->find('all')->where(['id' => $userId])->first();
        if(count($user_details) == 1){
            $userPlansModel = TableRegistry::get("user_plans");
            $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$userId, 'is_expired'=>0])->first();
            if(count($userPlansData) == 1){
                $isIchExpired = 0;
                $isAnyExpired = 0;
            }else{
                if($user_details->registerFrom == "ICH"){
                    $isIchExpired = 1;
                }else{
                    if($userRole != 1 && $userRole != 2){
                        $isAnyExpired = 1;
                    }else{
                        $isAnyExpired = 0;
                    }
                }
            }
        }
        $this->set('isIchExpired', $isIchExpired);
        $this->set('isAnyExpired', $isAnyExpired);
        $this->set('userRole', $userRole);
        // Check user is ich and active or not

        $this->set('applications', $apps);        
        $this->set('startDate', date( "m/1/Y 00:00:00"));
        $this->set('endDate', appTime(date( "m/d/Y H:i:s")));
        $this->set('submited', '0');
        $where_count_join = "stream_status = 2";
        $where_join = "stream_status = 2";
        $where =  array('stream_status' => 2 );
        if(isset($this->request->data)&& !empty($this->request->data)){
            $reportrange = $this->request->data['reportrange'];
            $range = explode(' - ', $reportrange);
            $range1 = str_replace("/", "-",$range[0]);
            $range2 = str_replace("/", "-",$range[1]);
            $start = date( "Y-m-d H:i:s", strtotime(convertToUtc($range1)));
            $end = date( "Y-m-d H:i:s", strtotime(convertToUtc($range2)));            
        
            $where = array_merge($where, array('stream_started >=' => $start, 'stream_started <=' => $end) );

            $where_count_join .= " AND stream_started >= '" .$start. "' AND stream_started <= '" . $end . "'";
            $where_join .= " AND stream_started >= '" .$start. "' AND stream_started <= '" . $end . "'";

            $this->set('startDate',date( "d/m/Y H:i:s", strtotime($range1) ));
            $this->set('endDate',date( "d/m/Y H:i:s", strtotime($range2) ));
            $this->set('submited', '1');
        }

        $appId = @$this->request->data['appid'];
        if ($appId) {
            $where = array_merge( $where, array('stream_app' => $appId) );
            $where_count_join .= " AND stream_app = " .$appId;
            $where_join .= " AND stream_app = " .$appId;
        }

        $userSegment = $this->Auth->user('segment_id');

        if(!in_array( $this->user('role'), ['1', '2'])) {
            $groupsModel = TableRegistry::get("Groups");
            if($userRole==5){
              if($userId==$userSegmentId && !empty($usersList)){    
                  $groupDataList = $groupsModel->find('list', ['keyField' => 'id','valueField' => 'id'])->where(['owner_id IN'=>$usersList])->toArray();
                  if(!empty($groupDataList)){
                     $implodeGroupList =implode(",",$groupDataList);      
                  $where = array_merge( $where, array('group_id IN' => $groupDataList ) );
                  $where_count_join .= " AND live_streams.group_id IN (" .$implodeGroupList.")";
                  $where_join .= " AND LiveStream.group_id IN (" .$implodeGroupList.")";
                  }
              }else{
                $groupData = $groupsModel->find('all')->where(['owner_id'=>$userId])->first();
                if(!empty($groupData)){
                    $where = array_merge( $where, array('group_id' => $groupData['id'] ) );
                    $where_count_join .= " AND live_streams.group_id = " .$groupData['id'];
                    $where_join .= " AND LiveStream.group_id = " .$groupData['id'];
                }else{
                    $where = array('group_id' => 0);
                    $where_count_join .= " AND live_streams.group_id = 0";
                    $where_join .= " AND LiveStream.group_id = 0";
                }   
              }
            }else{
            $groupData = $groupsModel->find('all')->where(['owner_id'=>$this->getManager()])->first();
            $where = array_merge( $where, array('group_id' => $groupData['id'] ) );
            $where_count_join .= " AND live_streams.group_id = " .$groupData['id'];
            $where_join .= " AND LiveStream.group_id = " .$groupData['id'];
        }

        }
        
        //echo "<pre>";
        //print_r($where);die;
        

        $conn = ConnectionManager::get('default');
        $TotalReachedQuery = "SELECT IFNULL(SUM(user.followers),0) as Reached FROM `live_streams` as LiveStream LEFT JOIN `broadcast_details` as detail ON detail.stream_id = LiveStream.id LEFT JOIN `comming_users` as user ON detail.target_id = user.id where " . $where_join." AND detail.status != 'UNPUBLISHED'  AND detail.type = 'yt'";
        $TotalBroadcastQuery = "SELECT SUM(TIME_TO_SEC(TIMEDIFF(LiveStream.stream_ends, LiveStream.stream_started))) as diff FROM `live_streams` as LiveStream where " . $where_join;

              $FormListingModel = TableRegistry::get("form_listings");
        $FormListingData = $FormListingModel->find('all')->where(['owner_id'=>$userId])->first();
        $userRole = $this->Auth->user('role');
        if($userRole != 1 && $userRole != 2){
            $this->set('form_id',$FormListingData->form_id);
            $liveBroadDataCount = $liveStreamModel->find('all')->where([$where])->count();
        $broadcastResults = $conn->execute( $TotalBroadcastQuery )->fetch('assoc');
            $followersResults = $conn->execute( $TotalReachedQuery )->fetch('assoc');
            $this->set('followersSum', $followersResults['Reached']);
        $this->set('totalBroadCasted', secToStamp($broadcastResults['diff']));
            $this->set('liveBroadDataCount', $liveBroadDataCount);    
        }else{
            $this->set('form_id',"");
            $globalDataModel = TableRegistry::get("global_data");
            $globalData = $globalDataModel->newEntity();
            $data = $globalDataModel->find('all')->first();
            if(!empty($data)){
               $followersResults = $data->followersSum;
               $broadcastResults = $data->totalBroadCasted;
               $liveBroadDataCount = $data->liveBroadDataCount;
            }
            $this->set('followersSum', $followersResults);
            $this->set('totalBroadCasted', secToStamp($broadcastResults));
            $this->set('liveBroadDataCount', $liveBroadDataCount);
        }

        $query = array();               
        $query['count']  = "select count(*) AS count from (SELECT live_streams.id  FROM `broadcast_details` LEFT JOIN `comming_users` as user ON broadcast_details.target_id = user.id join live_streams on live_streams.id=broadcast_details.stream_id where " . $where_count_join."
GROUP BY live_streams.id) as countedtable";

        $query['detail'] = "SELECT
            LiveStream.id, 
            LiveStream.group_id, 
            LiveStream.stream_app,
            LiveStream.stream_started,
            LiveStream.lastUpdates,
            LiveStream.stream_ends,
            LiveStream.startDate,
            LiveStream.Total,
            LiveStream.TotalInteraction,
            LiveStream.Error,
            LiveStream.Success,
            LiveStream.Reached, 
            LiveStream.vlikeSum, 
            LiveStream.shareSum,
            LiveStream.viewSum, 
            LiveStream.commentSum, 
            LiveStream.loveSum, 
            LiveStream.wowSum, 
            LiveStream.angrySum, 
            LiveStream.sadSum, 
            LiveStream.hahaSum,
            LiveStream.Broadcasted,
            LiveStream.fb_td_status,
            sum(broadcast_details.angry) as angryBSum,
            sum(broadcast_details.share) as shareBSum,
            sum(broadcast_details.comment) as commentBSum,
            sum(broadcast_details.view) as viewBSum,
            sum(broadcast_details.vlike) as vlikeBSum,
            sum(broadcast_details.love) as loveBSum,
            sum(broadcast_details.haha) as hahaBSum,
            sum(broadcast_details.wow) as wowBSum,
            sum(broadcast_details.sad) as sadBSum,
            sum(broadcast_details.dislike) as dislikeSum,
            IFNULL(SUM(user.followers),0) as BReached
            FROM `broadcast_details` LEFT JOIN `comming_users` as user ON broadcast_details.target_id = user.id join live_streams as LiveStream on LiveStream.id=broadcast_details.stream_id
            where " . $where_join;
        $this->request->session()->write('query', $query);


        $this->set('page','broadcasts');
         $this->set('pg','view_broadcasts_yt');
        
        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
    }
    // End- YT global report

    function ytTargetRegistrationErrorMessageEng(){
        echo '<table class="table" style="border-spacing: 15px;text-align: left;margin-left:10px;margin-right:10px;font-family: Lato, sans-serif;"><tbody>';

        echo '<tr style=""><td>YouTube returned an error with your registration. Please check the items below and if you are still unable to register your YouTube channel, contact us by clicking on the support icon (?), Located at the bottom of this page:</td></tr>';

        echo '<tr style=""><td>1 - Are you the real administrator (creator) of this channel on YouTube? Only channel creators can register.</td></tr>';

        echo '<tr style=""><td>2 - Is there 2-factor authentication enabled on this channel? Please disable, register and re-enable the function. This will only be necessary once.</td></tr>';

        echo '<tr style=""><td>3 - Did you receive any notification from YouTube to confirm anything? In that case, please check your YouTube notifications.</td></tr>';

        echo '<tr style=""><td>4 - Did you recently use any copyrighted soundtrack before, during or after your live, or something else that may have temporarily penalized your account for going live? In that case, please check your YouTube notifications.</td></tr>';

        echo '<tr style=""><td>5 - At <a target="_blank" href="https://www.youtube.com/features">www.youtube.com/features</a> is the live streaming module activated on your channel? You must be approved by YouTube to go live.</td></tr>';

        echo "<br/>";

        echo '<tr style=""><td><img src="'.HTTP_ROOT.'img/uploads/YTfeatures.jpg" style="" ></td></tr>';

        echo '<tr style=""><td>6 - At <a target="_blank" href="https://www.youtube.com/live_dashboard">www.youtube.com/live_dashboard</a> is the title or description field empty? These fields cannot go live if they are missing any text.</td></tr>';

        echo '<tr style=""><td><img src="'.HTTP_ROOT.'img/uploads/YTdashboard.jpg" style="" ></td></tr>';

        echo '<tr style=""><td>7 - At <a target="_blank" href="https://www.youtube.com/live_dashboard">www.youtube.com/live_dashboard</a>, is the channel privacy for publications set to PUBLIC or PRIVATE? It is not possible to register if you are PRIVATE.</td></tr>';

        echo '<tr style=""><td>Once again, if you are still unable to register your YouTube channel, contact us by clicking on the support icon (?), Located at the bottom of this page. Thanks.</td></tr>';

        echo '<tr style=""><td><b>MultiplierApp Team</b></td></tr>';

        echo '<tr style=""><td><img src="'.HTTP_ROOT.'img/logo_big.jpg" style="width: 300px;" height=""></td></tr>';

        echo '</tbody></table>';

        echo '<style>#bottom-help-img{z-index: 2147483646;width: 60px;height: 60px;font-size: 18px;font-weight: 400;overflow: hidden;justify-content: center;align-items: center;display: flex;cursor: pointer;position: fixed;right: 10px;bottom: 10px;}</style>';

        echo '<a target="_blank" href="https://api.whatsapp.com/send?phone=5521990555702&text=MultiplierApp%20Suporte.%20Por%20favor%2C%20salve%20esse%20n%C3%BAmero.%20MultiplierApp%20Helpdesk.%20Please%2C%20save%20this%20number."><img class = "img-zoom" src="'.HTTP_ROOT.'img/1962073065-Help.png" id="bottom-help-img"></a>';
    }

    function ytTargetRegistrationErrorMessagePort(){
        echo '<table class="table" style="border-spacing: 15px;text-align: left;margin-left:10px;margin-right:10px;font-family: Lato, sans-serif;"><tbody>';

        echo '<tr style=""><td>O YouTube retornou um erro em seu cadastramento. Por favor, verifique os itens abaixo e se você ainda não conseguir cadastrar seu canal YouTube, entre em contato conosco clicando no ícone de suporte (?), localizado na parte inferior desta página:</td></tr>';

        echo '<tr style=""><td>1 - Você é o verdadeiro administrador (criador) desse canal no YouTube? Apenas os criadores do canal pode cadastrar.</td></tr>';

        echo '<tr style=""><td>2 - Existe autenticação em 2 fatores habilitada nesse canal? Por favor desabilite, cadastre e reabilite a função. Isso só será necessário 1 vez.</td></tr>';

        echo '<tr style=""><td>3 - Você recebeu alguma notificação do YouTube para confirmar alguma coisa? Nesse caso, por favor verifique suas notificações do YouTube.</td></tr>';

        echo '<tr style=""><td>4 - Recentemente você usou alguma trilha sonora com direitos autorais antes, durante ou após a sua live, ou outra coisa que pode ter penalizado temporariamente sua conta para entrar ao vivo? Nesse caso, por favor verifique suas notificações do YouTube.</td></tr>';

        echo '<tr style=""><td>5 - Em <a target="_blank" href="https://www.youtube.com/features">www.youtube.com/features</a> está ativado o módulo de transmissão ao vivo em seu canal? É necessário estar aprovado pelo YouTube para entrar ao vivo.</td></tr>';

        echo "<br/>";

        echo '<tr style=""><td><img src="'.HTTP_ROOT.'img/uploads/YTfeatures.jpg" style="" ></td></tr>';

        echo '<tr style=""><td>6 - Em <a target="_blank" href="https://www.youtube.com/live_dashboard">www.youtube.com/live_dashboard</a> o campo título ou descrição está vazio? Esses campos não podem entrar ao vivo se estiverem sem qualquer texto.</td></tr>';

        echo '<tr style=""><td><img src="'.HTTP_ROOT.'img/uploads/YTdashboard.jpg" style="" ></td></tr>';

        echo '<tr style=""><td>7 - Em <a target="_blank" href="https://www.youtube.com/live_dashboard">www.youtube.com/live_dashboard</a>, a privacidade do canal para publicações está definida como PUBLICO ou PRIVADO? Não é possível cadastrar se estiver como PRIVADO.</td></tr>';

        echo '<tr style=""><td>Mais uma vez, se você ainda não conseguir cadastrar seu canal YouTube, entre em contato conosco clicando no ícone de suporte (?), localizado na parte inferior desta página.</td></tr>';

         echo '<tr style=""><td><b>Equipe MultiplierApp</b></td></tr>';

        echo '<tr style=""><td><img src="'.HTTP_ROOT.'img/logo_big.jpg" style="width: 300px;" height=""></td></tr>';

        echo '</tbody></table>';

        echo '<style>#bottom-help-img{z-index: 2147483646;width: 60px;height: 60px;font-size: 18px;font-weight: 400;overflow: hidden;justify-content: center;align-items: center;display: flex;cursor: pointer;position: fixed;right: 10px;bottom: 10px;}</style>';

        echo '<a target="_blank" href="https://api.whatsapp.com/send?phone=5521990555702&text=MultiplierApp%20Suporte.%20Por%20favor%2C%20salve%20esse%20n%C3%BAmero.%20MultiplierApp%20Helpdesk.%20Please%2C%20save%20this%20number."><img class = "img-zoom" src="'.HTTP_ROOT.'img/1962073065-Help.png" id="bottom-help-img"></a>';
    }

    function ytTargetRegistrationErrorMessageSpan(){
        echo '<table class="table" style="border-spacing: 15px;text-align: left;margin-left:10px;margin-right:10px;font-family: Lato, sans-serif;"><tbody>';

        echo '<tr style=""><td>YouTube devolvió un error con su registro. Verifique los elementos a continuación y si aún no puede registrar su canal de YouTube, contáctenos haciendo clic en el ícono de soporte (?), Ubicado en la parte inferior de esta página:</td></tr>';

        echo '<tr style=""><td>1 - ¿Eres el verdadero administrador (creador) de este canal en YouTube? Solo los creadores de canales pueden registrarse.</td></tr>';

        echo '<tr style=""><td>2 - ¿Hay habilitada la autenticación de 2 factores en este canal? Deshabilite, registre y vuelva a habilitar la función. Esto solo será necesario una vez.</td></tr>';

        echo '<tr style=""><td>3 - ¿Recibiste alguna notificación de YouTube para confirmar algo? En ese caso, verifique sus notificaciones de YouTube.</td></tr>';

        echo '<tr style=""><td>4 - ¿Utilizó recientemente alguna banda sonora con derechos de autor antes, durante o después de su concierto, o alguna otra cosa que pueda haber penalizado temporalmente su cuenta por publicarse? En ese caso, verifique sus notificaciones de YouTube.</td></tr>';

        echo '<tr style=""><td>5 - ¿Está <a target="_blank" href="https://www.youtube.com/features">www.youtube.com/features</a> el módulo de transmisión en vivo activado en su canal? YouTube debe aprobarlo para que se publique.</td></tr>';

        echo "<br/>";

        echo '<tr style=""><td><img src="'.HTTP_ROOT.'img/uploads/YTfeatures.jpg" style="" ></td></tr>';

        echo '<tr style=""><td>6 - En <a target="_blank" href="https://www.youtube.com/live_dashboard">www.youtube.com/live_dashboard</a>, ¿está vacío el campo de título o descripción? Estos campos no pueden activarse si les falta algún texto.</td></tr>';

        echo '<tr style=""><td><img src="'.HTTP_ROOT.'img/uploads/YTdashboard.jpg" style="" ></td></tr>';

        echo '<tr style=""><td>7 - En <a target="_blank" href="https://www.youtube.com/live_dashboard">www.youtube.com/live_dashboard</a>, ¿la privacidad del canal para publicaciones está establecida en PÚBLICO o PRIVADO? No es posible registrarse si es PRIVADO.</td></tr>';

        echo '<tr style=""><td>Nuevamente, si aún no puede registrar su canal de YouTube, contáctenos haciendo clic en el ícono de soporte (?), Ubicado en la parte inferior de esta página.</td></tr>';

         echo '<tr style=""><td><b>MultiplierApp Team</b></td></tr>';

        echo '<tr style=""><td><img src="'.HTTP_ROOT.'img/logo_big.jpg" style="width: 300px;" height=""></td></tr>';

        echo '</tbody></table>';

        echo '<style>#bottom-help-img{z-index: 2147483646;width: 60px;height: 60px;font-size: 18px;font-weight: 400;overflow: hidden;justify-content: center;align-items: center;display: flex;cursor: pointer;position: fixed;right: 10px;bottom: 10px;}</style>';

        echo '<a target="_blank" href="https://api.whatsapp.com/send?phone=5521990555702&text=MultiplierApp%20Suporte.%20Por%20favor%2C%20salve%20esse%20n%C3%BAmero.%20MultiplierApp%20Helpdesk.%20Please%2C%20save%20this%20number."><img class = "img-zoom" src="'.HTTP_ROOT.'img/1962073065-Help.png" id="bottom-help-img"></a>';
    }
}
?>