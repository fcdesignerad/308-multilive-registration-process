<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="../css/sonoclick.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->

<link rel="stylesheet" href="<?php echo HTTP_ROOT;?>css/invitation_default.css">

<style>

	<?php if ( $FormListingData->selected_image ){ ?>

		body{
			background-image: url(" <?=  $FormListingData->selected_image ?> ") !important;
		}

	<?php } ?>

</style>

<?php
	$form_data = json_decode( $FormListingData['form_body']);
	$form_data = json_decode( $form_data, true);
	$session = $this->request->session();
	$themeFa =  '<i class="fa fa-gift"></i>';
	$info_text = "";

	if($session->read( 'social.is_login' ) == "google"){
		$themeFa = '<i class="fa fa-youtube"></i>';
		$theme = $session->read('social.theme');
		$name           =  $session->read( 'social.name' );
		$identifier     =  $session->read( 'social.identifier' );
		$profile_url    = $session->read( 'social.profile_url' );
		$image          =  $session->read( 'social.image' );
		$email          = $session->read( 'social.email' ); 
		$access_token = $session->read('social.accesstoken');
		$logOutUrl = HTTP_ROOT."commingusers/view-form?id=".$_GET['id'].'&logout=1';
		$targname = "gmail";
	}
	else{

		$targname = "facebook";
		$name           =  $session->read( 'fb.name' );
		$identifier     =  $session->read( 'fb.identifier' );
		$profile_url    = $session->read( 'fb.profile_url' );
		$image          =  $session->read( 'fb.image' );
		$email          = $session->read( 'fb.email' ); 
		$access_token = $session->read('fb.accesstoken');
		$logOutUrl = "https://www.facebook.com/logout.php?next=".HTTP_ROOT."commingusers/view-form?id=".$_GET['id']."&logout=1&access_token=".@$access_token;
		$posts  = $session->read( 'fb.posts' );
		$pages  = $session->read( 'fb.accounts' );
		$groups = $session->read( 'fb.groups' );
		$events  = $session->read( 'fb.events' );
		$permission = $session->read( 'fb.permissions' );

		// var_dump($permission);
		// exit();

		$newEventsArr = array("data"=>array());
		foreach($events['data'] as $e){
			$status = @$this->Custom->checkTargetsEndTime($e['id'], 'event', $access_token);
			if($status){
				array_push($newEventsArr['data'], $e);
			}
		}
			
		$target_name = $app_name = 'mobilyte'  . '_FB_' . rand(0,9999) . time();

	}

	$is_success_wizard = ($session->read('successfull') ) ? true : false;

	$social_login = ( ( empty( $session->read( 'fb.is_login' ) )  ||  $session->read( 'fb.is_login' ) == '' ) && ( empty( $session->read( 'social.is_login' ) )  ||  $session->read( 'social.is_login' ) == '' ));

	// echo "<pre>";
	// var_dump($session->read('successfull'));
	// var_dump($session->read('newNetwork'));
	// var_dump($FormListingData);
	// var_dump($fbUserSubData);
	// var_dump($session->read( 'fb.is_login' ));
	// var_dump($session->read('social_selected_error'));
	// exit();

?>

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto&display=swap" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />

<div class="page-wrapper">
  <!-- BEGIN HEADER & CONTENT DIVIDER -->
  <div class="clearfix"> </div>
  <!-- END HEADER & CONTENT DIVIDER -->
  <!-- BEGIN CONTAINER -->
  <div class="page-container_dummy">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <!-- BEGIN CONTENT BODY -->
      <div class="page-content view_form_page">
        <div class="container">
          <div class="row view_form">
		  <form role="form" class="form-horizontal" method="POST" id="form_sample_1">
		  	<input type="hidden" name="uemail" id="uemail" value="<?php echo $uemail; ?>"/>
			<input type="hidden" name="uname" id="uname" value="<?php echo $uname; ?>"/>
			<input type="hidden" name="ucountry" id="ucountry" value="<?php echo $ucountry; ?>"/>
			<input type="hidden" name="uphone" id="uphone" value="<?php echo $uphone; ?>"/>
            <div class="col-sm-12 <?= !($social_login) ? "hide" : "" ?>" id = "content_wrapper">

				<?php 
					$selectedLanguage='Portuguese';
					$gifImage='';
					if($session->read('language') == "en_US"){
						$selectedLanguage='English';
						$url = "../img/facebook_login.png";
						$yurl = "../img/you_tube.png";
						$gifImage = "../img/public_en.gif";
						$form_content = $FormListingData->form_description;
						$terms_link = 'https://www.multiplierapp.live/terms-and-conditions/';
						$privacy_link = 'https://www.multiplierapp.live/privacy-policy/';
						
					}else if($session->read('language') == "en_SP"){
						$selectedLanguage='Spanish';
						$url = "../img/facebook_login.png";
						$yurl = "../img/you_tube.png";
						$gifImage = "../img/public_es.gif";
						$form_content = $FormListingData->form_description_sp;
						$terms_link ='https://www.multiplierapp.live/terminos-y-condiciones/';
						$privacy_link = 'https://www.multiplierapp.live/politica-de-privacidad/';

					}else{
						$selectedLanguage='Portuguese';
						$url = "../img/facebook_login.png";
						$yurl = "../img/you_tube.png";
						$gifImage = "../img/public_pt.gif";
						$form_content = $FormListingData->form_description_pt;
						$terms_link = 'https://www.multiplierapp.live/termos-e-condicoes/';
						$privacy_link = 'https://www.multiplierapp.live/politica-de-privacidade/';

					}      

					$form_content = ($form_content ) ? $form_content : __(causeDescritionText);
					$headline = ($form_title_lang ) ? $form_title_lang : __(causeTitle);
					$logo = ($FormListingData->selected_header_image) ? $FormListingData->selected_header_image : "../img/logo_big.jpg";
              	?>

              	<input type="hidden" name="form_lang" id="form_lang" value="<?= $selectedLanguage ?>"/>

              	<?php    
					if(!$session->read('successfull') && !isset($_GET['d'])){
				?>
				
						<div class = "row" id = "header_row">

							<div class = "col-md-3" id = "header_logo_wrapper">
								<img class = "img-zoom" src = <?= $logo ?> >
							</div>

							<div class = "col-md-9" id = "flag_wrapper">

								<div class="row flag_row">
									<div> 
										
										<a href="<?php echo HTTP_ROOT ;?>app/changeLanguage?language=2" class="flag <?php echo ($session->read('language') == "en_BR")?'active':''; ?>" >
											<img class = "img-zoom" src="<?php echo '../img/brasilflag.jpg' ; ?>" / >
										</a> 

										<a href="<?php echo HTTP_ROOT ;?>app/changeLanguage?language=1" class="flag <?php echo ($session->read('language') == "en_US")?'active':''; ?>">
											<img class = "img-zoom" src="<?php echo '../img/usaflag.jpg' ; ?>" / >
										</a>

										<a href="<?php echo HTTP_ROOT ;?>app/changeLanguage?language=3" class="flag <?php echo ($session->read('language') == "en_SP")?'active':''; ?>">
											<img class = "img-zoom" src="<?php echo '../img/spanish.jpg' ; ?>" / > 
										</a> 

									</div>
								</div>

							</div>

						</div>
						
						<div class="row detail_text <?= ($social_login) ?  : "hide"?>">

							<div class = "col-md-5" id = "invitation_headline_wrapper">
								<h2 id = "invitation_headline"><?= $headline?> </h2>
							</div>

								<div class = "col-md-7" id = "convite_text_wrapper">

									<div class = "new-content-container">	
										<?= $form_content ?>
									</div>

								</div>

						</div>

						<?php if ($social_login) { ?>

					  		<div class="row social_row ">

								<div class = "col-md-6" id = "col_social_text">

									<div class = "col-md-12">
										<img class = "img-zoom" id = "social_image" src = "https://multiplierapp.com.br/app/img/logo_big.jpg">
									</div>

									<div class="col-sm-12 choose_one">
										<p> <?= __(defaultPrivacyText) ?> <br> <?= __(formRegistrationPrivacy)?> </p>
									</div>

								</div>

								<div class = "col-md-6" id = "col_social_form"> 

									<div class="col-sm-12 col-form-comming-users">

										<form method="post" id="formsubmit" action="" class="formCommingUsers">

											<div class="form-group">
												<label class="control-label"><?php echo __(formRegistrationName); ?>*</label>
												<?php echo $this->Form->input('name', ['label' => false,'class'=>'form-control']); ?>
												<span id="form_error1" class="help-block help-block-error"></span>
											</div>

											<div class="form-group">
												<label class="control-label"><?php echo __(formRegistrationEmail); ?>*</label>
												<?php echo $this->Form->input('email', ['label' => false,'class'=>'form-control', 'type'=>'email']); ?>
												<span id="form_error1" class="help-block help-block-error"></span>
											</div>

											<div class="form-group">
												<label class="control-label"><?php echo __(formRegistrationMobile); ?>*</label>

												<div class="row">

													<div class="col-sm-3" style="clear: both;">
														<img id="countryflag" src="../img/flags/br.png" width="50"/>
													</div>

													<div class="col-sm-9">
														<select id="countries_dropdown" class="form-control" onChange="countyChanged()"> 

															<?php foreach($countries as $country){ 
																if(file_exists($_SERVER["DOCUMENT_ROOT"].'/app/webroot/img/flags/'.strtolower($country->iso2).'.png')){ ?>
																	<option value="<?php echo strtolower($country->iso2) ?>" data-contry-id="<?php echo $country->id; ?>" data-phone="<?php echo $country->phonecode; ?>" data-flag="<?php echo '/app/webroot/img/flags/'.strtolower($country->iso2).'.png';  ?>"><?php echo $country->name; ?></option>
															<?php }}?>

														</select>
													</div>

													<span id="form_error71" class="help-block help-block-error"></span>

												</div>

												<br/>

												<div class="row">
													<div class="col-sm-3" id = "country_code_wrapper" style="clear: both;">
														<input type="text" id="countrycode" class="form-control" value="+55" readonly />
													</div>

													<div class="col-sm-9" id = "cellphone_number_wrapper">
														<?php echo $this->Form->input('phone', ['label' => false,'class'=>'form-control']); ?>
													</div>

													<span id="form_error7" class="help-block help-block-error"></span>
													
												</div>

											</div>

										</form>

									</div>

									<div class="col-sm-6 fb fb-login"> <a id="get_values_facebook" href="javascript:void(0);" ><img src="<?php echo $url; ?>"></a> </div>
									<div class="col-sm-6 you_tube"> <a  href="javascript:void(0);" id="get_values_gmail"> <img src="<?php echo $yurl;?>"></a> </div> 

								</div>

								<div class="row fb_public fb_login_box" style="display:none">
									<div class="col-sm-12 fb_border">

										<div class="col-sm-12">
											<p class="heading"><?php echo __(social_select2);?></p>
										</div>

										<div class="col-sm-12">
											<img src="<?php echo $gifImage; ?>" >
										</div>

										<div class="col-sm-12">
											<button type="button" class="btn back_btn" style="float:left;margin-left:10px;"  onclick="$('.fb_login_box').hide();$('#col_social_text, #col_social_form, .flag_row, .img_row').show();">  <i class=" fa fa-angle-left"></i> <?php echo __(back);?>  </button>
											<button type="button" class="btn next_btn"  id="get_values"><?php echo __(next);?>  <i class=" fa fa-angle-right"></i></button>
										</div>  

									</div>                                         
					   			</div>

								<div class="row logo_row">

									<div class="col-sm-12"> 

										<?php if ( $_GET['id']!=2507){ ?>

											<hr>
											<h5 id = "share_text"> <b> <?php echo __(ShareThisInvitationToYourFriends); ?> </b> </h5>
											<div class="button-preview-copy ">
												<div class="sharethis-inline-share-buttons"
													data-title="<?= $headline ?>" 
													data-url="<?php  echo HTTP_ROOT; ?>commingusers/view-form?id=<?php echo $FormListingData['form_id']; ?>" 
													data-description="<?= $text ?>" 
													data-image="<?php echo HTTP_ROOT.'img/uploads/'.(@$siteinfo->site_logo != ''?@$siteinfo->site_logo:'logo.png'); ?>"></div>
									
											</div>
										<?php } ?>

									</div>

								</div>

            				</div>

              			<?php } else { ?>

							<!-- <input type="hidden" value="<?php echo isset($new_stream_url_secure)? $new_stream_url_secure:''; ?>" name="new_stream_url_secure">
							<input type="hidden" value="" name="json_data"> -->

              				<?php if($session->read( 'social.is_login' ) == "google") { ?>

								<!-- <div class="row fb_login_box youtube_login_box">

									<div class="col-sm-12 fb_border" style="background:#fff;">

										<p class="heading"><?php echo __(register_youtube); ?></p>

										<div class="login_form">

											<div class="row flex_row fb_user_row">

												<div class="col-sm-2 col-sm-offset-2"> <img src="<?php echo $image; ?>"> </div>

													<div class="col-sm-5">
														<p><strong><?php echo $name; ?> </strong> <?php echo __(register_youtube2); ?></p>
													</div>

												</div>

												<div class="row">

													<div class="col-sm-3 align-right">
														<label for="email" class="select_reg"><?php echo __(permission); ?>*</label>
													</div>

													<div class="col-sm-8">

														<div class="form-group">
															<select class="form-control" id="privacy" name="privacy"  placeholder="Input">
																<option value="public"><?php echo __(publics);?></option>
															</select>
														</div>

													</div>

												</div>

												<div class="row">

													<div class="col-sm-3 align-right">
														<label for="email" class="select_reg"></label>
													</div>

													<div class="col-sm-8">

														<div class="checkbox fb_checkbox">
															<label>
															<input type="checkbox" name="agree" value="1" required="required">
															&nbsp; <?php echo __(iagree); ?> <a target="_blank" href="<?php echo HTTP_ROOT.'terms' ?>"><?php echo __(terms); ?></a><?php echo " ".__(andmsg)." "; ?><a target="_blank" href="https://www.multiplierapp.live/politicas-de-uso/"><?php echo __(private_policy); ?></a></label>
														</div>

													</div>

												</div>

												<div class="row button_row">
													<div class="col-sm-12"> <a href="<?php echo $logOutUrl?>" id="change_user" class="btn logout next_btn"><i class="fas fa-sign-out-alt"></i> <?php echo __(logout);?> </a> <button type="submit" class="btn register next_btn"><?php echo __(REGISTER);?> <i class=" fa fa-angle-double-right"></i></button> </div>
												</div>

											</div>

										</div>

									</div>

								</div> -->

							<?php } else { ?> 

								<?php if($FormListingData['hide_share_option'] ==  "0"){ ?> 

									<!-- OLD SHARE -->
									<!-- <div id="social_step_1">

										<div class="row fb_login_box">

											<div class="col-sm-12 fb_border" style="background: #fff;">

												<p class="heading"><?php echo __(social_select4);?></p>

												<div class="login_form">
										
													<p><i class="fa fa-edit"></i></p>

													<div class="row">

														<div class="col-sm-6 col-sm-offset-3">

															<p><strong>Multiplier</strong> <?php echo __(social_select5);?></p>

															<div class="form-group">

																<select class="form-control" placeholder="Input" id="share" name="share">
																	<option value="" readonly><?php echo __(select); ?></option>
																	<option value="SELF"><?php echo __(Onlyme); ?></option>
																	<option value="ALL_FRIENDS"><?php echo __(friends); ?></option>
																	<option value="FRIENDS_OF_FRIENDS"><?php echo __(FRIENDS_OF_FRIENDS); ?></option>
																	<option value="EVERYONE"><?php echo __(publics); ?></option>
																</select>

																<span id="shareOptionRequired" style="display:none;color:red;"><?php echo __(Required);?></span>

															</div>

														</div>

													</div>

													<div class="row button_row">
														<div class="col-sm-12"> <a href="javascript:void(0);" onClick="fbShareOptionSelect()" class="btn next_btn"><?php echo __(next);?> <i class="fa fa-angle-right"></i></a> </div>
													</div>

													<?php if($session->read('social_selected_error')){ ?>

														<div class="alert alert-danger"><?php echo $session->read('social_selected_error'); 
														// $session->write('social_selected_error',"");
														?></div>

													<?php } ?>

												</div>

											</div>

										</div>

									</div> -->

								<?php  } ?>

								<!-- OLD SHARE -->
								<!-- <input id="share" name="share"  type="hidden" value="EVERYONE"> -->

								<!-- OLD SHARE -->
								<!-- <div id="social_step_<?php echo ($FormListingData['hide_share_option']=="0")?2:1;?>" style="<?php echo ($FormListingData['hide_share_option']=="0")?'display:none':'';?>">
										
									<div class="row fb_login_box">

										<div class="col-sm-12  fb_border" style="background: #fff;">

											<p class="heading"><?php echo __(social_select6); ?></p>

											<div class="login_form">

												<div class="row flex_row fb_user_row">

													<div class="col-sm-2 col-sm-offset-2"> <img src="<?php echo $image;?>"> </div>

													<div class="col-sm-5">
														<p><strong><?php echo ucwords($name);?> </strong> <?php echo __(not_facebook); ?></p>
													</div>

												</div>

												<div class="row">

													<div class="col-sm-3 align-right">
														<label for="email" class="select_reg"><?php echo __(Selectdestination); ?>:</label>
													</div>
													
													<div class="col-sm-8">
														<div class="form-group">
															<select style="" class="form-control" name="flag" id="flag" placeholder="Input">
																<option value="" readonly><?php echo __(cao); ?></option>
																<option value="timeline" ><?php echo __(yourfacebook); ?></option>
																<option value="page"><?php echo __(YourPages); ?></option>
																<option value="group"><?php echo __(YourGroup); ?></option>
																<option value="event"><?php echo __(YourEvent); ?></option>
															</select>
														</div>
													</div>

												</div>

												<div class="row"  id="stream_title" style="display:none;">

													<div class="col-sm-3 align-right">
														<label for="email" class="select_reg"><?php echo __(Streamtargetname); ?>*</label>
													</div>

													<div class="col-sm-8">

														<div class="form-group">
															<input class="form-control" name="target_name" type="text" value="<?php echo $target_name;?>" />
														</div>

													</div>

												</div>
											
												<div class="row"  id="show_pages" style="display:none;">

													<div class="col-sm-3 align-right">
														<label for="email" class="select_reg"><?php echo __(Mypages); ?>*</label>
													</div>

													<div class="col-sm-8">
														<div class="form-group">
															<select class="form-control" id="pages" name="pages"  placeholder="Input">
																<option value="" readonly><?php echo __(select); ?></option>
																<?php foreach($pages['data'] as $p){ ?>
																	<option value="<?php echo $p['id']; ?>"><?php echo $p['name']; ?></option>
																<?php } ?>
															</select>
														</div>
													</div>

												</div>
											
												<div class="row"  id="show_groups" style="display:none;">

													<div class="col-sm-3 align-right">
														<label for="email" class="select_reg"><?php echo __(Mygroup); ?>*</label>
													</div>

													<div class="col-sm-8">
														<div class="form-group">
															<select class="form-control" id="groups" name="groups"  placeholder="Input">
																<option value="" readonly><?php echo __(select); ?></option>
																<?php foreach($groups['data'] as $g){ ?>
																	<option value="<?php echo $g['id']; ?>"><?php echo $g['name']; ?></option>
																<?php } ?>
															</select>

															<p style="padding: 5px; margin: 5px; font-size: 13px;"><?php echo __(group_warning); ?></p>
														</div>
													</div>

												</div>

												<div class="row"  id="show_events" style="display:none;">

													<div class="col-sm-3 align-right">
														<label for="email" class="select_reg"><?php echo __(Myevent); ?>*</label>
													</div>

													<div class="col-sm-8">
														<div class="form-group">
															<select class="form-control" id="events" name="events"  placeholder="Input">
																<option value="" readonly><?php echo __(select); ?></option>
																<?php foreach($newEventsArr['data'] as $e){ ?>
																	<option value="<?php echo $e['id']; ?>"><?php echo $e['name']; ?></option>
																<?php } ?>
															</select>

															<p style="padding: 5px; margin: 5px; font-size: 13px;"><?php echo __(event_warning); ?></p>
														</div>
													</div>
												</div>

												<?php if($FormListingData['fb_td_status'] ==  "0"){ ?>  

													<div class="row">

														<div class="col-sm-3 align-right">
															<label for="email" class="select_reg"><?php echo __(title); ?>*</label>
														</div>

														<div class="col-sm-8">

															<div class="form-group">        
																<input class="form-control" name="stream_title" placeholder="" type="text" >
															</div>

														</div>
													</div>

													<div class="row">

														<div class="col-sm-3 align-right">
															<label for="email" class="select_reg"><?php echo __(description); ?>*</label>
														</div>

														<div class="col-sm-8">

															<div class="form-group">        
																<input class="form-control" name="stream_description" placeholder="" type="text" >
															</div>

														</div>
													</div>

												<?php } else{ ?>
													<input name="stream_title" type="hidden" value="<?php echo $FormListingData['facebook_title'];?> ">

													<?php //echo str_replace('"',"'",$FormListingData['facebook_description']);?>

													<input name="stream_description" type="hidden" value="<?php echo str_replace('"',"'",$FormListingData['facebook_description']);?>">

												<?php }?>

												<div class="row">
												
													<div class="col-sm-3 align-right">
														<label for="email" class="select_reg"></label>
													</div>

													<div class="col-sm-8">

														<div class="checkbox fb_checkbox">
															<label>
																<input type="checkbox" name="agree" value="1" required="required">
																&nbsp; <?php echo __(iagree); ?> <a target="_blank" href="<?php echo HTTP_ROOT.'terms' ?>"><?php echo __(terms); ?></a><?php echo " ".__(andmsg)." "; ?><a target="_blank" href="https://www.multiplierapp.live/politicas-de-uso/"><?php echo __(private_policy); ?></a>
															</label>
														</div>

													</div>

												</div>

												<div class="row button_row">
													<div class="col-sm-12"> 
														<a href="<?php echo $logOutUrl; ?>" id="change_user" class="btn logout next_btn"><i class="fas fa-sign-out-alt"></i> <?php echo __(logout); ?> </a> 
														<button type="submit"  class="btn register next_btn"><?php echo __(REGISTER); ?> <i class=" fa fa-angle-double-right"></i></button> 
													</div>
												</div>

												<input type="hidden" id="CommingUsersCount" value="<?php echo $CommingUsersCount; ?>">
												
												<?php if($session->read('social_selected_error')){ ?>
													<div class="alert alert-danger"><?php //echo $session->read('social_selected_error'); $session->write('social_selected_error',""); ?> </div>
												<?php } ?>
													
											</div>   

										</div>

									</div>

								</div> -->

					  		<?php  } ?>

					  	<?php } ?>

				<?php } else {

					// $is_success_wizard=true;
					if($session->read( 'social.is_login' ) == "google") {
				?>                 
						<!-- <div class="row fb_login_box youtube_sucess">

							<div class="col-sm-12 fb_border" style="background: #fff;">

								<p class="heading"><?php echo __(youtube_registered_successfully); ?> </p>

								<div class="login_form">          

									<p style="text-transform: none;"><?php echo __(NowWhatDoYouWantToDo); ?></p>

									<div class="row other_register">

										<div class="col-sm-4 youtube">
											<a href="javascript:void(0);" id="get_values_gmail"><?php echo __(register_another_youtube); ?></a>
										</div>      

										<div class="col-sm-4 untill_now">
											<a href="javascript:void(0);" onclick="$('#socialnetworkList').show()"><?php echo __(SeeAllRegisteredUntilNow); ?></a>
										</div>

										<div class="col-sm-4 facebook">
											<a href="javascript:void(0);" id="get_values"><?php echo __(register_another_facebook); ?></a>
										</div>

									</div>

								</div>

							</div>         

						</div> -->

					<?php } else {?>

						<!-- <div class="row fb_login_box youtube_sucess facebook_sucess">

							<div class="col-sm-12 fb_border" style="background: #fff;">

								<p class="heading"><?php echo __(facebook_registered_successfully); ?> </p>

									<div class="login_form">       

										<p style="text-transform: none;"><?php echo __(NowWhatDoYouWantToDo); ?></p>

										<div class="row other_register">

											<div class="col-sm-4 facebook">
												<a href="javascript:void(0);"  id="get_values"><?php echo __(register_another_facebook); ?></a>
											</div>

											<div class="col-sm-4 untill_now">
												<a href="javascript:void(0);" onclick="$('#socialnetworkList').show()"><?php echo __(SeeAllRegisteredUntilNow); ?></a>
											</div>

											<div class="col-sm-4 youtube">
												<a href="javascript:void(0);"  id="get_values_gmail"><?php echo __(register_another_youtube); ?></a>
											</div>

										</div>

									</div>

							</div>

						</div> -->

					<?php }

						// $session->write('successfull',""); 
				} ?> 


				<div id = "footer_card" style = "display: <?= ($FormListingData->hide_footer) ? "none" : "" ?>" >
					
					<?php if ( $_GET['id']!=2507){ ?> 

						<div class = "row" id = "footer_wrapper">

							<div class = "col-md-6">
								<p><a href="https://multiplierapp.com.br/"> <img class = "img-zoom" id = "footer_logo" src="https://multiplierapp.com.br/app/img/logo_big.jpg"> </a> </p>
							</div>

							<div class="col-md-5" id = "footer_login_wrapper">
								<a class = "btn btn-multiplier" href = "https://app.pipefy.com/public/form/H5C35JjZ"> <?= __(registration) ?> </a>
								<a class = "btn btn-multiplier btn-green" href = "https://multiplierapp.com.br/app/users/login"> Login </a>
							</div>

						</div>

					<?php } ?>

					<?php if (!empty($videoId) && $_GET['id']!=2507){ ?>

						<div class="row advertising_row">

							<div class="col-md-6"> 

								<?php if($videoType == 'youtube'){ ?>
									<iframe width="100%" height="315" src="<?php echo __(how_it_work_link) ?>" frameborder="0" allowfullscreen></iframe>
								<?php }else{ ?>
									<iframe src="https://player.vimeo.com/video/F13Gr68_yZo" width="100%" height="315" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								<?php } ?> 

							</div>

							<div class = "col-md-6">
								<iframe width="100%" height="315" id="youtube_video" src="<?php echo __(social_cause_link) ?>"> </iframe>
							</div>

						</div>
						
					<?php } ?>
								
					<?php if ( $_GET['id']!=2507){ ?>

						<div class="row knowMore_row">

							<div class="col-md-6">
								<p> <?php echo __(KnowMoreAboutUsAt); ?> <a href="http://www.multiplierapp.live" target="_blank">multiplierapp.live</a></p>
							</div>

							<div class = "col-md-5" id = "icons">

								<a href = "https://www.instagram.com/multiplierapp/"> <i class="fab fa-instagram img-zoom"></i> </a>
								<a href = "https://www.facebook.com/multiplierapp/"> <i class="fab fa-facebook-f img-zoom"></i> </a>
								<a href = "https://www.youtube.com/c/MultiplierApp"> <i class="fab fa-youtube img-zoom"></i> </a>
								<a href = "https://twitter.com/MultiplierApp"> <i class="fab fa-twitter img-zoom"></i> </a>
								<a href = "https://www.linkedin.com/company/multiplierapp/"> <i class="fab fa-linkedin-in img-zoom"></i> </a>

							</div>

						</div>

					<?php } ?>

					<div class = "row">
						
						<div class = "col-md-12" id = "app_stores_wrapper">
							<div id = "app_stores">
								<a href = "https://play.google.com/store/apps/details?id=com.br.multiplierapp"> <img class = "img-zoom footer-tag-img" src = "http://www.multiplierapp.fcdesigner.art.br/wp-content/uploads/2019/11/Multiplierapp-Baixe-no-google-play.png"> </a>
								<a href = "https://apps.apple.com/app/multiplierapp/id1301426934?ls=1"> <img class = "img-zoom footer-tag-img" src = "http://www.multiplierapp.fcdesigner.art.br/wp-content/uploads/2019/11/Multiplierapp-Baixe-no-apple-store.png"> </a>
							</div>
						</div>

					</div>

					<div class = "row">

						<div class = "col-md-12">
							<p id = "text_rights">@2020 MultiplierApp – Todos os direitos reservados / All rights reserved / Todos los derechos reservados.</p>
						</div>

					</div>

				</div>

            </div>

			<div class = "<?= !($social_login) ? "" : "hide" ?> <?= ($session->read( 'social.is_login' ) == "google") ? "share-step-google" : "share-step-facebook" ?>" id = "share_modal">
			
				<i class="fas fa-times img-zoom" id = "close_share_modal"></i>

				<div class = "<?= ($social_login) ? "" : "hide" ?>" id = "share_step_1">

					<h2 class = "share-tittle"> <?= __(iWantToMultiply) ?> </h2>
					<p class = "share-subtittle"> <?= __(chooseShareChanel) ?> </p>

					<div class = "row" id = "share_card_row">

						<div class = "share-card-wrapper share-card-not-mobile">

							<div class = "share-card" id = "personal_profile">

								<svg version="1.1" class = "share-card-icon" id = "personal_profile_icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									viewBox="0 0 511.997 511.997" style="enable-background:new 0 0 511.997 511.997;" xml:space="preserve">
									<g>
										<g>
											<path d="M157.897,185.906c-3.217-2.609-7.939-2.117-10.549,1.101c-3.03,3.736-7.047,5.793-11.313,5.793
												c-4.266,0-8.283-2.058-11.313-5.793c-2.609-3.217-7.333-3.709-10.549-1.101c-3.217,2.609-3.71,7.332-1.101,10.549
												c5.847,7.21,14.217,11.345,22.963,11.345c8.746,0,17.115-4.135,22.963-11.345C161.607,193.238,161.114,188.515,157.897,185.906z"
												/>
										</g>
									</g>
									<g>
										<g>
											<path d="M101.759,141.386c-4.142,0-7.5,3.358-7.5,7.5v8.569c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-8.569
												C109.259,144.744,105.901,141.386,101.759,141.386z"/>
										</g>
									</g>
									<g>
										<g>
											<path d="M170.311,141.386c-4.142,0-7.5,3.358-7.5,7.5v8.569c0,4.142,3.357,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-8.569
												C177.811,144.744,174.453,141.386,170.311,141.386z"/>
										</g>
									</g>
									<g>
										<g>
											<path d="M406.397,228.75c-3.217-2.609-7.94-2.117-10.549,1.101c-3.03,3.735-7.048,5.793-11.313,5.793
												c-4.266,0-8.283-2.058-11.313-5.793c-2.608-3.217-7.332-3.71-10.549-1.101c-3.217,2.609-3.709,7.332-1.1,10.549
												c5.846,7.211,14.216,11.345,22.962,11.345c8.746,0,17.115-4.135,22.962-11.345C410.106,236.082,409.614,231.359,406.397,228.75z"
												/>
										</g>
									</g>
									<g>
										<g>
											<path d="M350.259,184.231c-4.142,0-7.5,3.358-7.5,7.5v8.569c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-8.569
												C357.759,187.589,354.401,184.231,350.259,184.231z"/>
										</g>
									</g>
									<g>
										<g>
											<path d="M418.811,184.231c-4.142,0-7.5,3.358-7.5,7.5v8.569c0,4.142,3.357,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-8.569
												C426.311,187.589,422.953,184.231,418.811,184.231z"/>
										</g>
									</g>
									<g>
										<g>
											<path d="M491.126,332.545l-58.757-23.503c-0.318-0.127-0.612-0.289-0.91-0.445c40.583-9.234,59.803-24.676,60.656-25.375
												c2.029-1.663,3.042-4.265,2.671-6.862c-0.085-0.594-8.494-60.135-8.494-118.904c0-56.11-45.649-101.759-101.759-101.759
												c-56.11,0-101.758,45.648-101.758,101.759c0,15.374-0.698,34.178-1.834,51.979l-16.373,8.187l-48.06-24.029
												c-0.138-0.069-0.283-0.119-0.424-0.179c2.286-6.663,3.781-13.689,4.33-20.98c10.053-3.106,17.378-12.487,17.378-23.547
												c0-7.449-3.328-14.131-8.569-18.653v-49.9c0-13.785-6.965-26.683-18.387-34.433c-3.87-14.38-16.79-24.481-31.958-24.481h-68.552
												c-37.21,0-67.483,30.273-67.483,67.483v41.33c-5.241,4.521-8.569,11.204-8.569,18.653c0,11.06,7.325,20.441,17.378,23.547
												c1.894,25.179,14.87,47.302,34.036,61.54v20.73L23.664,273.31C9.51,277.557,0,290.338,0,305.116v83.701c0,4.142,3.358,7.5,7.5,7.5
												h1.069v86.759c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-86.759h27.845v86.759c0,4.142,3.358,7.5,7.5,7.5
												c4.142,0,7.5-3.358,7.5-7.5v-129.32c0-6.578-1.929-12.948-5.577-18.42l-12.82-19.23c-2.298-3.447-6.955-4.377-10.4-2.081
												c-3.447,2.297-4.378,6.954-2.08,10.401l12.82,19.231c2,3,3.058,6.492,3.058,10.099v27.561H16.069H15v-76.201
												c0-8.103,5.214-15.11,12.976-17.439l25.747-7.724c8.966,37.542,43.017,64.95,82.312,64.95c14.117,0,27.827-3.481,40.104-10.105
												c4.487,6.121,11.724,10.105,19.878,10.105h1.069v1.069c0,25.143,13.832,47.103,34.276,58.712v12.233
												c0,0.46-0.294,0.868-0.731,1.014l-27.977,9.326c-13.581,4.527-22.706,17.187-22.706,31.503v24.316c0,4.142,3.358,7.5,7.5,7.5
												c4.142,0,7.5-3.358,7.5-7.5V458.76c0-7.849,5.003-14.791,12.449-17.272l8.227-2.743c10.884,16.232,29.189,26.125,48.944,26.125
												c19.776,0,38.064-9.887,48.946-26.124l8.225,2.742c7.446,2.482,12.449,9.423,12.449,17.272v24.316c0,4.142,3.358,7.5,7.5,7.5
												c4.142,0,7.5-3.358,7.5-7.5V458.76c0-14.316-9.125-26.976-22.706-31.502l-27.977-9.326c-0.437-0.146-0.731-0.553-0.731-1.014
												v-12.233c15.307-8.692,26.893-23.188,31.753-40.481c14.173,14.83,33.968,23.546,55.005,23.546
												c33.212,0,62.353-21.469,72.391-52.728l28.629,11.451c6.953,2.781,11.445,9.417,11.445,16.904v35.079h-1.069h-26.776v-9.638
												c0-4.142-3.358-7.5-7.5-7.5c-4.142,0-7.5,3.358-7.5,7.5v94.259c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-69.621
												h19.276v69.621c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-69.621h1.069c1.989,0,3.896-0.79,5.303-2.196
												c1.407-1.407,2.197-3.314,2.197-5.304v-42.579C512,349.719,503.807,337.617,491.126,332.545z M297.776,157.455
												c0-47.839,38.92-86.759,86.759-86.759c47.839,0,86.758,38.92,86.758,86.759c0,50.363,6.065,101.274,8.036,116.479
												c-6.721,4.397-23.929,14.081-53.018,20.445v-11.935c3.984-2.258,7.798-4.835,11.384-7.733
												c14.758-11.929,25.187-28.636,29.363-47.042c1.392-6.133,2.098-12.458,2.098-18.799c0-42.391-30.657-48.477-60.305-54.363
												c-18.865-3.745-38.372-7.618-53.906-20.045c-2.251-1.801-5.335-2.152-7.934-0.903c-2.599,1.249-4.251,3.876-4.251,6.76
												c0,0.199-0.327,19.933-28.595,27c-4.019,1.005-6.462,5.077-5.457,9.096c1.004,4.019,5.077,6.46,9.095,5.457
												c23.66-5.915,33.312-19.188,37.248-29.16c16.264,9.638,34.483,13.254,50.879,16.509c31.689,6.291,48.226,10.912,48.226,39.65
												c0,5.226-0.581,10.434-1.726,15.48c-3.435,15.134-12.016,28.876-24.165,38.696c-12.5,10.104-27.622,15.444-43.73,15.444
												c-14.684,0-28.659-4.504-40.519-13.015c-4.345-11.859-11.324-22.709-20.533-31.649v-33.525c0-2.6-1.346-5.014-3.557-6.38
												c-2.211-1.366-4.971-1.491-7.297-0.328l-16.193,8.096C297.324,186.071,297.776,171.221,297.776,157.455z M308.483,212.435v27.143
												l-27.144-13.571L308.483,212.435z M220.655,212.435l27.143,13.572l-27.143,13.571V212.435z M179.948,294.558v7.123
												c-5.37,4.637-8.569,11.423-8.569,18.583c-10.682,6.311-22.817,9.639-35.345,9.639c-32.624,0-60.849-22.958-67.91-54.27
												l18.529-5.559c4.571,23.088,24.972,40.553,49.382,40.553c19.383,0,36.231-11.017,44.641-27.112
												C180.195,287.157,179.948,290.845,179.948,294.558z M100.69,260.27v-17.373c10.762,4.97,22.734,7.747,35.345,7.747
												c12.61,0,24.583-2.778,35.345-7.746v17.383c0,19.489-15.855,35.345-35.345,35.345c-19.457,0-35.29-15.804-35.343-35.249
												C100.692,260.341,100.69,260.305,100.69,260.27z M136.035,235.644c-38.39,0-69.621-31.231-69.621-69.62c0-4.142-3.358-7.5-7.5-7.5
												c-5.314,0-9.638-4.324-9.638-9.638c0-5.314,4.323-9.638,9.638-9.638h17.138c3.228,0,6.094-2.065,7.115-5.128l6.884-20.652
												c17.235-0.389,70.664-2.812,100.751-17.856c3.705-1.853,5.207-6.357,3.354-10.062c-1.852-3.705-6.356-5.207-10.062-3.354
												c-32.281,16.14-98.807,16.346-99.475,16.346c-3.228,0.001-6.094,2.066-7.114,5.129l-6.859,20.578H58.914
												c-0.358,0-0.714,0.012-1.069,0.027V88.903c0-28.939,23.544-52.483,52.483-52.483h68.552c8.956,0,16.48,6.455,17.893,15.347
												c0.359,2.26,1.73,4.232,3.723,5.356c8.468,4.776,13.729,13.669,13.729,23.21v43.941c-0.355-0.016-0.71-0.027-1.069-0.027h-8.569
												c-4.142,0-7.5,3.358-7.5,7.5c0,4.142,3.358,7.5,7.5,7.5h8.569c5.314,0,9.638,4.323,9.638,9.638s-4.323,9.638-9.638,9.638
												c-4.142,0-7.5,3.358-7.5,7.5C205.656,204.412,174.425,235.644,136.035,235.644z M186.379,260.283v-26.31
												c7.461-5.543,13.982-12.276,19.276-19.928v19.781c-8.399,8.154-14.946,17.895-19.329,28.543
												C186.355,261.676,186.379,260.982,186.379,260.283z M298.548,433.756c-8.257,10.087-20.661,16.113-33.979,16.113
												c-13.302,0-25.714-6.03-33.974-16.114l4.78-1.593c6.572-2.19,10.988-8.317,10.988-15.244v-5.978
												c5.796,1.627,11.898,2.516,18.207,2.516c6.309,0,12.411-0.889,18.207-2.516v5.978c-0.001,6.926,4.414,13.053,10.987,15.243
												L298.548,433.756z M324.553,329.903c-4.142,0-7.5,3.358-7.5,7.5v7.457c0,0.065-0.001,0.13,0,0.195v0.917
												c-0.001,28.939-23.545,52.483-52.484,52.483c-28.939,0-52.482-23.544-52.482-52.483v-8.569c0-4.142-3.358-7.5-7.5-7.5h-8.569
												c-5.314,0-9.638-4.323-9.638-9.638c0-3.42,1.81-6.515,4.841-8.279c0.677-0.394,1.266-0.893,1.776-1.457
												c18.54-0.537,78.639-4.026,118.138-26.597c3.596-2.055,4.846-6.636,2.791-10.233c-2.055-3.597-6.638-4.846-10.233-2.791
												c-34.602,19.772-88.974,23.731-108.744,24.522v-0.872c0-13.949,4.224-27.458,11.878-38.847c0.599,0.949,1.4,1.773,2.386,2.382
												c1.205,0.744,2.572,1.12,3.943,1.12c1.146,0,2.295-0.263,3.354-0.792l48.06-24.03l48.06,24.03
												c1.059,0.529,2.208,0.792,3.354,0.792c1.372,0,2.739-0.376,3.943-1.12c0.985-0.609,1.786-1.434,2.385-2.382
												c7.654,11.389,11.878,24.898,11.878,38.847v10.946c0,2.67,1.419,5.139,3.728,6.482c3.032,1.765,4.842,4.859,4.842,8.279
												c0,5.314-4.324,9.638-9.638,9.638H324.553z M384.535,372.748c-20.764,0-40.028-10.571-51.242-27.849
												c13.506-0.093,24.466-11.106,24.466-24.633c0-7.16-3.199-13.947-8.569-18.584v-7.123c0-3.019-0.163-6.021-0.482-8.995
												c11.121,5.214,23.263,7.925,35.827,7.925c9.145,0,18.189-1.515,26.776-4.379v10.983c0,10.133,6.079,19.113,15.487,22.876
												l16.173,6.469C435.22,355.066,411.55,372.748,384.535,372.748z"/>
										</g>
									</g>
									<g>
										<g>
											<path d="M286.432,357.285c-3.217-2.609-7.94-2.117-10.549,1.101c-3.031,3.736-7.048,5.793-11.313,5.793
												c-4.265,0-8.283-2.057-11.313-5.793c-2.609-3.217-7.333-3.71-10.549-1.101c-3.217,2.609-3.71,7.332-1.101,10.549
												c5.848,7.209,14.217,11.345,22.963,11.345c8.746,0,17.115-4.135,22.963-11.344C290.141,364.618,289.648,359.894,286.432,357.285z"
												/>
										</g>
									</g>
									<g>
										<g>
											<circle cx="239" cy="328.868" r="7.5"/>
										</g>
									</g>
									<g>
										<g>
											<circle cx="290" cy="328.868" r="7.5"/>
										</g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
								</svg>
								<p> <b> <?= __(personalProfile) ?> </b> </p>

							</div>

						</div>

						<div id = "share_card_mobile">

							<div class = "share-card-wrapper">

								<div class = "share-card" id = "personal_profile_mobile">

									<svg version="1.1" class = "share-card-icon" id = "personal_profile_icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										viewBox="0 0 511.997 511.997" style="enable-background:new 0 0 511.997 511.997;" xml:space="preserve">
										<g>
											<g>
												<path d="M157.897,185.906c-3.217-2.609-7.939-2.117-10.549,1.101c-3.03,3.736-7.047,5.793-11.313,5.793
													c-4.266,0-8.283-2.058-11.313-5.793c-2.609-3.217-7.333-3.709-10.549-1.101c-3.217,2.609-3.71,7.332-1.101,10.549
													c5.847,7.21,14.217,11.345,22.963,11.345c8.746,0,17.115-4.135,22.963-11.345C161.607,193.238,161.114,188.515,157.897,185.906z"
													/>
											</g>
										</g>
										<g>
											<g>
												<path d="M101.759,141.386c-4.142,0-7.5,3.358-7.5,7.5v8.569c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-8.569
													C109.259,144.744,105.901,141.386,101.759,141.386z"/>
											</g>
										</g>
										<g>
											<g>
												<path d="M170.311,141.386c-4.142,0-7.5,3.358-7.5,7.5v8.569c0,4.142,3.357,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-8.569
													C177.811,144.744,174.453,141.386,170.311,141.386z"/>
											</g>
										</g>
										<g>
											<g>
												<path d="M406.397,228.75c-3.217-2.609-7.94-2.117-10.549,1.101c-3.03,3.735-7.048,5.793-11.313,5.793
													c-4.266,0-8.283-2.058-11.313-5.793c-2.608-3.217-7.332-3.71-10.549-1.101c-3.217,2.609-3.709,7.332-1.1,10.549
													c5.846,7.211,14.216,11.345,22.962,11.345c8.746,0,17.115-4.135,22.962-11.345C410.106,236.082,409.614,231.359,406.397,228.75z"
													/>
											</g>
										</g>
										<g>
											<g>
												<path d="M350.259,184.231c-4.142,0-7.5,3.358-7.5,7.5v8.569c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-8.569
													C357.759,187.589,354.401,184.231,350.259,184.231z"/>
											</g>
										</g>
										<g>
											<g>
												<path d="M418.811,184.231c-4.142,0-7.5,3.358-7.5,7.5v8.569c0,4.142,3.357,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-8.569
													C426.311,187.589,422.953,184.231,418.811,184.231z"/>
											</g>
										</g>
										<g>
											<g>
												<path d="M491.126,332.545l-58.757-23.503c-0.318-0.127-0.612-0.289-0.91-0.445c40.583-9.234,59.803-24.676,60.656-25.375
													c2.029-1.663,3.042-4.265,2.671-6.862c-0.085-0.594-8.494-60.135-8.494-118.904c0-56.11-45.649-101.759-101.759-101.759
													c-56.11,0-101.758,45.648-101.758,101.759c0,15.374-0.698,34.178-1.834,51.979l-16.373,8.187l-48.06-24.029
													c-0.138-0.069-0.283-0.119-0.424-0.179c2.286-6.663,3.781-13.689,4.33-20.98c10.053-3.106,17.378-12.487,17.378-23.547
													c0-7.449-3.328-14.131-8.569-18.653v-49.9c0-13.785-6.965-26.683-18.387-34.433c-3.87-14.38-16.79-24.481-31.958-24.481h-68.552
													c-37.21,0-67.483,30.273-67.483,67.483v41.33c-5.241,4.521-8.569,11.204-8.569,18.653c0,11.06,7.325,20.441,17.378,23.547
													c1.894,25.179,14.87,47.302,34.036,61.54v20.73L23.664,273.31C9.51,277.557,0,290.338,0,305.116v83.701c0,4.142,3.358,7.5,7.5,7.5
													h1.069v86.759c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-86.759h27.845v86.759c0,4.142,3.358,7.5,7.5,7.5
													c4.142,0,7.5-3.358,7.5-7.5v-129.32c0-6.578-1.929-12.948-5.577-18.42l-12.82-19.23c-2.298-3.447-6.955-4.377-10.4-2.081
													c-3.447,2.297-4.378,6.954-2.08,10.401l12.82,19.231c2,3,3.058,6.492,3.058,10.099v27.561H16.069H15v-76.201
													c0-8.103,5.214-15.11,12.976-17.439l25.747-7.724c8.966,37.542,43.017,64.95,82.312,64.95c14.117,0,27.827-3.481,40.104-10.105
													c4.487,6.121,11.724,10.105,19.878,10.105h1.069v1.069c0,25.143,13.832,47.103,34.276,58.712v12.233
													c0,0.46-0.294,0.868-0.731,1.014l-27.977,9.326c-13.581,4.527-22.706,17.187-22.706,31.503v24.316c0,4.142,3.358,7.5,7.5,7.5
													c4.142,0,7.5-3.358,7.5-7.5V458.76c0-7.849,5.003-14.791,12.449-17.272l8.227-2.743c10.884,16.232,29.189,26.125,48.944,26.125
													c19.776,0,38.064-9.887,48.946-26.124l8.225,2.742c7.446,2.482,12.449,9.423,12.449,17.272v24.316c0,4.142,3.358,7.5,7.5,7.5
													c4.142,0,7.5-3.358,7.5-7.5V458.76c0-14.316-9.125-26.976-22.706-31.502l-27.977-9.326c-0.437-0.146-0.731-0.553-0.731-1.014
													v-12.233c15.307-8.692,26.893-23.188,31.753-40.481c14.173,14.83,33.968,23.546,55.005,23.546
													c33.212,0,62.353-21.469,72.391-52.728l28.629,11.451c6.953,2.781,11.445,9.417,11.445,16.904v35.079h-1.069h-26.776v-9.638
													c0-4.142-3.358-7.5-7.5-7.5c-4.142,0-7.5,3.358-7.5,7.5v94.259c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-69.621
													h19.276v69.621c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-69.621h1.069c1.989,0,3.896-0.79,5.303-2.196
													c1.407-1.407,2.197-3.314,2.197-5.304v-42.579C512,349.719,503.807,337.617,491.126,332.545z M297.776,157.455
													c0-47.839,38.92-86.759,86.759-86.759c47.839,0,86.758,38.92,86.758,86.759c0,50.363,6.065,101.274,8.036,116.479
													c-6.721,4.397-23.929,14.081-53.018,20.445v-11.935c3.984-2.258,7.798-4.835,11.384-7.733
													c14.758-11.929,25.187-28.636,29.363-47.042c1.392-6.133,2.098-12.458,2.098-18.799c0-42.391-30.657-48.477-60.305-54.363
													c-18.865-3.745-38.372-7.618-53.906-20.045c-2.251-1.801-5.335-2.152-7.934-0.903c-2.599,1.249-4.251,3.876-4.251,6.76
													c0,0.199-0.327,19.933-28.595,27c-4.019,1.005-6.462,5.077-5.457,9.096c1.004,4.019,5.077,6.46,9.095,5.457
													c23.66-5.915,33.312-19.188,37.248-29.16c16.264,9.638,34.483,13.254,50.879,16.509c31.689,6.291,48.226,10.912,48.226,39.65
													c0,5.226-0.581,10.434-1.726,15.48c-3.435,15.134-12.016,28.876-24.165,38.696c-12.5,10.104-27.622,15.444-43.73,15.444
													c-14.684,0-28.659-4.504-40.519-13.015c-4.345-11.859-11.324-22.709-20.533-31.649v-33.525c0-2.6-1.346-5.014-3.557-6.38
													c-2.211-1.366-4.971-1.491-7.297-0.328l-16.193,8.096C297.324,186.071,297.776,171.221,297.776,157.455z M308.483,212.435v27.143
													l-27.144-13.571L308.483,212.435z M220.655,212.435l27.143,13.572l-27.143,13.571V212.435z M179.948,294.558v7.123
													c-5.37,4.637-8.569,11.423-8.569,18.583c-10.682,6.311-22.817,9.639-35.345,9.639c-32.624,0-60.849-22.958-67.91-54.27
													l18.529-5.559c4.571,23.088,24.972,40.553,49.382,40.553c19.383,0,36.231-11.017,44.641-27.112
													C180.195,287.157,179.948,290.845,179.948,294.558z M100.69,260.27v-17.373c10.762,4.97,22.734,7.747,35.345,7.747
													c12.61,0,24.583-2.778,35.345-7.746v17.383c0,19.489-15.855,35.345-35.345,35.345c-19.457,0-35.29-15.804-35.343-35.249
													C100.692,260.341,100.69,260.305,100.69,260.27z M136.035,235.644c-38.39,0-69.621-31.231-69.621-69.62c0-4.142-3.358-7.5-7.5-7.5
													c-5.314,0-9.638-4.324-9.638-9.638c0-5.314,4.323-9.638,9.638-9.638h17.138c3.228,0,6.094-2.065,7.115-5.128l6.884-20.652
													c17.235-0.389,70.664-2.812,100.751-17.856c3.705-1.853,5.207-6.357,3.354-10.062c-1.852-3.705-6.356-5.207-10.062-3.354
													c-32.281,16.14-98.807,16.346-99.475,16.346c-3.228,0.001-6.094,2.066-7.114,5.129l-6.859,20.578H58.914
													c-0.358,0-0.714,0.012-1.069,0.027V88.903c0-28.939,23.544-52.483,52.483-52.483h68.552c8.956,0,16.48,6.455,17.893,15.347
													c0.359,2.26,1.73,4.232,3.723,5.356c8.468,4.776,13.729,13.669,13.729,23.21v43.941c-0.355-0.016-0.71-0.027-1.069-0.027h-8.569
													c-4.142,0-7.5,3.358-7.5,7.5c0,4.142,3.358,7.5,7.5,7.5h8.569c5.314,0,9.638,4.323,9.638,9.638s-4.323,9.638-9.638,9.638
													c-4.142,0-7.5,3.358-7.5,7.5C205.656,204.412,174.425,235.644,136.035,235.644z M186.379,260.283v-26.31
													c7.461-5.543,13.982-12.276,19.276-19.928v19.781c-8.399,8.154-14.946,17.895-19.329,28.543
													C186.355,261.676,186.379,260.982,186.379,260.283z M298.548,433.756c-8.257,10.087-20.661,16.113-33.979,16.113
													c-13.302,0-25.714-6.03-33.974-16.114l4.78-1.593c6.572-2.19,10.988-8.317,10.988-15.244v-5.978
													c5.796,1.627,11.898,2.516,18.207,2.516c6.309,0,12.411-0.889,18.207-2.516v5.978c-0.001,6.926,4.414,13.053,10.987,15.243
													L298.548,433.756z M324.553,329.903c-4.142,0-7.5,3.358-7.5,7.5v7.457c0,0.065-0.001,0.13,0,0.195v0.917
													c-0.001,28.939-23.545,52.483-52.484,52.483c-28.939,0-52.482-23.544-52.482-52.483v-8.569c0-4.142-3.358-7.5-7.5-7.5h-8.569
													c-5.314,0-9.638-4.323-9.638-9.638c0-3.42,1.81-6.515,4.841-8.279c0.677-0.394,1.266-0.893,1.776-1.457
													c18.54-0.537,78.639-4.026,118.138-26.597c3.596-2.055,4.846-6.636,2.791-10.233c-2.055-3.597-6.638-4.846-10.233-2.791
													c-34.602,19.772-88.974,23.731-108.744,24.522v-0.872c0-13.949,4.224-27.458,11.878-38.847c0.599,0.949,1.4,1.773,2.386,2.382
													c1.205,0.744,2.572,1.12,3.943,1.12c1.146,0,2.295-0.263,3.354-0.792l48.06-24.03l48.06,24.03
													c1.059,0.529,2.208,0.792,3.354,0.792c1.372,0,2.739-0.376,3.943-1.12c0.985-0.609,1.786-1.434,2.385-2.382
													c7.654,11.389,11.878,24.898,11.878,38.847v10.946c0,2.67,1.419,5.139,3.728,6.482c3.032,1.765,4.842,4.859,4.842,8.279
													c0,5.314-4.324,9.638-9.638,9.638H324.553z M384.535,372.748c-20.764,0-40.028-10.571-51.242-27.849
													c13.506-0.093,24.466-11.106,24.466-24.633c0-7.16-3.199-13.947-8.569-18.584v-7.123c0-3.019-0.163-6.021-0.482-8.995
													c11.121,5.214,23.263,7.925,35.827,7.925c9.145,0,18.189-1.515,26.776-4.379v10.983c0,10.133,6.079,19.113,15.487,22.876
													l16.173,6.469C435.22,355.066,411.55,372.748,384.535,372.748z"/>
											</g>
										</g>
										<g>
											<g>
												<path d="M286.432,357.285c-3.217-2.609-7.94-2.117-10.549,1.101c-3.031,3.736-7.048,5.793-11.313,5.793
													c-4.265,0-8.283-2.057-11.313-5.793c-2.609-3.217-7.333-3.71-10.549-1.101c-3.217,2.609-3.71,7.332-1.101,10.549
													c5.848,7.209,14.217,11.345,22.963,11.345c8.746,0,17.115-4.135,22.963-11.344C290.141,364.618,289.648,359.894,286.432,357.285z"
													/>
											</g>
										</g>
										<g>
											<g>
												<circle cx="239" cy="328.868" r="7.5"/>
											</g>
										</g>
										<g>
											<g>
												<circle cx="290" cy="328.868" r="7.5"/>
											</g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
									</svg>
								
									<p> <b> <?= __(personalProfile) ?> </b> </p>

								</div>

							</div>

							<div class = "share-card-wrapper">

								<div class = "share-card" id = "page_mobile">

									<svg xmlns="http://www.w3.org/2000/svg" class = "share-card-icon" id = "page_icon" viewBox="0 0 512 512" width="512" height="512"><path d="M472,24H40A24.028,24.028,0,0,0,16,48V464a24.028,24.028,0,0,0,24,24H472a24.028,24.028,0,0,0,24-24V48A24.028,24.028,0,0,0,472,24ZM40,40H472a8.009,8.009,0,0,1,8,8V88H32V48A8.009,8.009,0,0,1,40,40ZM472,472H40a8.009,8.009,0,0,1-8-8V104H480V464A8.009,8.009,0,0,1,472,472Z"/><circle cx="120" cy="64" r="8"/><circle cx="88" cy="64" r="8"/><circle cx="56" cy="64" r="8"/><path d="M440,136H72a8,8,0,0,0-8,8V272a8,8,0,0,0,8,8H440a8,8,0,0,0,8-8V144A8,8,0,0,0,440,136ZM308.849,254.465,328,235.313,356.687,264H316ZM296,264H232l32-42.667Zm-84,0H151.365L192,205.95l30.673,43.819Zm220,0H379.313l-45.656-45.657a8,8,0,0,0-11.314,0l-23.192,23.192L270.4,203.2a8,8,0,0,0-12.8,0l-24.825,33.1-34.221-48.888a8,8,0,0,0-13.108,0L131.835,264H80V152H432Z"/><circle cx="304" cy="192" r="8"/><circle cx="288" cy="304" r="8"/><circle cx="256" cy="304" r="8"/><circle cx="224" cy="304" r="8"/><path d="M168,328H72a8,8,0,0,0-8,8v64a8,8,0,0,0,8,8h96a8,8,0,0,0,8-8V336A8,8,0,0,0,168,328Zm-8,16v45.755l-35-28a8,8,0,0,0-10,0l-35,28V344Zm-22.806,48H102.806L120,378.245Z"/><path d="M304,328H208a8,8,0,0,0-8,8v64a8,8,0,0,0,8,8h96a8,8,0,0,0,8-8V336A8,8,0,0,0,304,328Zm-8,16v45.755l-35-28a8,8,0,0,0-10,0l-35,28V344Zm-22.806,48H238.806L256,378.245Z"/><path d="M440,328H344a8,8,0,0,0-8,8v64a8,8,0,0,0,8,8h96a8,8,0,0,0,8-8V336A8,8,0,0,0,440,328Zm-8,16v45.755l-35-28a8,8,0,0,0-10,0l-35,28V344Zm-22.806,48H374.806L392,378.245Z"/><path d="M152,424H88a8,8,0,0,0,0,16h64a8,8,0,0,0,0-16Z"/><path d="M288,424H224a8,8,0,0,0,0,16h64a8,8,0,0,0,0-16Z"/><path d="M424,424H360a8,8,0,0,0,0,16h64a8,8,0,0,0,0-16Z"/>
									</svg>
									<p> <b> <?= __(pages) ?> </b> </p>

								</div>			

							</div>

							<div class = "share-card-wrapper">

								<div class = "share-card" id = "group_mobile">

									<svg version="1.1" class = "share-card-icon" id = "group_icon" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
										<g>
											<g>
												<path d="M344.214,276.755l-43.582-19.37c-3.079-1.368-5.067-4.429-5.067-7.797V237.62c14.64-11.665,24.05-29.628,24.05-49.758
													v-16.847c4.823-2.665,8.016-7.815,8.016-13.583v-1.635c0-35.076-28.537-63.613-63.613-63.613h-16.032
													c-35.076,0-63.613,28.537-63.613,63.613v14.835c0,5.07,2.182,9.663,5.985,12.602c0.645,0.498,1.324,0.936,2.031,1.315v3.312
													c0,20.13,9.409,38.093,24.048,49.757v11.969c0,3.368-1.989,6.429-5.067,7.797l-43.582,19.37
													c-14.273,6.343-23.496,20.535-23.496,36.154v99.404c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-99.403
													c0-0.869,0.062-1.728,0.152-2.58l20.082,20.082c3.125,3.126,4.847,7.281,4.847,11.702v70.2c0,4.142,3.358,7.5,7.5,7.5
													c4.142,0,7.5-3.358,7.5-7.5v-70.2c0-8.427-3.281-16.349-9.24-22.308l-23.941-23.942c2.157-2.238,4.745-4.093,7.689-5.402
													l42.847-19.043l31.775,31.775v109.119c0,4.142,3.358,7.5,7.5,7.5s7.5-3.358,7.5-7.5V303.194l31.775-31.775l42.846,19.042
													c2.944,1.309,5.532,3.164,7.689,5.403l-23.941,23.941c-5.959,5.959-9.24,13.881-9.24,22.308v70.2c0,4.142,3.358,7.5,7.5,7.5
													c4.142,0,7.5-3.358,7.5-7.5v-70.2c0-4.42,1.721-8.576,4.847-11.702l20.081-20.081c0.09,0.852,0.152,1.709,0.152,2.578v99.404
													c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-99.403C367.709,297.29,358.487,283.098,344.214,276.755z M256,289.481
													l-27.881-27.882c2.12-3.567,3.317-7.694,3.317-12.011v-3.052c7.563,3.178,15.862,4.939,24.565,4.939s17.001-1.76,24.563-4.938
													v3.051c0,4.318,1.197,8.444,3.318,12.012L256,289.481z M256.001,236.474c-26.805,0.001-48.613-21.807-48.613-48.612v-3.373
													c4.68-1.855,9.248-4.807,13.621-8.842c3.043-2.809,3.234-7.554,0.424-10.598c-2.809-3.043-7.553-3.234-10.598-0.425
													c-3.751,3.463-7.499,5.7-11.137,6.65c-0.088,0.023-0.153,0.033-0.199,0.037c-0.054-0.105-0.127-0.325-0.127-0.678v-14.835
													c0-26.805,21.808-48.613,48.613-48.613h16.032c26.806,0,48.613,21.808,48.613,48.613v1.635c0,0.243-0.146,0.454-0.338,0.492
													c-44.911,8.87-67.437-7.847-75.037-15.447c-2.929-2.929-7.678-2.929-10.606,0c-2.929,2.929-2.929,7.678,0,10.606
													c23.29,23.29,57.275,23.71,77.965,21.224v13.553C304.614,214.666,282.806,236.474,256.001,236.474z"/>
											</g>
										</g>
										<g>
											<g>
												<path d="M135.242,235.961v-16.035c0-19.566-10.168-36.79-25.489-46.702c0.946-3.035,1.44-6.214,1.44-9.411
													c0-17.396-14.152-31.548-31.548-31.548s-31.548,14.153-31.548,31.548c0,3.237,0.49,6.385,1.438,9.413
													c-15.32,9.911-25.487,27.135-25.487,46.7v16.032c0,18.947,9.533,35.701,24.048,45.743v16.493c0,3.253-1.807,6.177-4.716,7.632
													l-25.941,12.97C6.682,324.176,0,334.988,0,347.015v65.299c0,4.142,3.358,7.5,7.5,7.5s7.5-3.358,7.5-7.5v-62.055l12.218,12.218
													c3.125,3.126,4.847,7.281,4.847,11.702v38.135c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-38.135
													c0-8.427-3.282-16.349-9.24-22.308L20.82,334.866c-0.082-0.082-0.17-0.153-0.254-0.23c1.067-0.942,2.264-1.762,3.582-2.422
													l25.941-12.97c1.329-0.665,2.566-1.45,3.719-2.322l18.337,18.338v77.055c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5
													v-77.056l18.338-18.338c1.154,0.873,2.39,1.658,3.719,2.322l7.153,3.577c1.077,0.539,2.221,0.794,3.349,0.794
													c2.751,0,5.4-1.52,6.714-4.147c1.853-3.705,0.351-8.21-3.354-10.062l-7.153-3.577c-2.91-1.455-4.717-4.379-4.717-7.631v-16.493
													C125.709,271.662,135.242,254.907,135.242,235.961z M79.645,147.265c9.125,0,16.548,7.424,16.548,16.548
													c0,1.005-0.09,1.99-0.262,2.955c-5.154-1.582-10.621-2.439-16.286-2.439c-5.657,0-11.117,0.854-16.263,2.432
													c-0.173-0.965-0.285-1.945-0.285-2.948C63.097,154.688,70.52,147.265,79.645,147.265z M79.645,179.33
													c22.385,0,40.597,18.212,40.597,40.597v2.718c-31.225,9.56-49.834-14.193-50.629-15.234c-1.305-1.739-3.3-2.827-5.468-2.981
													c-2.168-0.152-4.298,0.641-5.835,2.178c-7.159,7.159-14.171,10.612-19.235,12.279C39.63,196.981,57.609,179.33,79.645,179.33z
													M79.645,321.545l-17.331-17.331c0.506-1.934,0.783-3.95,0.783-6.019v-9.155c5.229,1.634,10.788,2.515,16.548,2.515
													s11.319-0.881,16.548-2.515v9.155c0,2.069,0.277,4.086,0.783,6.019L79.645,321.545z M79.645,276.555
													c-22.385,0-40.597-18.212-40.597-40.597v-1.554c5.981-1.374,14.849-4.567,24.1-12.007c3.273,3.088,7.951,6.876,13.908,10.125
													c7.046,3.843,16.777,7.398,28.663,7.398c4.517,0,9.35-0.525,14.459-1.731C119.015,259.541,101.281,276.555,79.645,276.555z"/>
											</g>
										</g>
										<g>
											<g>
												<path d="M494.561,302.765l-41.973-20.987c-2.909-1.455-4.716-4.379-4.716-7.631v-10.629c9.825-8.723,16.032-21.435,16.032-35.575
													v-8.537c0.173,0,0.343,0.005,0.516,0.005c4.142,0,7.5-3.358,7.5-7.5v-16.032c0-26.236-21.345-47.581-47.581-47.581h-16.032
													c-26.236,0-47.581,21.345-47.581,47.581v16.032c0,4.142,3.358,7.5,7.5,7.5c0.062,0,0.242-0.002,0.516-0.013v8.545
													c0,14.141,6.208,26.853,16.033,35.576v10.629c0,3.253-1.807,6.177-4.717,7.631c-3.705,1.852-5.207,6.357-3.354,10.062
													c1.314,2.628,3.963,4.148,6.714,4.148c1.127,0,2.271-0.255,3.348-0.793c1.329-0.665,2.566-1.45,3.719-2.322l18.338,18.338v101.103
													c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5V311.21l18.337-18.338c1.154,0.873,2.39,1.658,3.719,2.322l40.615,20.308
													l-12.319,12.319c-5.958,5.959-9.24,13.881-9.24,22.308v62.184c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-62.184
													c0-4.42,1.721-8.576,4.847-11.702l11.669-11.669c0.354,1.357,0.548,2.772,0.548,4.224v81.331c0,4.142,3.358,7.5,7.5,7.5
													c4.142,0,7.5-3.358,7.5-7.5v-81.331C512,318.956,505.318,308.144,494.561,302.765z M375.725,202.868v-6.99h0.001
													c0-17.965,14.616-32.581,32.581-32.581h16.032c17.965,0,32.581,14.616,32.581,32.581v8.331
													c-32.603-1.759-52.278-14.446-52.469-14.571c-1.269-0.846-2.717-1.26-4.158-1.26c-1.936,0-3.856,0.748-5.306,2.197
													C387.811,197.751,380.78,201.205,375.725,202.868z M416.323,297.497l-17.331-17.331c0.506-1.934,0.783-3.95,0.783-6.019v-1.597
													c5.156,1.919,10.731,2.973,16.548,2.973c5.817,0,11.392-1.054,16.548-2.973v1.597c0,2.069,0.276,4.085,0.783,6.019
													L416.323,297.497z M416.323,260.524c-17.965,0-32.581-14.615-32.581-32.581v-12.091h0c5.253-2.099,11.373-5.436,17.601-10.742
													c7.876,4.098,24.354,11.181,47.561,13.521v9.312C448.904,245.908,434.288,260.524,416.323,260.524z"/>
											</g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
										<g>
										</g>
									</svg>
									<p> <b> <?= __(groups) ?> </b> </p>

								</div>

							</div>

						</div>

						<div class = "share-card-wrapper share-card-not-mobile">

							<div class = "share-card" id = "page">

								<svg xmlns="http://www.w3.org/2000/svg" class = "share-card-icon" id = "page_icon" viewBox="0 0 512 512" width="512" height="512"><path d="M472,24H40A24.028,24.028,0,0,0,16,48V464a24.028,24.028,0,0,0,24,24H472a24.028,24.028,0,0,0,24-24V48A24.028,24.028,0,0,0,472,24ZM40,40H472a8.009,8.009,0,0,1,8,8V88H32V48A8.009,8.009,0,0,1,40,40ZM472,472H40a8.009,8.009,0,0,1-8-8V104H480V464A8.009,8.009,0,0,1,472,472Z"/><circle cx="120" cy="64" r="8"/><circle cx="88" cy="64" r="8"/><circle cx="56" cy="64" r="8"/><path d="M440,136H72a8,8,0,0,0-8,8V272a8,8,0,0,0,8,8H440a8,8,0,0,0,8-8V144A8,8,0,0,0,440,136ZM308.849,254.465,328,235.313,356.687,264H316ZM296,264H232l32-42.667Zm-84,0H151.365L192,205.95l30.673,43.819Zm220,0H379.313l-45.656-45.657a8,8,0,0,0-11.314,0l-23.192,23.192L270.4,203.2a8,8,0,0,0-12.8,0l-24.825,33.1-34.221-48.888a8,8,0,0,0-13.108,0L131.835,264H80V152H432Z"/><circle cx="304" cy="192" r="8"/><circle cx="288" cy="304" r="8"/><circle cx="256" cy="304" r="8"/><circle cx="224" cy="304" r="8"/><path d="M168,328H72a8,8,0,0,0-8,8v64a8,8,0,0,0,8,8h96a8,8,0,0,0,8-8V336A8,8,0,0,0,168,328Zm-8,16v45.755l-35-28a8,8,0,0,0-10,0l-35,28V344Zm-22.806,48H102.806L120,378.245Z"/><path d="M304,328H208a8,8,0,0,0-8,8v64a8,8,0,0,0,8,8h96a8,8,0,0,0,8-8V336A8,8,0,0,0,304,328Zm-8,16v45.755l-35-28a8,8,0,0,0-10,0l-35,28V344Zm-22.806,48H238.806L256,378.245Z"/><path d="M440,328H344a8,8,0,0,0-8,8v64a8,8,0,0,0,8,8h96a8,8,0,0,0,8-8V336A8,8,0,0,0,440,328Zm-8,16v45.755l-35-28a8,8,0,0,0-10,0l-35,28V344Zm-22.806,48H374.806L392,378.245Z"/><path d="M152,424H88a8,8,0,0,0,0,16h64a8,8,0,0,0,0-16Z"/><path d="M288,424H224a8,8,0,0,0,0,16h64a8,8,0,0,0,0-16Z"/><path d="M424,424H360a8,8,0,0,0,0,16h64a8,8,0,0,0,0-16Z"/>
								</svg>
								<p> <b> <?= __(pages) ?> </b> </p>

							</div>			

						</div>

						<div class = "share-card-wrapper share-card-not-mobile">

							<div class = "share-card" id = "group">

								<svg version="1.1" class = "share-card-icon" id = "group_icon" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
									<g>
										<g>
											<path d="M344.214,276.755l-43.582-19.37c-3.079-1.368-5.067-4.429-5.067-7.797V237.62c14.64-11.665,24.05-29.628,24.05-49.758
												v-16.847c4.823-2.665,8.016-7.815,8.016-13.583v-1.635c0-35.076-28.537-63.613-63.613-63.613h-16.032
												c-35.076,0-63.613,28.537-63.613,63.613v14.835c0,5.07,2.182,9.663,5.985,12.602c0.645,0.498,1.324,0.936,2.031,1.315v3.312
												c0,20.13,9.409,38.093,24.048,49.757v11.969c0,3.368-1.989,6.429-5.067,7.797l-43.582,19.37
												c-14.273,6.343-23.496,20.535-23.496,36.154v99.404c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-99.403
												c0-0.869,0.062-1.728,0.152-2.58l20.082,20.082c3.125,3.126,4.847,7.281,4.847,11.702v70.2c0,4.142,3.358,7.5,7.5,7.5
												c4.142,0,7.5-3.358,7.5-7.5v-70.2c0-8.427-3.281-16.349-9.24-22.308l-23.941-23.942c2.157-2.238,4.745-4.093,7.689-5.402
												l42.847-19.043l31.775,31.775v109.119c0,4.142,3.358,7.5,7.5,7.5s7.5-3.358,7.5-7.5V303.194l31.775-31.775l42.846,19.042
												c2.944,1.309,5.532,3.164,7.689,5.403l-23.941,23.941c-5.959,5.959-9.24,13.881-9.24,22.308v70.2c0,4.142,3.358,7.5,7.5,7.5
												c4.142,0,7.5-3.358,7.5-7.5v-70.2c0-4.42,1.721-8.576,4.847-11.702l20.081-20.081c0.09,0.852,0.152,1.709,0.152,2.578v99.404
												c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-99.403C367.709,297.29,358.487,283.098,344.214,276.755z M256,289.481
												l-27.881-27.882c2.12-3.567,3.317-7.694,3.317-12.011v-3.052c7.563,3.178,15.862,4.939,24.565,4.939s17.001-1.76,24.563-4.938
												v3.051c0,4.318,1.197,8.444,3.318,12.012L256,289.481z M256.001,236.474c-26.805,0.001-48.613-21.807-48.613-48.612v-3.373
												c4.68-1.855,9.248-4.807,13.621-8.842c3.043-2.809,3.234-7.554,0.424-10.598c-2.809-3.043-7.553-3.234-10.598-0.425
												c-3.751,3.463-7.499,5.7-11.137,6.65c-0.088,0.023-0.153,0.033-0.199,0.037c-0.054-0.105-0.127-0.325-0.127-0.678v-14.835
												c0-26.805,21.808-48.613,48.613-48.613h16.032c26.806,0,48.613,21.808,48.613,48.613v1.635c0,0.243-0.146,0.454-0.338,0.492
												c-44.911,8.87-67.437-7.847-75.037-15.447c-2.929-2.929-7.678-2.929-10.606,0c-2.929,2.929-2.929,7.678,0,10.606
												c23.29,23.29,57.275,23.71,77.965,21.224v13.553C304.614,214.666,282.806,236.474,256.001,236.474z"/>
										</g>
									</g>
									<g>
										<g>
											<path d="M135.242,235.961v-16.035c0-19.566-10.168-36.79-25.489-46.702c0.946-3.035,1.44-6.214,1.44-9.411
												c0-17.396-14.152-31.548-31.548-31.548s-31.548,14.153-31.548,31.548c0,3.237,0.49,6.385,1.438,9.413
												c-15.32,9.911-25.487,27.135-25.487,46.7v16.032c0,18.947,9.533,35.701,24.048,45.743v16.493c0,3.253-1.807,6.177-4.716,7.632
												l-25.941,12.97C6.682,324.176,0,334.988,0,347.015v65.299c0,4.142,3.358,7.5,7.5,7.5s7.5-3.358,7.5-7.5v-62.055l12.218,12.218
												c3.125,3.126,4.847,7.281,4.847,11.702v38.135c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-38.135
												c0-8.427-3.282-16.349-9.24-22.308L20.82,334.866c-0.082-0.082-0.17-0.153-0.254-0.23c1.067-0.942,2.264-1.762,3.582-2.422
												l25.941-12.97c1.329-0.665,2.566-1.45,3.719-2.322l18.337,18.338v77.055c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5
												v-77.056l18.338-18.338c1.154,0.873,2.39,1.658,3.719,2.322l7.153,3.577c1.077,0.539,2.221,0.794,3.349,0.794
												c2.751,0,5.4-1.52,6.714-4.147c1.853-3.705,0.351-8.21-3.354-10.062l-7.153-3.577c-2.91-1.455-4.717-4.379-4.717-7.631v-16.493
												C125.709,271.662,135.242,254.907,135.242,235.961z M79.645,147.265c9.125,0,16.548,7.424,16.548,16.548
												c0,1.005-0.09,1.99-0.262,2.955c-5.154-1.582-10.621-2.439-16.286-2.439c-5.657,0-11.117,0.854-16.263,2.432
												c-0.173-0.965-0.285-1.945-0.285-2.948C63.097,154.688,70.52,147.265,79.645,147.265z M79.645,179.33
												c22.385,0,40.597,18.212,40.597,40.597v2.718c-31.225,9.56-49.834-14.193-50.629-15.234c-1.305-1.739-3.3-2.827-5.468-2.981
												c-2.168-0.152-4.298,0.641-5.835,2.178c-7.159,7.159-14.171,10.612-19.235,12.279C39.63,196.981,57.609,179.33,79.645,179.33z
												M79.645,321.545l-17.331-17.331c0.506-1.934,0.783-3.95,0.783-6.019v-9.155c5.229,1.634,10.788,2.515,16.548,2.515
												s11.319-0.881,16.548-2.515v9.155c0,2.069,0.277,4.086,0.783,6.019L79.645,321.545z M79.645,276.555
												c-22.385,0-40.597-18.212-40.597-40.597v-1.554c5.981-1.374,14.849-4.567,24.1-12.007c3.273,3.088,7.951,6.876,13.908,10.125
												c7.046,3.843,16.777,7.398,28.663,7.398c4.517,0,9.35-0.525,14.459-1.731C119.015,259.541,101.281,276.555,79.645,276.555z"/>
										</g>
									</g>
									<g>
										<g>
											<path d="M494.561,302.765l-41.973-20.987c-2.909-1.455-4.716-4.379-4.716-7.631v-10.629c9.825-8.723,16.032-21.435,16.032-35.575
												v-8.537c0.173,0,0.343,0.005,0.516,0.005c4.142,0,7.5-3.358,7.5-7.5v-16.032c0-26.236-21.345-47.581-47.581-47.581h-16.032
												c-26.236,0-47.581,21.345-47.581,47.581v16.032c0,4.142,3.358,7.5,7.5,7.5c0.062,0,0.242-0.002,0.516-0.013v8.545
												c0,14.141,6.208,26.853,16.033,35.576v10.629c0,3.253-1.807,6.177-4.717,7.631c-3.705,1.852-5.207,6.357-3.354,10.062
												c1.314,2.628,3.963,4.148,6.714,4.148c1.127,0,2.271-0.255,3.348-0.793c1.329-0.665,2.566-1.45,3.719-2.322l18.338,18.338v101.103
												c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5V311.21l18.337-18.338c1.154,0.873,2.39,1.658,3.719,2.322l40.615,20.308
												l-12.319,12.319c-5.958,5.959-9.24,13.881-9.24,22.308v62.184c0,4.142,3.358,7.5,7.5,7.5c4.142,0,7.5-3.358,7.5-7.5v-62.184
												c0-4.42,1.721-8.576,4.847-11.702l11.669-11.669c0.354,1.357,0.548,2.772,0.548,4.224v81.331c0,4.142,3.358,7.5,7.5,7.5
												c4.142,0,7.5-3.358,7.5-7.5v-81.331C512,318.956,505.318,308.144,494.561,302.765z M375.725,202.868v-6.99h0.001
												c0-17.965,14.616-32.581,32.581-32.581h16.032c17.965,0,32.581,14.616,32.581,32.581v8.331
												c-32.603-1.759-52.278-14.446-52.469-14.571c-1.269-0.846-2.717-1.26-4.158-1.26c-1.936,0-3.856,0.748-5.306,2.197
												C387.811,197.751,380.78,201.205,375.725,202.868z M416.323,297.497l-17.331-17.331c0.506-1.934,0.783-3.95,0.783-6.019v-1.597
												c5.156,1.919,10.731,2.973,16.548,2.973c5.817,0,11.392-1.054,16.548-2.973v1.597c0,2.069,0.276,4.085,0.783,6.019
												L416.323,297.497z M416.323,260.524c-17.965,0-32.581-14.615-32.581-32.581v-12.091h0c5.253-2.099,11.373-5.436,17.601-10.742
												c7.876,4.098,24.354,11.181,47.561,13.521v9.312C448.904,245.908,434.288,260.524,416.323,260.524z"/>
										</g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
									<g>
									</g>
								</svg>
								<p> <b> <?= __(groups) ?> </b> </p>

							</div>

						</div>

					</div>

					<div class = "row" id = "share_footer_wrapper">

						<div id = "share_footer_img_wrapper">
							<img src="<?php echo $gifImage; ?>">
						</div>

						<div id = "share_footer_text_wrapper">
							<p> <?= __(shareFooterText) ?> </p>
							<p> <a href="<?php echo $terms_link; ?>" target="_blank"><?php echo __(terms_conditions); ?></a> <?php echo __(getand); ?> <a href="<?php echo $privacy_link; ?>" target="_blank"><?php echo __(privacy_policy); ?></a><?php echo " "; ?><?php echo __(getand); ?><?php echo " "; ?><a target="_blank"  href="<?php echo __(use_policy_link); ?>"><?php echo __(use_policy); ?></a>.</label></p>
						</div>

					</div>

				</div>

				<?php if($session->read('youtubeError')) { $session->write('youtubeError', false) ?>

					<div id = "youtube_share_error">

						<h2 class = "share-tittle" > 
							<?= __(youtubeRegisterError) ?> 
						</h2>

						<div id = "error_content_wrapper">

							<div id = "error_content">

								<table class = "table">

									<tbody>
										<tr>
											<td> <?= __(youtubeError1) ?> </td>
										</tr>
										<tr>
											<td> <?= __(youtubeError2) ?> </td>
										</tr>
										<tr>
											<td> <?= __(youtubeError3) ?> </td>
										</tr>
										<tr>
											<td> <?= __(youtubeError4) ?> </td>
										</tr>
										<tr>
											<td> <?= __(youtubeError5) ?> </td>
										</tr>
										<tr>
											<td> <a target="_blank" href="https://www.youtube.com/features">www.youtube.com/features</a> <?= __(youtubeError6) ?> </td>
										</tr>

										<br/>

										<tr style="">
											<td><img src="<?=HTTP_ROOT?>img/uploads/YTfeatures.jpg" style="" > </td>
										</tr>
										<tr>
											<td> <a target="_blank" href="https://www.youtube.com/live_dashboard">www.youtube.com/live_dashboard</a> <?= __(youtubeError7) ?> </td>
										</tr>
										<tr style="">
											<td><img src="<?=HTTP_ROOT?>img/uploads/YTdashboard.jpg" style="" ></td>
										</tr>
										<tr>
											<td> <a target="_blank" href="https://www.youtube.com/live_dashboard">www.youtube.com/live_dashboard</a>, <?= __(youtubeError8) ?> </td>
										</tr>
									</tbody>

								</table>

							</div>	
						
						</div>

						<ul id = "youtube_error_checklist">
							<li> <?= __(youtubeError9) ?></li>
							<li> <?= __(youtubeError10) ?></li>	
							<li> <?= __(youtubeError11) ?></li>
						</ul>

						<div id = "youtube_error_button_wrapper">
							<a href = "https://www.youtube.com/live_dashboard" target = "_blank"> <?= __(youtubeError12) ?></a>
							<a href = "https://www.youtube.com/features" target = "_blank"> <?= __(youtubeError13) ?></a>
						</div>

						<p id = "youtube_error_footer_message"> <?= __(youtubeError14) ?>  </p>

						<a href = "" target = "_blank">
							<i class = "fab fa-youtube" id = "share_youtube_icon" ></i>
						</a>

					</div>

				<?php } else { ?>

					<div class = "<?= (!($social_login) && !($session->read('newNetwork')) ) ? "" : "hide" ?>" id = "share_step_2">

						<h2 class = "share-tittle" > 
							<?= __(iWantToMultiply) ?> 
							<!-- <i class="fas fa-chevron-right show-social-networks-list"></i>  -->
						</h2>
						<p class = "share-subtittle" > <?= __(chooseProfile) ?> </p>
						
						<div class = "row" id = "perfil_row">

							<input type="hidden" id="CommingUsersCount" value="<?php echo $CommingUsersCount; ?>">
							<input type="hidden" value="<?php echo isset($new_stream_url_secure)? $new_stream_url_secure:''; ?>" name="new_stream_url_secure">
							<input type="hidden" value="" name="json_data">

							<div id = "profile_image_wrapper">
								<img id = "profile_image" src = "<?= $image;?>">
							</div>

							<div id = "profile_information">
								<h4 id = "profile_name"> <b> <?= ucwords($name);?> </b> <?= ($session->read( 'social.is_login' ) == "google") ? __(register_youtube2) : __(not_facebook) ?> </h4>

								<?php if($session->read( 'social.is_login' ) == "google") { ?>

									<div class="row">

										<div id = "privacy_label_wrapper">
											<label for="email" class="select_reg"><?php echo __(permission); ?>*</label>
										</div>

										<div>
											<div>
												<select class="form-control" id="privacy" name="privacy"  placeholder="Input">
													<option value="public"><?php echo __(publics);?></option>
												</select>
											</div>
										</div>

									</div>

								<?php } else { ?>

									<input id = "share" name = "share"  type = "hidden" value="EVERYONE">
									<input id = "flag" name = "flag" type = "hidden" value = "<?= $permission ?>">

									<?php if($FormListingData['hide_share_option'] ==  "0"){ ?>

										<div class = "row">
												
											<div>
												<label for="email"><strong>Multiplier</strong> <?php echo __(social_select5);?></label>
											</div>

											<div>
												<div>
													<select class="form-control" placeholder="Input" id="select_share" name="share">
														<option value="" readonly><?php echo __(select); ?></option>
														<option value="SELF"><?php echo __(Onlyme); ?></option>
														<option value="ALL_FRIENDS"><?php echo __(friends); ?></option>
														<option value="FRIENDS_OF_FRIENDS"><?php echo __(FRIENDS_OF_FRIENDS); ?></option>
														<option value="EVERYONE"><?php echo __(publics); ?></option>
													</select>
												</div>
											</div>

										</div>

									<?php } ?>

									<?php if( $permission == "group") { ?>

										<div class="row"  id="show_groups">

											<div>
												<label for="email" class="select_reg"><?php echo __(Mygroup); ?>*</label>
											</div>

											<div>
												
												<select class="form-control" id="groups" name="groups"  placeholder="Input">
													<option value="" readonly><?php echo __(select); ?></option>
													<?php foreach($groups['data'] as $g){ ?>
														<option value="<?php echo $g['id']; ?>"><?php echo $g['name']; ?></option>
													<?php } ?>
												</select>

												<?php 
													$info_text = __(group_warning); 
												?>
												
											</div>

										</div>

									<?php } else if( $permission == "page") { ?>
								
										<div class="row"  id="show_pages">

											<div>
												<label for="email" class="select_reg"><?php echo __(Mypages); ?>*</label>
											</div>

											<div>
												
												<select class="form-control" id="pages" name="pages"  placeholder="Input">
													<option value="" readonly><?php echo __(select); ?></option>
													<?php foreach($pages['data'] as $p){ ?>
														<option value="<?php echo $p['id']; ?>"><?php echo $p['name']; ?></option>
													<?php } ?>
												</select>
												
											</div>

										</div>

									<?php } ?>

									<div class="row"  id="stream_title" style="display:none;">

										<div>
											<label for="email" class="select_reg"><?php echo __(Streamtargetname); ?>*</label>
										</div>

										<div>

											<div>
												<input class="form-control" name="target_name" type="text" value="<?php echo $target_name;?>" />
											</div>

										</div>

									</div>

									<?php if($FormListingData['fb_td_status'] ==  "0"){ ?>  

										<div class="row">

											<div class="col-sm-3">
												<label for="email" class="select_reg"><?php echo __(title); ?>*</label>
											</div>

											<div class="col-sm-8">

												<div class="form-group">        
													<input class="form-control" name="stream_title" placeholder="" type="text" >
												</div>

											</div>
										</div>

										<div class="row">

											<div class="col-sm-3">
												<label for="email" class="select_reg"><?php echo __(description); ?>*</label>
											</div>

											<div class="col-sm-8">

												<div class="form-group">        
													<input class="form-control" name="stream_description" placeholder="" type="text" >
												</div>

											</div>
										</div>

									<?php } else{ ?>

										<input name="stream_title" type="hidden" value="<?php echo $FormListingData['facebook_title'];?> ">
										<input name="stream_description" type="hidden" value="<?php echo str_replace('"',"'",$FormListingData['facebook_description']);?>">

									<?php }?>

								<?php } ?>

								<input type="checkbox" name = "agree" id = "terms_checkbox">
								<p id = "terms_text"> <?= __(iagree); ?> 
									<a href="<?php echo $terms_link; ?>" target="_blank"><?php echo __(terms_conditions); ?></a> <?php echo __(getand); ?> <a href="<?php echo $privacy_link; ?>" target="_blank"><?php echo __(privacy_policy); ?></a><?php echo " "; ?><?php echo __(getand); ?><?php echo " "; ?><a target="_blank"  href="<?php echo __(use_policy_link); ?>"><?php echo __(use_policy); ?></a>.</label>
								</p>

								<div id = "share_buttons_wrapper">
									<button class = "btn-disabled register" type = "submit" id = "confirm_share_profile"> <?= __(unpause) ?> </button>
									<button type = "button" id = "exit_share"> <?= __(changeuser) ?> </button>
								</div>

							</div>

						</div>

						<div class = "row" id = "share_footer_wrapper">

							<div id = "share_footer_text_wrapper">
								<p> <?= $info_text ?> </p>
								<!-- <p> <a href = "<?= HTTP_ROOT.__(url_cookies_lang) ?>"> <?= __(private_policy) ?> </a> e <a href = "<?= HTTP_ROOT.'terms'?>"><?= __(terms) ?> </a> Multiplierapp</p> -->
							</div>

						</div>

					</div>
					
					<div class = "<?= ($session->read('newNetwork')) ? "" : "hide" ?>" id = "share_step_3">

						<?= $session->write('successfull',""); $session->write('newNetwork', false);?>

						<h2 class = "share-tittle"> 
							<?= __(yourSocialNetworks) ?>
						</h2>
						<?php if(!$session->read('social_selected_error')){ ?>
							<p class = "share-subtittle"> <?= __(Congratulation_text) ?> <?= __(added_success) ?> </p>
						<?php } ?>

						<div class = "row table_row" id = "socialnetworkList">

							<div class="col-sm-12" style="background: #fff;">

								<p><?php echo __(social_select3); ?></p>

								<div class="table-responsive table_data">

									<table class="table">
										<thead>
											<tr>
												<th><?php echo __(source); ?></th>
												<th><?php echo __(image); ?></th>
												<th><?php echo __(Destination); ?></th>
												<th><?php echo __(name); ?></th>
												<th colspan="2" width="10%" class="text-left"><?php echo __(delete); ?></th>
											</tr>
										</thead>
										<tbody>
											<?php 
												foreach ($fbUserSubData as $fbData) 
												{ 
													$fbData->image_path = $fbData->image_path ? $fbData->image_path : HTTP_ROOT.'img/dummy_image.png';

													if($fbData->flag == 'timeline'){ $flag = __(yourFacebook); }
													if($fbData->flag == 'page'){ $flag = __(page); }
													if($fbData->flag == 'group'){ $flag = __(group); }
													if($fbData->flag == 'event'){ $flag = __(event); }
													if($fbData->flag == "youtube"){ $flag = 'YouTube'; }
											?>

													<tr>

														<?php if($fbData->flag != "youtube") { ?>
															<td><img src="../img/facebook.svg"/></td>
														<?php } else { ?>
															<td><i class="youtube_official fa fa-youtube fbIcon" aria-hidden="true" data-text="Youtube"></i></td>
														<?php } ?>

														<td><img class="listing_image" src="<?php echo $fbData->image_path;?>"></td>

														<td><?php echo $flag;?></td>

														<td><?php echo ucfirst( $fbData->fb_name );?></td>

														<?php if($FormListingData['fb_td_status'] ==  "0"){ ?>   
															<td align="center"><a  onclick = "return confirm('<?= __(Are_you_sure) ?>')" href="<?php echo HTTP_ROOT; ?>commingusers/edit-common?id=<?php echo $fbData->id?>&fid=<?php echo $fbData->form_id?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a> </td>
														<?php } ?>

														<td><a  href="<?php echo HTTP_ROOT; ?>commingusers/deletestream/?id=<?php echo $fbData->id?>&eid=<?php echo $fbData->fb_email?>&fid=<?php echo $fbData->form_id?>" onClick="return confirm('<?= __(Are_you_sure) ?>')" class="delete"><i class="fa fa-trash"></i></a></td>
													
													</tr>
											<?php }?>  
										</tbody>
									</table>
									
								</div>

							</div>

						</div>

						<div class="login_form">          

							<div class="row other_register">

								<div class = "col-sm-6 facebook">
									<a href="javascript:void(0);" id="another_facebook"> <i class = "fab fa-facebook" ></i> <?= __(registerAnother) ?></a>
								</div>

								<div class = "col-sm-6 youtube">
									<a href="javascript:void(0);" id="get_values_gmail"> <i class = "fab fa-youtube" ></i> <?= __(registerAnother) ?> </a>
								</div>

							</div>

						</div>
						
						<?php if($session->read('social_selected_error')){ ?>
							<div class="alert alert-danger"><?php echo $session->read('social_selected_error'); $session->write('social_selected_error',""); ?> </div>
						<?php } ?>

					</div>

					<div class = "step-wrapper<?= ($session->read( 'social.is_login' ) == "google") ? "-google" : "" ?> <?= !($session->read('newNetwork')) ? "" : "hide" ?>" id = "share_step_wrapper">
						<div class = "step <?= ($social_login) ? "active" : "" ?>"></div>
						<div class = "step <?= !($social_login)  ? "active" : "" ?>"></div>
						<!-- <div class = "step <?= !empty($fbUserSubData) ? "active" : "" ?>"></div> -->
					</div>

					<?php if($session->read( 'social.is_login' ) == "google") { ?>

						<a class = "<?= !($social_login) ? "" : "hide" ?>" href = "" target = "_blank">
							<i class = "fab fa-youtube" id = "share_youtube_icon" ></i>
						</a>

					<?php } else { ?>

						<a class = "<?= !($social_login)? "" : "hide" ?>" href = "https://www.facebook.com/multiplierapp/" target = "_blank">
							<i class = "fab fa-facebook" id = "share_facebook_icon" ></i> 
						</a>

					<?php } ?>

				<?php } ?>

			</div>
			
			</form>
          </div>
        </div>
      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->
</div>

<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>                 
<script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="//platform-api.sharethis.com/js/sharethis.js#property=5b362317c5ed9600115214d3&product=inline-share-buttons"></script>
<script type="text/javascript"> var base_url = '<?php echo HTTP_ROOT; ?>';</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/inputmask.min.js" integrity="sha256-OeQqhQmzxOCcKP931DUn3SSrXy2hldqf21L920TQ+SM=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/inputmask.extensions.min.js" integrity="sha256-vpL5m6IilsG6TXFKZ99NU+cBmT122RJ6sqBgq0a9/vQ=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/jquery.inputmask.min.js" integrity="sha256-x4dnYQ/ZWD3+ubQ1kR3oscEWXKZj/gkbZIhGB//kmmg=" crossorigin="anonymous"></script>

<?php //pr($form_data);?>

<!-- BEGIN THEME LAYOUT STYLES -->
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
    $(document).on('click', 'helptext', function (event) {
      $('[data-toggle="tooltip"]').tooltip(); 
    });

</script>

<script type="text/javascript">

    //DEV-116 - Get mandatory aditional data at registration forms
    function countyChanged(){
        var contry_flag = $("#countries_dropdown option:selected").attr("data-flag");
        $('#countryflag').attr('src', contry_flag);

        var country_code = $("#countries_dropdown option:selected").attr("data-phone");
        $('#countrycode').val(country_code);

        var country_id = $("#countries_dropdown option:selected").attr("data-contry-id");

        var states = $("#state");

        if (country_code == '55'){

            $('#phone').inputmask('(99) 99999-9999',{ "clearIncomplete": true });

        }else{
            $('#phone').inputmask('');
        }


    }

    function validateForm(){

        var name = $('#name').val();
        var email = $('#email').val();
        var country = $('#countries_dropdown').val();
        var phone = $('#phone').val();


        if (name == '' || email == '' || country == '' || !validateBRPhone(phone) || !validateEmail(email)){
            return false;
        }else{
            return true;
        }

    }

    function  validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }

    function validateBRPhone($phone) {
        var phoneReg = /(\([1-9][0-9]\)?|[1-9][0-9])\s?([9]{1})([0-9]{4})-?([0-9]{4})$/;
        return phoneReg.test( $phone );
    }

	function fbShareOptionSelect(){
					if($("#share").val()==""){
							$("#shareOptionRequired").show();
									return false;
					}
					$("#shareOptionRequired").hide();
					$('#social_step_1').hide();$('#social_step_2').show();
	}
	
    var from_data = <?php echo json_encode($form_data) ?>;
    // console.log(from_data);
    var validation_fields = {};

	jQuery.each(from_data, function(i, field) {
		if( typeof field.required !== 'undefined' )
		{
			var jsonObj = {};
			if( typeof field.subtype !== 'undefined' )
			{   
				if( field.subtype == 'email' )
				{
					jsonObj['email'] = true;
				}

				if( field.subtype == 'tel' )
				{
					jsonObj['number'] = '!0';
					jsonObj['minlength'] = '5';
					jsonObj['maxlength'] = '10';
				}
			}
			jsonObj['required'] = true;
			validation_fields[field.name] = jsonObj;
		}
	});

	var flagObj = {};
	flagObj['required'] = true;
	validation_fields['flag'] = flagObj;

	var streamTitleObj = {};
	streamTitleObj['required'] = true;
	validation_fields['stream_title'] = streamTitleObj;

	var streamDescriptionObj = {};
	streamDescriptionObj['required'] = true;
	validation_fields['stream_description'] = streamDescriptionObj;
                
    var FormValidation = function() {

    	var e = function() {
   
			var formLang = $("#form_lang").val();
			//alert(formLang);

			var validMsg = '';

			if(formLang == 'Portuguese'){
				validMsg = "Os campos Título e Descrição do post não podem ficar em branco.";
			}
			else if(formLang == 'English'){
				validMsg = "Post Title and Description fields must not be blank.";
			}
			else if(formLang == 'Spanish'){
				validMsg = "Los campos Título y Descripción de la publicación no deben estar en blanco.";
			}

			var e = $("#form_sample_1"),
				r = $(".alert-danger", e),
				i = $(".alert-success", e);

			e.validate({
				errorElement: "span",
				errorClass: "help-block",
				errorPlacement: function(error, element) {
					// console.log(element);
					if( ( element.parent('.mt-radio').length ) || ( element.parent('.mt-checkbox').length) ) {
						error.insertAfter(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				},
				focusInvalid: !1,
				ignore: "",
				messages: {
					select_multi: {
						maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
						minlength: jQuery.validator.format("At least {0} items must be selected")
					},
					stream_title: validMsg,
					stream_description: validMsg,
				},
				rules: validation_fields,
				// ,rules: {
				//     name: {
				//         minlength: 2,
				//         required: !0
				//     },
				//     email: {
				//         required: !0,
				//         email: !0
				//     },
				// },
				invalidHandler: function(e, t) {
					//i.hide(), r.show(), App.scrollTo(r, -200)
				},
				highlight: function(e) {
					$(e).closest(".form-group").addClass("has-error")
				},
				unhighlight: function(e) {
					$(e).closest(".form-group").removeClass("has-error")
				},
				success: function(e) {
					e.closest(".form-group").removeClass("has-error")
				},
				submitHandler: function(e) {
					return true;
					// i.show(), r.hide()
				}
			});


            jQuery('#flag').change(function(){
                if( jQuery(this).val() == 'timeline' )
                {
                    jQuery("#share").rules("add", "required");
                    jQuery("#pages").rules("remove", "required");
                    jQuery("#groups").rules("remove", "required");
                    jQuery("#events").rules("remove", "required");

                }
                else if( jQuery(this).val() == 'page' )
                {
                    jQuery("#pages").rules("add", "required");
                    jQuery("#share").rules("remove", "required");
                    jQuery("#groups").rules("remove", "required");
                    jQuery("#events").rules("remove", "required");
                }
                else if( jQuery(this).val() == 'group' )
                {
                    jQuery("#groups").rules("add", "required");
                    jQuery("#pages").rules("remove", "required");
                    jQuery("#share").rules("remove", "required");
                    jQuery("#events").rules("remove", "required");
                }
                else if( jQuery(this).val() == 'event' ){
                    jQuery("#events").rules("add", "required");
                    jQuery("#groups").rules("remove", "required");
                    jQuery("#pages").rules("remove", "required");
                    jQuery("#share").rules("remove", "required");
                }
                else
                {

                }
            });

        };

    	return {
			init: function() {
				e() // t(), r(), i()
			}
    	}
	}();

	jQuery(document).ready(function() {

        //DEV-116 - Get mandatory aditional data at registration forms
        $('#countries_dropdown').val('br'); 
        $('#countries_dropdown').trigger('change'); 

		$('#change_user, #exit_share').click(function(event){
			event.preventDefault();
			var urlLogout = base_url + 'commingusers/fblogout/?id=';

			<?php  if($session->read( 'social.is_login' ) == "google"){ ?>
				urlLogout = base_url + 'commingusers/commonLogOut/?id=';
			<?php } ?>

			var logout_url =  "<?= $logOutUrl ?>";

			$.ajax({
				'url' : urlLogout + <?php echo $_GET['id'];?>,
				success: function(data){
				window.location.href = logout_url;
				}
			});

		});

		$("#select_share, #terms_checkbox").change(function(){

			if( $("#select_share").val() != "" && $("#terms_checkbox").is(":checked") && $("#groups").val() != "" && $("#pages").val() != "" ){
				$("#confirm_share_profile").removeClass("btn-disabled");
			}
			else{
				$("#confirm_share_profile").addClass("btn-disabled");
			}

		});

		$("#confirm_share_profile").click(function(e){

			if( $(this).hasClass("btn-disabled") ){
				e.preventDefault();
			}

		});

		$('#flag').change(function(){
			var flag = $('#flag').val();
			if(flag == 'timeline'){
			
				var hide_share_option = '<?php echo $FormListingData['hide_share_option'];?>';
				if( hide_share_option == "0"){
				$('#show_timeline').show();
				}
				$('#stream_title').hide();
				$('#show_pages').hide();
				$('#show_events').hide();
				$('#show_groups').hide();
			} else if(flag == 'page') {
				$('#show_pages').show();
				$('#show_events').hide();
				$('#stream_title').hide();
				$('#show_timeline').hide();
				$('#show_groups').hide();
			} else if(flag == 'group') {
				$('#show_groups').show();
				$('#show_events').hide();
				$('#show_pages').hide();
				$('#stream_title').hide();
				$('#show_timeline').hide();
			}else if(flag == 'event'){
				$('#show_events').show();
				$('#show_groups').hide();
				$('#show_pages').hide();
				$('#stream_title').hide();
				$('#show_timeline').hide();
			}else{
				$('#show_pages').hide();
				$('#show_events').hide();
				$('#show_timeline').hide();
				$('#stream_title').hide();
				$('#show_groups').hide();
				return false;
			}
		});

		$("#another_facebook").click(function(){
			$("#share_step_3").addClass("hide");
			$("#share_step_1").removeClass("hide");
			$("#share_youtube_icon").addClass("hide");
		});

		$('#get_values').click(function(){
			window.location.href = base_url + 'commingusers/fblogin?id=' + <?php echo $_GET['id'];?>; 
		});

		$('#get_values_gmail').click(function(){

            if (validateForm() == true){

			//Get values from form to session (AJAX)
			var name_value = $('#name').val();
			var email_value = $('#email').val();
			var country_value = $('#countries_dropdown option:selected').text();
			var phone_value = $('#phone').val();
			var state_value = $('#state option:selected').text();
			var city_value = $('#city option:selected').text();


			$.ajax({
				url: "/app/commingusers/saveFormSession",
				type: "POST",
				data: {name: name_value, email: email_value, country: country_value, phone: phone_value, state: state_value, city: city_value },
				dataType: "json"
			}).done(function(result) {

			});

			setTimeout(function(){ 
				window.location.href = base_url + 'commingusers/glogin?id=' + <?php echo $_GET['id'];?>; 
			}, 3000);


			//window.location.href = base_url + 'commingusers/glogin?id=' + <?php echo $_GET['id'];?>; 
			}else{

				var formLang = $("#form_lang").val();
				var uemail = $('#uemail').val();
				var uphone = $('#uphone').val();

				//alert(uphone);
				if(!validateBRPhone(uphone)){
					if(formLang == 'Portuguese'){
						var validMsg = "Todos os campos são de preenchimento obrigatório.";
					}
					else if(formLang == 'English'){
						var validMsg = "All fields are required.";
					}
					else if(formLang == 'Spanish'){
						var validMsg = "Todos los campos son obligatorios.";
					}
					alert(validMsg);
				}
				else if(!validateEmail(uemail)){
					if(formLang == 'Portuguese'){
						var validMsg = "E-mail inválido.";
					}
					else if(formLang == 'English'){
						var validMsg = "Invalid email.";
					}
					else if(formLang == 'Spanish'){
						var validMsg = "Email inválido.";
					}
				//$('#email').blur();
					alert(validMsg);
				}
				else
				{
					window.location.href = base_url + 'commingusers/glogin?id=' + <?php echo $_GET['id'];?>;
				}
			}
		});

		$('#get_values_facebook').click(function(){

			if (validateForm() == true){

				//Get values from form to session (AJAX)
				var name_value = $('#name').val();
				var email_value = $('#email').val();
				var country_value = $('#countries_dropdown option:selected').text();
				var phone_value = $('#phone').val();

				$.ajax({
					url: "/app/commingusers/saveFormSession",
					type: "POST",
					data: {name: name_value, email: email_value, country: country_value, phone: phone_value },
					dataType: "json"
				}).done(function(result) {

				});

				// $('.fb_login_box').show();
				// window.location.href = base_url + 'commingusers/fblogin?id=' + <?php echo $_GET['id'];?>; 

				//NEW SHARE MODAL 
				$("#share_modal").removeClass("hide");
				// $('html, body').animate({ scrollTop: 0 }, 'slow');

			}
			else {

				var formLang = $("#form_lang").val();
				if(validateBRPhone( $('#phone').val() )) {

					if(formLang == 'Portuguese') {
						var validMsg = "Todos os campos são de preenchimento obrigatório.";
					}
					else if(formLang == 'English') {
						var validMsg = "All fields are required.";
					}
					else if(formLang == 'Spanish'){
						var validMsg = "Todos los campos son obligatorios.";
					}

					alert(validMsg);
				}
				else {
					if(formLang == 'Portuguese'){
						var validMsg = "Número de telefone móvel inválido.";
					}
					else if(formLang == 'English'){
						var validMsg = "Invalid mobile phone number.";
					}
					else if(formLang == 'Spanish'){
						var validMsg = "Número de teléfono móvil no válido.";
					}

					alert(validMsg);
				}
			}
		});
	
		$("#close_share_modal").click(function(){
			$("#share_modal").addClass("hide");

			if($(this).parent().find("#exit_share").length > 0){
				$("#exit_share").click();
			}

			if($("#youtube_share_error").length > 0) {
				location.reload();
			}

		});

		$("#personal_profile, #personal_profile_mobile").click(function(){
			window.location.href = `${base_url}commingusers/fblogin?id=<?=$_GET['id'];?>&permission=0`; 
		});

		$("#group, #group_mobile").click(function(){
			window.location.href = `${base_url}commingusers/fblogin?id=<?=$_GET['id'];?>&permission=1`; 
		});

		$("#page, #page_mobile").click(function(){
			window.location.href = `${base_url}commingusers/fblogin?id=<?=$_GET['id'];?>&permission=2`; 
		});

		$(".show-social-networks-list").click(function(){
			$("#share_step_2, #share_facebook_icon, #share_youtube_icon, #share_step_wrapper").addClass("hide");
			$("#share_step_3").removeClass("hide");
		});

		$(".show-social-networks-form").click(function(){
			$("#share_step_3").addClass("hide");
			$("#share_step_2, #share_facebook_icon, #share_youtube_icon, #share_step_wrapper").removeClass("hide");
		});

		FormValidation.init();
	});

</script>

<script type="text/javascript">

	$(document).on('change', '.other', function (event) {
		if($(this).is(':checked')){
			$(".other-box").show(200);
			$(".other-box").prop('required',true);
			// $(this).parent().find(".help-block").show();
		}else{
			$(".other-box").hide(200);
			// $(this).parent().find(".help-block").hide();
			$(".other-box").prop('required',false);
		}
	});

	$(document).on('keyup', '.other-box', function (event) {
		$(".other").val($(this).val());
	});


	$(document).on("click", "input[class='radio-input']", function (event) 
	{
		var is_other = $(this).attr('other-field');
		console.log($(this).attr('other-field'));
		console.log($(this));
		if(is_other == '1'){
			$(".other-box-radio").show(200);
			$(".other-box-radio").prop('required',true);
			// $(this).parent().find(".help-block").show();
		}else{
			$(".other-box-radio").hide(200);
			// $(this).parent().find(".help-block").hide();
			$(".other-box-radio").prop('required',false);
		}

	});

	$(document).on('keyup', '.other-box-radio', function (event) {
		$(".radio-input").val($(this).val());
	});

</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150918709-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-150918709-1');
</script>
  
<a target="_blank" href="https://multiplierapp.atlassian.net/servicedesk/customer/portal/1"><img class = "img-zoom" src="<?php echo HTTP_ROOT ;?>img/1962073065-Help.png" id="bottom-help-img"></a> 